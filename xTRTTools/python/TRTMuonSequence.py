
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from AsgAnalysisAlgorithms.AnalysisObjectSharedSequence import makeSharedObjectSequence
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
import ROOT

# helper method to translate muon ID working point to a quality value
def getMuonQuality(workingPoint) :
    quality = None
    if workingPoint == 'Tight' :
        quality = ROOT.xAOD.Muon.Tight
    elif workingPoint == 'Medium' :
        quality = ROOT.xAOD.Muon.Medium
    elif workingPoint == 'Loose' :
        quality = ROOT.xAOD.Muon.Loose
    elif workingPoint == 'VeryLoose' :
        quality = ROOT.xAOD.Muon.VeryLoose
    elif workingPoint == 'HighPt' :
        quality = 4
    elif workingPoint == 'LowPtEfficiency' :
        quality = 5
    return quality

# Creates a muon selection sequence that cuts on pT, eta, impact parameter, muon quality and isolation.
# The cuts on muon quality and isolation are not working properly in TRTxAODs.
# There is a possibility to enable cut-flow histograms and kinematic plots.
# Creates a shallow copy container with the selection already applied.
def makeTRTMuonSelectionSequence(dataType, minPt=3e3, identificationWP='', isolationWP='NonIso', postfix='', enableCutflow=True, enableKinematicHistograms=False):
    postfix = '_'+postfix if postfix else ''
    muonQuality = getMuonQuality(identificationWP)

    seq = AnaAlgSequence( "TRTMuonSelectionSequence{}".format(postfix) )
    seq.addMetaConfigDefault ("selectionDecorNames", [])
    seq.addMetaConfigDefault ("selectionDecorNamesOutput", [])
    seq.addMetaConfigDefault ("selectionDecorCount", [])

    # kinematic cuts
    alg = createAlgorithm( 'CP::AsgSelectionAlg', 'MuonPtEtaCutAlg'+postfix )
    alg.selectionDecoration = 'selectPtEta{},as_bits'.format(postfix)
    addPrivateTool( alg, 'selectionTool', 'CP::AsgPtEtaSelectionTool' )
    alg.selectionTool.maxEta = 2.1
    alg.selectionTool.minPt = minPt
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                      'selectionDecorNamesOutput' : [alg.selectionDecoration],
                      'selectionDecorCount' : [2]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])})

    # impact parameter cuts - need to use custom tool since the official tool uses the primary track,
    # which is often not available.
    alg = createAlgorithm( 'xTRT::AsgLeptonTrackSelectionAlg', 'MuonTrackSelectionAlg'+postfix )
    alg.selectionDecoration = 'trackSelection{},as_bits'.format(postfix)
    alg.maxD0Significance = 3
    alg.maxDeltaZ0SinTheta = 0.5
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                      'selectionDecorNamesOutput' : [alg.selectionDecoration],
                      'selectionDecorCount' : [3]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])})

    # muon ID - missing MS tracks in TRTxAODs will make all muons fail any ID cut
    if muonQuality is not None:
        alg = createAlgorithm( 'CP::MuonSelectionAlgV2', 'MuonSelectionAlg'+postfix )
        addPrivateTool( alg, 'selectionTool', 'CP::MuonSelectionTool' )
        alg.selectionTool.MuQuality = muonQuality
        alg.selectionTool.IsRun3Geo = True
        alg.selectionTool.DisablePtCuts = True
        alg.selectionDecoration = 'good_muon{},as_bits'.format(postfix)
        alg.badMuonVetoDecoration = 'is_bad,as_char'
        seq.append( alg, inputPropName = 'muons',
            stageName = 'selection',
            metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                          'selectionDecorNamesOutput' : [alg.selectionDecoration],
                          'selectionDecorCount' : [4]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])})

    # muon isolation - the input variables for recommended isolation WPs are missing in TRTxAODs
    if isolationWP != 'NonIso':
        alg = createAlgorithm( 'CP::MuonIsolationAlg', 'MuonIsolationAlg'+postfix )
        addPrivateTool( alg, 'isolationTool', 'CP::IsolationSelectionTool' )
        alg.isolationDecoration = 'isolated{},as_bits'.format(postfix)
        alg.isolationTool.MuonWP = isolationWP
        seq.append( alg, inputPropName = 'muons',
            stageName = 'selection',
            metaConfig = {'selectionDecorNames' : [alg.isolationDecoration],
                          'selectionDecorNamesOutput' : [alg.isolationDecoration],
                          'selectionDecorCount' : [1]},
            dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])})

    makeSharedObjectSequence (seq, deepCopyOutput = False, shallowViewOutput = True, postfix = '_TRTMuon'+postfix,
        enableCutflow = enableCutflow, enableKinematicHistograms = enableKinematicHistograms)
    
    return seq