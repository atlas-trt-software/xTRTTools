# xTRTTools

[![pipeline status](https://gitlab.cern.ch/atlas-trt-software/xTRTTools/badges/master/pipeline.svg)](https://gitlab.cern.ch/atlas-trt-software/xTRTTools/commits/master)

## Documentation
The full documentation, tutorials, etc. are available [here](doc/docs/index.md).

## Quick start

Check out the `xTRTTools` package:
```bash
cd /path/to/source
git clone ssh://git@gitlab.cern.ch:7999/atlas-trt-software/xTRTTools.git
```

Set up the analysis environment and compile:
```bash
setupATLAS
asetup --stable AthAnalysis,22.2,latest
mkdir ./build
cd ./build
cmake ../xTRTTools/
make
source x86_64-*/setup.sh
```