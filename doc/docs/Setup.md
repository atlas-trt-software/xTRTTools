# Starting an Analysis Package

## Analysis Software Tutorial and Purpose of `xTRTTools`

The ATLAS Analysis Software Group (ASG) maintains a tutorial for
building an ATLAS Analysis Algorithm, which you can find at [this
website](https://atlassoftwaredocs.web.cern.ch/ABtutorial/). That
tutorial covers all basics for writing your own analysis code.

The xTRTTools package can be used either as a standalone package
together with the ATLAS analysis framework to produce TRT n-tuples.
Alternatively you can include it as a dependency in your analysis
package to provide functions to easily retrieve TRT information.
The algorithms provided with the xTRT tools can be used as
examples for your own analysis algorithms.

## Getting the xTRTTools package

```bash
cd /path/to/source
git clone ssh://git@gitlab.cern.ch:7999/atlas-trt-software/xTRTTools.git
```

## Building xTRTTools for the first time

First we need to set up the ATLAS software
environment. You can use either `AthAnalysis` or `AnalysisBase` (see the
[ATLAS tutorial](https://atlassoftwaredocs.web.cern.ch/ABtutorial/)
for details on these two options). Here we are using `AthAnalysis`.

```bash
setupATLAS
asetup --stable AthAnalysis,22.2,latest
```

Now we need to create a build directory and run cmake so that our analysis
environment will find `xTRTTools`. Then we run make to compile the code.
Finally, we need to set the environment variables (only once when
running in a fresh terminal):

```bash
mkdir ./build
cd ./build
cmake ../xTRTTools/
make
source x86_64-*/setup.sh
```

## Adding `xTRTTools` usage to your analysis code

The sections below are only needed if you want to include the
xTRTTools as a dependency to your own analysis package.

If you've followed the ASG tutorial then you already have a working
ATLAS analysis package which includes an algorithm
(`EL::AnaAlgorithm`) class. The structure of your `source` area should
look something like this:

```none
.
├── ATestRun.py
├── CMakeLists.txt
└── MyAnalysis
    ├── CMakeLists.txt
    ├── MyAnalysis
    │   ├── MyAnalysisDict.h
    │   ├── MyxAODAnalysis.h
    │   └── selection.xml
    ├── Root
    │   └── MyxAODAnalysis.cxx
    └── share
```

!!! note
    Here we are using the python based configuration for EventLoop,
    i.e. using AnalysisBase instead of AthAnalysis. This is why
    there is no `src` folder.


We're going to add `xTRTTools` to the source area by cloning the git
repository (calling your source area `/path/to/source`):

You should now see the compiler building both your package
(`MyxAODAnalysis`) and `xTRTTools`.

### Adding xTRTTools as a dependency

Now we're going to make your package dependent on `xTRTTools`. In
`source/MyxAODAnalysis/CMakeLists.txt` add the following section above
the `atlas_add_library` call:

```cmake
atlas_depends_on_subdirs (
  PUBLIC
  xTRTTools
  )
```

Update the `atlas_add_library` call to this look like this:

```cmake
# Add the shared library:
atlas_add_library (
  MyAnalysisLib
  MyAnalysis/*.h Root/*.cxx
  PUBLIC_HEADERS MyAnalysis
  LINK_LIBRARIES AnaAlgorithmLib xTRTToolsLib
  )
```

Since we've updated your packages `CMakeLists.txt` file, we need to
re-run cmake again:

```
cd /path/to/build
cmake ../source
make
```

Now your analysis environment includes your `MyxAODAnalysis` package
_and_ `xTRTTools`, with the `MyxAODAnalysis` packaging being dependent
on `xTRTools`, so you'll be able to access the `xTRTTools` API from
your package. You can have a look at the Doxygen [API
Reference](http://trtswdocs.web.cern.ch/xtrttoolsapi/).
