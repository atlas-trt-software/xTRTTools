# TRT N-Tuples

The `xTRTTools` package comes with several pre-defined workflows
to produce standardised TRT n-tuples. The respective job option
files can be modified to tune the object selection without the
need to modify the c++ code or to re-compile the code.


## Local n-tuples production
The n-tuple production is usually very fast. Producing them locally
is no problem if you have access to all input xAOD files. Before
launching a large scale production on the GRID it is also good
practice to perform a small scale test locally first.

Set up the `xTRTTools` following the [instructions](./Setup.md).

Run the desired job options script:
```bash
athena xTRTTools/TRTNTupler_jobOptions.py --filesInput=<myFile.root>
```

## N-tuple production on the GRID
After the usual setup procedure you also need to initialise your grid credentials:
```bash
lsetup panda
# authenticate with grid certificate
voms-proxy-init -voms atlas
# authentication with production role in case of official production
voms-proxy-init --voms atlas:/atlas/det-indet/Role=production
```

Then you need to prepare a submission directory and copy the desired job options
script. The content of the submission directory will be shipped to each worker
node. The datasets that should be processed need to be defined in a text file.
Finally, submit the jobs using `pathena` and define the ouput dataset name.

```bash
mkdir ./submission/
cd ./submission/
cp ../build/x86_64-*/jobOptions/xTRTTools/*.py .
# Tag-and-probe n-tuple production using data as input
pathena --inDsTxt=datasets_r23_data.txt --addNthFieldOfInDSToLFN=1,2,3,6 --mergeOutput --outDS=group.det-indet.TRTNTupTnP_v03_data TRTNTuplerTagAndProbe_jobOptions.py --official
# Full n-tuple production using MC samples as input
pathena --inDsTxt=datasets_r23_mc.txt --addNthFieldOfInDSToLFN=1,2,3,6 --mergeOutput --outDS=group.det-indet.TRTNTupTnP_v03_mc TRTNTuplerTagAndProbe_jobOptions.py --official
```

For a private production, i.e. no production role, remove `--official` and change the output dataset name: `--outDS=user.$USER.abcd`.

## TRT N-tuple content
All TRT n-tuples are ROOT files with a single ROOT `TTree`. Each entry
corresponds to a selected track, truth particle, electron or muon,
where each of these objects corrsponds to a dedicated set of branches
that can be identified by their prefix.

The objects are matched by the logic of the `TRTTrackWriterAlg`, such
that the corresponding truth particles, tracks, electron and muon
objects are filled in parallel branches. This allows to directly relate
their quantities and for example calculate differences in momentum, energy, etc.
to derive the resolution; or determine reconstruction efficiencies and fake
rates.

If no corresponding object is found (for example a reconstructed track that
can not be mapped to a truth particle), all of these branches are set to 0.

The list of stored variables is described [here](./NTupleContent.md).

## Full n-tuples
[TRTNTupler_jobOptions.py](../../xTRTTools/share/TRTNTupler_jobOptions.py) produces n-tuples that contain all
tracks and truth particles (in case of MC) that pass some simple
pre-selection. The corresponding electrons and muons are stored as well.

Since all good tracks are stored these files are typically very large.
They are suited for MC studies to dermine reconstruction efficiencies.
fake rates and resolutions for all tracks, including pile-up tracks.

## Tag and probe n-tuples
[TRTNTuplerTagAndProbe_jobOptions.py](../../xTRTTools/share/TRTNTuplerTagAndProbe_jobOptions.py) produces n-tuples that contain
only those tracks that pass a tag and probe selection. The tag objects
are all good electrons and muons that pass some pre-selection.
The tag and probe selection is based on the combined invariant mass and
the z0 distance of the tag object and the probe track.
For MC truth electron and muon particles are stored as well.

These n-tuples are suitable for selecting pure samples of electron and
muon tracks in data and MC.

## DAOD n-tuples
[TRTNTuplerDAOD_jobOptions.py](../../xTRTTools/share/TRTNTuplerDAOD_jobOptions.py) produces n-tuples that are very similar
to the full n-tuples described above. The only difference is that
physics analysis derived AOD files are expected as input (for example `DAOD_PHYS`).
These AOD files typically contain only contain tracks that correspond to
physics objects (electrons, muons, taus, jets) and do not contain
TRT hit level information.

These n-tuples are of very limited use for TRT studies, but can be useful
to for comparisons with studies performed in physics analysis context.
