// IAsgNTupleWriterTool.h

#ifndef IAsgParticleWriterTool_H
#define IAsgParticleWriterTool_H

#include "AsgTools/IAsgTool.h"
#include "xAODBase/IParticle.h"
#include "xAODTracking/Vertex.h"
#include "xAODEventInfo/EventInfo.h"
#include "TTree.h"

class IAsgParticleWriterTool : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(IAsgParticleWriterTool)

public:

  virtual bool fill(const xAOD::IParticle* particle, const xAOD::EventInfo* eventInfo=nullptr, const xAOD::Vertex* vertex=nullptr) =0;
  virtual StatusCode setupTree(TTree* tree) =0;
};

#endif
