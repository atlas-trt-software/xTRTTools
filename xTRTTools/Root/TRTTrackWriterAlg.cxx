/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/TRTTrackWriterAlg.h>
#include <xTRTTools/Helpers.h>

#include <xAODBase/IParticleHelpers.h>
#include <xAODTruth/xAODTruthHelpers.h>
#include <xAODTracking/TrackMeasurementValidationContainer.h>
#include <xAODTracking/TrackStateValidationContainer.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>

#include <AsgMessaging/MessageCheck.h>

xTRT::TRTTrackWriterAlg::TRTTrackWriterAlg(const std::string& name,
                             ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator) {
  declareProperty("truthPrefix", m_truthPrefix = "truth_", "Prefix used for the truth-level branches");
  declareProperty("primaryVertexPrefix", m_primaryVertexPrefix = "primaryVertex_", "Prefix used for the primary vertex branches");
  declareProperty("muonPrefix", m_muonPrefix = "muon_", "Prefix used for the muon branches");
  declareProperty("electronPrefix", m_electronPrefix = "electron_", "Prefix used for the electron branches");
  declareProperty("treeName", m_treeName = "TRT", "Name of the output ROOT tree");
  declareProperty("storeOnlyObjectsMatchedToTracks", m_storeOnlyObjectsMatchedToTracks = false, "Decide whether to store only those objects that correspond to a track object");
  declareProperty("isMC", m_isMC = true, "Decide whether the sample is simulated or real data");
  declareProperty("trackWriterTools", m_trackWriterTools, "List of tools to write track information. Typically, each tool corresponds to one collection.");

  declareProperty("eventInfoVariables", m_eventInfoVariables = EventInfoFiller::defaultVariables, "List of event info variables to fill");
  declareProperty("electronVariables", m_electronVariables = ElectronFiller::defaultVariables, "List of track variables to fill");
  declareProperty("muonVariables", m_muonVariables = MuonFiller::defaultVariables, "List of track variables to fill");
}

StatusCode xTRT::TRTTrackWriterAlg::initialize() {
  ANA_CHECK(m_eventInfoHandle.initialize(m_systematicsList));
  ANA_CHECK(m_primaryVerticesHandle.initialize(m_systematicsList, SG::AllowEmpty));
  ANA_CHECK(m_truthParticleHandle.initialize(m_systematicsList, SG::AllowEmpty));
  ANA_CHECK(m_electronHandle.initialize(m_systematicsList, SG::AllowEmpty));
  ANA_CHECK(m_muonHandle.initialize(m_systematicsList, SG::AllowEmpty));
  ANA_CHECK(setupTree());
  return StatusCode::SUCCESS;
}

StatusCode xTRT::TRTTrackWriterAlg::setupTree() {
  ANA_CHECK(book(TTree(m_treeName.c_str(),m_treeName.c_str())));
  m_tree = tree(m_treeName.c_str());
  if(!m_tree)  {
    ATH_MSG_ERROR("Could not find output tree \"" << m_treeName << "\"");
    return StatusCode::FAILURE;
  }

  m_eventInfoFiller = std::make_unique<xTRT::EventInfoFiller>("", m_tree, m_eventInfoVariables, m_isMC);

  if (not m_primaryVerticesHandle.empty()) {
    m_primaryVertexFiller = std::make_unique<xTRT::VertexFiller>(m_primaryVertexPrefix, m_tree, VertexFiller::defaultVariables, m_isMC);
  }

  if (not m_truthParticleHandle.empty()) {
    m_truthFiller = std::make_unique<xTRT::TruthParticleFiller>(m_truthPrefix, m_tree, TruthParticleFiller::defaultVariables, m_isMC);
  }
  ANA_CHECK(m_trackWriterTools.retrieve());
  for (auto toolHandle : m_trackWriterTools) {
    ANA_CHECK(toolHandle->setupTree(m_tree));
    std::string containerName;
    ANA_CHECK(toolHandle->getProperty("container", containerName));
    m_trackContainerNames.push_back(containerName); // list of track containers to determine order of processing
    m_containerNameToTrackWriterTool[containerName] = toolHandle.operator->();
    m_containerNameToTrackContainer[containerName] = new CP::SysReadHandle<xAOD::TrackParticleContainer>(this, containerName, containerName, "");
    ANA_CHECK(m_containerNameToTrackContainer[containerName]->initialize(m_systematicsList));
  }

  if (not m_electronHandle.empty()) {
    m_electronFiller = std::make_unique<xTRT::ElectronFiller>(m_electronPrefix, m_tree, m_electronVariables, m_isMC);
  }

  if (not m_muonHandle.empty()) {
    m_muonFiller = std::make_unique<xTRT::MuonFiller>(m_muonPrefix, m_tree, m_muonVariables, m_isMC);
  }

  ANA_CHECK (m_systematicsList.initialize());

  return StatusCode::SUCCESS;
}

StatusCode xTRT::TRTTrackWriterAlg::execute() {
  for (const auto& sys : m_systematicsList.systematicsVector()) {
    auto& counter = m_counters[sys];
    ATH_MSG_DEBUG("Processing event " << counter.nEvents);
    const xAOD::EventInfo* eventInfo = nullptr;
    ANA_CHECK(m_eventInfoHandle.retrieve(eventInfo, sys));

    if (m_isMC != eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
      ATH_MSG_ERROR("\"isMC =" << m_isMC << "\", but \"EventInfo::IS_SIMULATION = " << eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) << "\"");
      return StatusCode::FAILURE;
    }

    m_eventInfoFiller->reset();
    m_eventInfoFiller->fill(eventInfo);

    // get the primary vertex
    const xAOD::VertexContainer* primaryVertices = nullptr;
    const xAOD::Vertex* primaryVertex = nullptr;
    if (not m_primaryVerticesHandle.empty() and m_primaryVerticesHandle.retrieve(primaryVertices, sys) == StatusCode::SUCCESS) {
      m_primaryVertexFiller->reset();
      if (primaryVertices and primaryVertices->size() > 0) {
        primaryVertex = primaryVertices->at(0);
        m_primaryVertexFiller->fill(primaryVertex);
      }
    }

    ATH_MSG_DEBUG("Processing truth particles");
    // collect all truth particles that should be reconstructed
    std::vector<const xAOD::TruthParticle*> selectedTruthParticles;
    const xAOD::TruthParticleContainer* truthParticles = nullptr;
    if (m_isMC and not m_truthParticleHandle.empty() and m_truthParticleHandle.retrieve(truthParticles, sys) == StatusCode::SUCCESS) {
      for (const auto& particle : *truthParticles) {
        // in case of a shallow copy, we use the original truth particle since we make use of the truth particle links
        auto originalParticle = dynamic_cast<const xAOD::TruthParticle*>(xAOD::getOriginalObject(*particle));
        if (originalParticle) {
          selectedTruthParticles.push_back(originalParticle);
        } else {
          selectedTruthParticles.push_back(particle);
        }
      }
    }

    ATH_MSG_DEBUG("Processing track particles");
    std::map<std::string, std::vector<const xAOD::TrackParticle*>> containerToTracks;
    // collect all tracks from all track containers that should be stored
    for (const auto& containerName : m_trackContainerNames){
      const auto& trackHandle = m_containerNameToTrackContainer[containerName];
      const xAOD::TrackParticleContainer* tracks = nullptr;
      std::vector<const xAOD::TrackParticle*> selectedTracks(selectedTruthParticles.size(), nullptr);
      if (trackHandle->retrieve(tracks, sys) == StatusCode::SUCCESS) {
        for (const auto& track : *tracks) {
          auto truthParticle = xAOD::TruthHelpers::getTruthParticle(*track);
          // found a truth particle, check if this is already in the list of stored truth particles
          if (truthParticle) {
            auto itTruthParticle = std::find(selectedTruthParticles.begin(), selectedTruthParticles.end(), truthParticle);
            if (itTruthParticle == selectedTruthParticles.end()) {
              // new truth particle
              selectedTruthParticles.push_back(truthParticle);
              selectedTracks.push_back(track);
              // previously processed track containers
              for (auto& [containerName, otherTracks] : containerToTracks) {
                otherTracks.push_back(nullptr);
              }
            } else {
              // update track entry corresponding to found truth particle
              selectedTracks[std::distance(selectedTruthParticles.begin(), itTruthParticle)] = track;
            }
          } else {
            // special treatment for derived track particles (like GSF tracks)
            auto originalTrack = xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(track);
            bool foundOtherTrack = false;
            if (originalTrack != nullptr) {
              for (auto& [otherContainerName, otherTracks] : containerToTracks) {
                for (size_t index=0; index < otherTracks.size(); index++) {
                  auto otherTrack = otherTracks[index];
                  if (otherTrack == nullptr) continue;
                  // in case of a shallow copy of the track we also need to compare with the original track instead
                  auto otherOriginalTrack = dynamic_cast<const xAOD::TrackParticle*>(xAOD::getOriginalObject(*otherTrack));
                  if (originalTrack == otherTrack or (otherOriginalTrack != nullptr and originalTrack == otherOriginalTrack)) {
                    foundOtherTrack = true;
                    // found the corresponding track, simply update the entry
                    selectedTracks[index] = track;
                    break;
                  }
                }
                if (foundOtherTrack) break;
              }
            }
            if (!foundOtherTrack) {
              // no corresponding truth particle and no other matching track particle
              selectedTruthParticles.push_back(nullptr);
              selectedTracks.push_back(track);
              // previously processed track containers
              for (auto& [otherContainerName, otherTracks] : containerToTracks) {
                otherTracks.push_back(nullptr);
              }
            }
          } 
        }
      }
      containerToTracks[containerName] = selectedTracks;
    }

    ATH_MSG_DEBUG("Processing electrons");
    // process electrons
    std::vector<const xAOD::Electron*> selectedElectrons;
    const xAOD::ElectronContainer* electrons = nullptr;
    if (not m_electronHandle.empty() and m_electronHandle.retrieve(electrons, sys) == StatusCode::SUCCESS) {
      selectedElectrons.resize(selectedTruthParticles.size(), nullptr);
      for (const auto& electron : *electrons) {
        auto track = xAOD::EgammaHelpers::getOriginalTrackParticle(electron);
        bool foundTrack = false;
        for (auto& [trackContainerName, tracks] : containerToTracks) {
          for (size_t index=0; index < tracks.size(); index++) {
            auto otherTrack = tracks[index];
            if (otherTrack == nullptr) continue;
            // in case of a shallow copy of the track we also need to compare with the original track instead
            auto originalTrack = dynamic_cast<const xAOD::TrackParticle*>(xAOD::getOriginalObject(*otherTrack));
            if (track == otherTrack or (originalTrack != nullptr and track == originalTrack)) {
              foundTrack = true;
              // found the corresponding track, simply update the entry
              selectedElectrons[index] = electron;
              break;
            }
          }
          if (foundTrack) break;
        }
        if (foundTrack or m_storeOnlyObjectsMatchedToTracks) continue;
        // no corresponding track found, check for matching truth particle
        if (m_isMC) {
          auto truthParticle = xAOD::TruthHelpers::getTruthParticle(*electron);
          if (truthParticle) {
            auto itTruthParticle = std::find(selectedTruthParticles.begin(), selectedTruthParticles.end(), truthParticle);
            if (itTruthParticle == selectedTruthParticles.end()) {
              selectedElectrons[std::distance(selectedTruthParticles.begin(), itTruthParticle)] = electron;
              continue;
            }
          }
        }
        // found neither track nor truth particle
        selectedTruthParticles.push_back(nullptr);
        selectedElectrons.push_back(electron);
        for (auto& [trackContainerName, tracks] : containerToTracks) {
          tracks.push_back(nullptr);
        }
      }
    }

    ATH_MSG_DEBUG("Processing muons");
    // process muons
    std::vector<const xAOD::Muon*> selectedMuons;
    const xAOD::MuonContainer* muons = nullptr;
    if (not m_muonHandle.empty() and m_muonHandle.retrieve(muons, sys) == StatusCode::SUCCESS) {
      selectedMuons.resize(selectedTruthParticles.size(), nullptr);
      for (const auto& muon : *muons) {
        auto track = muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
        bool foundTrack = false;
        for (auto& [trackContainerName, tracks] : containerToTracks) {
          for (size_t index=0; index < tracks.size(); index++) {
            auto otherTrack = tracks[index];
            if (otherTrack == nullptr) continue;
            // in case of a shallow copy of the track we also need to compare with the original track instead
            auto originalTrack = dynamic_cast<const xAOD::TrackParticle*>(xAOD::getOriginalObject(*otherTrack));
            if (track == otherTrack or (originalTrack != nullptr and track == originalTrack)) {
              foundTrack = true;
              // found the corresponding track, simply update the entry
              selectedMuons[index] = muon;
              break;
            }
          }
          if (foundTrack) break;
        }
        if (foundTrack or m_storeOnlyObjectsMatchedToTracks) continue;
        // no corresponding track found, check for matching truth particle
        if (m_isMC) {
          auto truthParticle = xAOD::TruthHelpers::getTruthParticle(*muon);
          if (truthParticle) {
            auto itTruthParticle = std::find(selectedTruthParticles.begin(), selectedTruthParticles.end(), truthParticle);
            if (itTruthParticle == selectedTruthParticles.end()) {
              selectedMuons[std::distance(selectedTruthParticles.begin(), itTruthParticle)] = muon;
              continue;
            }
          }
        }
        // found neither track nor truth particle
        selectedTruthParticles.push_back(nullptr);
        if (not selectedElectrons.empty()) selectedElectrons.push_back(nullptr);
        selectedMuons.push_back(muon);
        for (auto& [trackContainerName, tracks] : containerToTracks) {
          tracks.push_back(nullptr);
        }
      }
    }

    // truth particle and all track particle vectors have the same length by construction
    for (size_t index = 0; index < selectedTruthParticles.size(); index++) {
      if (m_isMC) {
        m_truthFiller->reset();
        if (selectedTruthParticles.at(index)) {
          ATH_MSG_DEBUG("Filling truth particle branches: pT=" << 1e-3*selectedTruthParticles.at(index)->pt() << "GeV");
          m_truthFiller->fill(selectedTruthParticles.at(index));
          counter.nTruthParticles++;
        }
      }
    
      for (auto& [trackContainerName, trackWriterTool] : m_containerNameToTrackWriterTool) {
        auto track = containerToTracks[trackContainerName].at(index);
        if (trackWriterTool->fill(track, eventInfo, primaryVertex)) counter.nTracks++;
      }

      if (selectedElectrons.size()) {
        m_electronFiller->reset();
        if (selectedElectrons.at(index)) {
          ATH_MSG_DEBUG("Filling electron branches: pT=" << 1e-3*selectedElectrons.at(index)->pt() << "GeV");
          m_electronFiller->fill(selectedElectrons.at(index));
          counter.nElectrons++;
        }
      }
      if (selectedMuons.size()) {
        m_muonFiller->reset();
        if (selectedMuons.at(index)) {
          ATH_MSG_DEBUG("Filling muon branches: pT=" << 1e-3*selectedMuons.at(index)->pt() << "GeV");
          m_muonFiller->fill(selectedMuons.at(index));
          counter.nMuons++;
        }
      }
      ATH_MSG_DEBUG("Filling tree");
      m_tree->Fill();
    }

    ATH_MSG_DEBUG("Done processing event " << counter.nEvents);
    counter.nEvents++;
  } 
  return StatusCode::SUCCESS;
}

StatusCode xTRT::TRTTrackWriterAlg::finalize() {
  for (const auto& sys : m_systematicsList.systematicsVector()) {
    auto counter = m_counters[sys];
    ATH_MSG_INFO("Processed " << counter.nEvents << " events.");
    ATH_MSG_INFO("Filled branches for " << counter.nTruthParticles << " truth particles.");
    ATH_MSG_INFO("Filled branches for " << counter.nTracks << " tracks.");
    ATH_MSG_INFO("Filled branches for " << counter.nElectrons << " electrons.");
    ATH_MSG_INFO("Filled branches for " << counter.nMuons << " muons.");
  }
  return StatusCode::SUCCESS;
}