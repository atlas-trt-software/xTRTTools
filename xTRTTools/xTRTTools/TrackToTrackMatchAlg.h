/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef xTRTTools_TrackToTrackMatchAlg_H
#define xTRTTools_TrackToTrackMatchAlg_H

#include <PATCore/IAsgSelectionTool.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>
#include <AsgServices/ServiceHandle.h>
#include <SystematicsHandles/ISystematicsSvc.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SelectionHelpers/SysWriteSelectionHandle.h>
#include <SelectionHelpers/SelectionReadAccessorChar.h>
#include <SelectionHelpers/SysReadSelectionHandle.h>

#include <xAODTracking/TrackParticleContainer.h>

// C++ includes
#include <string>
#include <memory>
#include <map>

namespace xTRT {
  class TrackToTrackMatchAlg : public EL::AnaAlgorithm {
  public:
    // this is a standard algorithm constructor
    TrackToTrackMatchAlg(const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize() override;
    virtual StatusCode execute()    override;
    virtual StatusCode finalize()   override;
  private:
    ServiceHandle<CP::ISystematicsSvc> m_systematics {"SystematicsSvc", ""};
    CP::SysListHandle m_systematicsList {this};
    CP::SysReadHandle<xAOD::TrackParticleContainer> m_tracksToMatchHandle {this, "tracksToMatch", "GSFTrackParticles_%SYS%", "Name of container with xAOD::TrackParticles to match to"};
    CP::SysReadHandle<xAOD::TrackParticleContainer> m_tracksHandle {this, "tracks", "InDetTrackParticles_%SYS%", "Name of container with xAOD::TrackParticles that will be decorated"};
    CP::SysReadSelectionHandle m_preselectionHandle {this, "preselection", "", "the preselection to apply to the matched objects"};
    CP::SysWriteSelectionHandle m_selectionHandle {this, "selectionDecoration", "matched,as_char", "the decoration for the selection, needs to be stored as char"};

    std::string m_trackAccessorDecoration;
    std::string m_trackToMatchAccessorDecoration;
    std::unique_ptr<const SG::AuxElement::Accessor<ElementLink<xAOD::TrackParticleContainer>>> m_trackAccessor;
    std::unique_ptr<const SG::AuxElement::Accessor<ElementLink<xAOD::TrackParticleContainer>>> m_trackToMatchAccessor;

    struct Counter {
      size_t nTracksToMatch = 0;
      size_t nTracksToMatchSelected = 0;
      size_t nTracks = 0;
      size_t nTracksAccepted = 0;
    };

    std::map<CP::SystematicSet, xTRT::TrackToTrackMatchAlg::Counter> m_counters;
  };
}

#endif
