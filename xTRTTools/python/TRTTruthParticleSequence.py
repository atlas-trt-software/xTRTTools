
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from AsgAnalysisAlgorithms.AnalysisObjectSharedSequence import makeSharedObjectSequence
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence

# Creates a truth particle selection sequence that cuts on pT, eta, status code and PDG ID.
# There is a possibility to enable cut-flow histograms and kinematic plots.
# Creates a shallow copy container with the selection already applied.
def makeTRTTruthParticleSelectionSequence(dataType, postfix='', maxEta=2.5, minPt=1000.,
        statusCodes=[1], pdgIds=[11,13,15,211,321], parentPdgIds=[],
        enableCutflow=True, enableKinematicHistograms=False):
    postfix = '_'+postfix if postfix else ''

    seq = AnaAlgSequence( "TRTTruthParticleSelectionSequence" )
    seq.addMetaConfigDefault ("selectionDecorNames", [])
    seq.addMetaConfigDefault ("selectionDecorNamesOutput", [])
    seq.addMetaConfigDefault ("selectionDecorCount", [])

    # kinematic cuts
    alg = createAlgorithm( 'CP::AsgSelectionAlg', 'TruthParticlePtEtaCutAlg'+postfix )
    alg.selectionDecoration = 'selectPtEta{},as_bits'.format(postfix)
    addPrivateTool( alg, 'selectionTool', 'CP::AsgPtEtaSelectionTool' )
    alg.selectionTool.maxEta = maxEta
    alg.selectionTool.minPt = minPt
    alg.selectionTool.useDressedProperties = False  # not available in TRTxAOD
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                      'selectionDecorNamesOutput' : [alg.selectionDecoration],
                      'selectionDecorCount' : [sum([alg.selectionTool.maxEta > 0., alg.selectionTool.minPt > 0.])]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])})

    # selection of status, PDG ID, etc.
    alg = createAlgorithm('CP::AsgSelectionAlg', 'TruthParticleSelectionAlg'+postfix)
    alg.selectionDecoration = 'selectTruth{},as_bits'.format(postfix)
    addPrivateTool( alg, 'selectionTool', 'xTRT::TruthParticleSelectionTool' )
    alg.selectionTool.statusCodes = statusCodes
    alg.selectionTool.pdgIds = pdgIds
    alg.selectionTool.parentPdgIds = parentPdgIds
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                      'selectionDecorNamesOutput' : [alg.selectionDecoration],
                      'selectionDecorCount' : [sum([len(alg.selectionTool.statusCodes)!=0,
                            len(alg.selectionTool.pdgIds)!=0, len(alg.selectionTool.parentPdgIds)!=0])] },
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])})

    makeSharedObjectSequence(seq, deepCopyOutput = False, shallowViewOutput = True, postfix = '_TruthParticle'+postfix,
            enableCutflow = enableCutflow, enableKinematicHistograms = enableKinematicHistograms)

    return seq