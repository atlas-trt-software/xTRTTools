testFile = '/home/cgrefe/atlas/samples/TRT/group.det-indet.mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.TRTxaod.e3601_s3446_trt119-04_EXT0/group.det-indet.22851517.EXT0._000399.xAOD.pool.root'

#override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [testFile] 

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:xTRT.ExampleAlg.outputs.root"]
svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'xTRT::ExampleAlg', 'myExampleAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
theApp.EvtMax = 500

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")