/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef xTRTTools_TrackFromVertexSelectionAlg_H
#define xTRTTools_TrackFromVertexSelectionAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgServices/ServiceHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SelectionHelpers/SysWriteSelectionHandle.h>
#include <SystematicsHandles/SysWriteDecorHandle.h>

#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/VertexContainer.h>

// C++ includes
#include <string>
#include <map>

namespace xTRT {
  class TrackFromVertexSelectionAlg : public EL::AnaAlgorithm {
  public:
    // this is a standard algorithm constructor
    TrackFromVertexSelectionAlg(const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize() override;
    virtual StatusCode execute()    override;
    virtual StatusCode finalize()   override;
  private:
    CP::SysListHandle m_systematicsList {this};
    CP::SysReadHandle<xAOD::VertexContainer> m_verticesHandle {this, "vertices", "GSFConversionVertices", "Name of vertex container."};
    CP::SysReadHandle<xAOD::TrackParticleContainer> m_tracksHandle {this, "tracks", "GSFTrackParticles", "Name of track container, to which to decorate vertex match information"};
    
    CP::SysWriteSelectionHandle m_selectionHandle {this, "vertexSelectionDecoration", "vertex_selected,as_char", "Name of the vertex selection decoration, needs to be stored as char"};
    CP::SysWriteDecorHandle<float> m_vertexChi2Decor {this, "vertexChi2Decoration", "vertex_chi2", "Name of vertex chi^2 decoration"};
    CP::SysWriteDecorHandle<unsigned> m_vertexDoFDecor {this, "vertexDoFDecoration", "vertex_DoF", "Name of vertex DoF decoration"};
    CP::SysWriteDecorHandle<TLorentzVector> m_vertexP4Decor {this, "vertexP4Decoration", "vertex_p4", "Name of vertex 4-vector decoration"};
    CP::SysWriteDecorHandle<TVector3> m_vertexPositionDecor {this, "vertexPositionDecoration", "vertex_position", "Name of vertex position decoration"};

    struct Counter {
      size_t nVertices = 0;
      size_t nTracks = 0;
      size_t nTracksSelected = 0;
    };

    std::map<CP::SystematicSet, xTRT::TrackFromVertexSelectionAlg::Counter> m_counters;
  };
}

#endif
