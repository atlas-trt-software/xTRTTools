from AthenaCommon.AppMgr import theApp
from xTRTTools.TRTTrackSequence import makeTRTTrackSelectionSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, createService

##
# These job options create TRT n-tuples with track information. Each entry corresponds to a track that passed some pre-selection.
# The expected input format is any DAOD format that contains tracks, muons and electrons. Hit level information is ignored since it is usually not available.
# If they exist, truth particles (only for MC), electron and muon objects corresponding to the selected tracks will be stored as well.
# In case of data, any single electron or single muon trigger is required to process the event.
##

############################################################################################
# Define the containers and the pre-selection for the objects to be stored in the n-tuples #
############################################################################################

# output file information
outputFileName = 'xTRTNTupleDAOD.root'
outputTreeName = 'TRT'

# the event info container to be used (only needed to extract trigger information)
eventInfoName = 'EventInfo'

# define the truth particle container
truthParticleContainer = ''

# define the list of track containers to store and their respective pre-selection
# see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsRun2R22#Track_Selection
trackContainers = ['InDetTrackParticles']
trackCutLevelDecorations = ['TightPrimary']
trackCutLevel = 'TightPrimary'
trackMinPt = 2.e3    # in MeV
trackMaxEta = 2.5    # tracker acceptance
trackMinTRTHits = 10 # any value < 0 means that no TRT hits are required

# define the electron container to be stored and its pre-selection
electronContainer = 'Electrons'              # set to '' to ignore electrons
electronWP = 'MediumLHElectron.Tight_VarRad' # electron ID and isolation WPs

# define the muon container to be stored and its pre-selection
muonContainer = 'Muons' # set to '' to ignore muons
muonWP = 'Medium.Iso'   # muon ID and isolation WPs

# definition of the trigger requirements to be applied for data (OR between all listed trigger items)
triggerListElectron = ['HLT_e24_lhmedium_L1EM20VH', 'HLT_e60_lhmedium', 'HLT_e120_lhloose', 'HLT_e24_lhtight_nod0_ivarloose',
                       'HLT_e26_lhtight_nod0_ivarloose', 'HLT_e26_lhtight_nod0', 'HLT_e60_lhmedium_nod0', 'HLT_e140_lhloose_nod0', 'HLT_e300_etcut']
triggerListMuon = ['HLT_mu20_iloose_L1MU15', 'HLT_mu40', 'HLT_mu60_0eta105_msonly', 'HLT_mu24_iloose',
                   'HLT_mu24_ivarloose', 'HLT_mu40', 'HLT_mu50', 'HLT_mu24_ivarmedium', 'HLT_mu26_ivarmedium']



########################################################
# Changes below here should typically not be necessary #
########################################################

# override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = []

# Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
#jps.AthenaCommonFlags.AccessMode = "ClassAccess"
# POOL access should be more robust across releases
jps.AthenaCommonFlags.AccessMode = "POOLAccess"

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:"+outputFileName]
svcMgr.THistSvc.MaxFileSize = -1 #speeds up jobs that output lots of histograms

# read the metadata from the input file
dataType = 'data'
if len(jps.AthenaCommonFlags.FilesInput()) > 0:
    from PyUtils import AthFile
    af = AthFile.fopen(jps.AthenaCommonFlags.FilesInput()[0]) # opens the first file from the FilesInput list

    # extract the datatype from the metadata of the first file to process
    if 'IS_SIMULATION' in af.fileinfos['evt_type']:
        if 'ATLFASTII' in af.fileinfos['metadata']['/Simulation/Parameters']['SimulationFlavour'].upper():
            dataType = 'afii'
        else:
            dataType = 'mc'

# Set up the systematics loader/handler service:
sysService = createService( 'CP::SystematicsSvc', 'SystematicsSvc', sequence = athAlgSeq )
sysService.sigmaRecommended = 1



####################################
# Sequence of algorithms to be run #
####################################

# Include and set up the pileup analysis sequence:
from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
from AsgAnalysisAlgorithms.AsgAnalysisAlgorithmsTest import pileupConfigFiles
prwfiles, lumicalcfiles = pileupConfigFiles( dataType )
pileupSequence = makePileupAnalysisSequence(dataType, userPileupConfigs=[], userLumicalcFiles=[])
pileupSequence.configure( inputName = "EventInfo", outputName = "" )
athAlgSeq += pileupSequence

# apply the trigger selection
if dataType == 'data':
    alg = createAlgorithm('TriggerSelectionAlg', 'TriggerSelectionAlg')
    alg.EventInfoName = eventInfoName
    alg.TriggerList = triggerListElectron + triggerListMuon
    athAlgSeq += alg

# Electron selection using the default recommendations
selectedElectrons = ''
if electronContainer:
    from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import makeElectronAnalysisSequence
    electronSequence = makeElectronAnalysisSequence(dataType, electronWP, postfix = 'selected', recomputeLikelihood=False, shallowViewOutput = False)
    electronSequence.configure(inputName = electronContainer, outputName = 'SelectedElectrons_%SYS%')
    selectedElectrons = 'SelectedElectrons_NOSYS'
    athAlgSeq += electronSequence

# Muon selection using the default recommendations
selectedMuons = ''
if muonContainer:
    from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
    muonSequence = makeMuonAnalysisSequence(dataType, muonWP, postfix = 'selected', shallowViewOutput = False, isRun3Geo=True)
    muonSequence.configure( inputName = muonContainer, outputName = 'SelectedMuons_%SYS%' )
    selectedMuons = 'SelectedMuons_NOSYS'
    athAlgSeq += muonSequence

# preselection for tracks to store - allow for multiple track containers
selectedTrackContainers = []
for trackContainer in trackContainers:
    seq = makeTRTTrackSelectionSequence(dataType, postfix=trackContainer,
                                        maxEta=trackMaxEta, minPt=trackMinPt, cutLevel=trackCutLevel,
                                        minNTRTHits=trackMinTRTHits, cutLevelDecorations=trackCutLevelDecorations,
                                        enableCutflow=True, enableKinematicHistograms=False,)
    selectedTrackContainers.append(trackContainer+'_selected')
    seq.configure(inputName = trackContainer, outputName = trackContainer+'_selected')
    athAlgSeq += seq

# set up the algorithm to fill the ROOT tree
trackWriter = createAlgorithm( 'xTRT::TRTTrackWriterAlg', 'TrackWriter' )
trackWriter.isMC = dataType in [ 'mc', 'afii' ]
trackWriter.treeName = outputFileName
trackWriter.storeHits = False
trackWriter.truthContainerName = truthParticleContainer
trackWriter.electronContainerName = electronContainer
trackWriter.muonContainerName = selectedMuons
trackWriter.trackContainerNames = selectedTrackContainers
trackWriter.trackPrefixes = [container+'_' for container in trackContainers]
# store only those electron and muon objects that have a corresponding selected track object
trackWriter.storeOnlyObjectsMatchedToTracks = True
trackWriter.storeElectronGsfTracks = True
# add branches to store the track quality cut result
for decoration in trackCutLevelDecorations:
    trackWriter.trackVariables.append("<char> {} -> <bool> {}".format(decoration, decoration))
trackWriter.eventInfoVariables = list(filter(lambda entry: 'TRTOcc' not in entry, trackWriter.eventInfoVariables))
athAlgSeq += trackWriter

#############################
# End of algorithm sequence #
#############################



MessageSvc.defaultLimit=100000
# limit the number of events (for testing purposes)
#from AthenaCommon.AppMgr import ServiceMgr
#ServiceMgr.EventSelector.SkipEvents = 10
theApp.EvtMax = -1

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")