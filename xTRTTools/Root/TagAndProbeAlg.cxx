/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/TagAndProbeAlg.h>

#include <xAODBase/IParticleHelpers.h>
#include "xAODEgamma/ElectronxAODHelpers.h"
#include <AthContainers/ConstDataVector.h>
#include <AsgMessaging/MessageCheck.h>

#include <iomanip>
#include <sstream>

xTRT::TagAndProbeAlg::TagAndProbeAlg(const std::string& name, ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator) {

  declareProperty("massWindowLow", m_massWindowLow = 0., "Lower boundary for invariant mass window");
  declareProperty("massWindowHigh", m_massWindowHigh = 0., "Upper boundary for invariant mass window");
  declareProperty("maxDeltaZ0", m_maxDeltaZ0 = 0., "Maximum difference in z0 between tag and probe");
  declareProperty("oppositeCharge", m_oppositeCharge = true, "Select only tag and probe combinations with opposite charge");
}

StatusCode xTRT::TagAndProbeAlg::initialize() {

  // initialise all handles
  ANA_CHECK (m_tagHandle.initialize(m_systematicsList));
  ANA_CHECK (m_probeHandle.initialize(m_systematicsList));
  ANA_CHECK (m_selectionHandle.initialize(m_systematicsList, m_probeHandle));
  ANA_CHECK (m_p4Decor.initialize(m_systematicsList, m_probeHandle));
  ANA_CHECK (m_deltaZ0Decor.initialize(m_systematicsList, m_probeHandle));
  ANA_CHECK (m_qxqDecor.initialize(m_systematicsList, m_probeHandle));
  ANA_CHECK (m_systematicsList.initialize());

return StatusCode::SUCCESS;
}

StatusCode xTRT::TagAndProbeAlg::execute() {
  bool result = false;

  for (const auto& sys : m_systematicsList.systematicsVector()) {
    // keep track of the number of selected objects
    auto& counter = m_counters[sys];

    const xAOD::IParticleContainer* tagParticles = nullptr;
    ANA_CHECK (m_tagHandle.retrieve(tagParticles, sys));

    const xAOD::IParticleContainer* probeParticles = nullptr;
    ANA_CHECK (m_probeHandle.retrieve(probeParticles, sys));

    const TLorentzVector nullP4(0., 0., 0., 0.);
    for (const auto& probeParticle : *probeParticles) {
      m_selectionHandle.setBool(*probeParticle, false, sys);
      m_p4Decor.set(*probeParticle, nullP4, sys);
      m_deltaZ0Decor.set(*probeParticle, 0., sys);
      m_qxqDecor.set(*probeParticle, 0, sys);
    }

    for (const auto& tagParticle : *tagParticles) {
      auto tagTrack = getTrack(tagParticle);
      if (tagTrack == nullptr) continue;

      counter.nTags++;

      for (const auto& probeParticle : *probeParticles) {
        // check if we have a valid track object and it is not based on the same track as the tag object
        auto probeTrack = getTrack(probeParticle);
        if (probeTrack == nullptr or probeTrack == tagTrack) {
          continue;
        }

        result = true;
        counter.nProbes++;
        
        const int qxq = tagTrack->charge() * probeTrack->charge();
        if (m_oppositeCharge) {
          result = qxq < 0.;
          if (result) counter.nOppositeCharge++;
        }
        if (!result) continue;

        const TLorentzVector p4 = tagParticle->p4() + probeParticle->p4();
        if (m_massWindowLow > 0. or m_massWindowHigh > 0.) {
          result = true;
          if (m_massWindowLow > 0.) result = p4.M() >= m_massWindowLow;
          if (m_massWindowHigh > 0.) result &= p4.M() <= m_massWindowHigh;
          if (result) counter.nMassWindow++;
        }
        if (!result) continue;

        const float deltaZ0 = std::abs(tagTrack->z0() - probeTrack->z0());
        if (m_maxDeltaZ0 > 0.) {
          result = deltaZ0 <= m_maxDeltaZ0;
          if (result) counter.nMaxDeltaZ0++;
        }

        if (!result) continue;

        m_selectionHandle.setBool(*probeParticle, result, sys);
        m_p4Decor.set(*probeParticle, p4, sys);
        m_deltaZ0Decor.set(*probeParticle, deltaZ0, sys);
        m_qxqDecor.set(*probeParticle, qxq, sys);
      }
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode xTRT::TagAndProbeAlg::finalize() {
  // determine number of bins for cutflow histograms
  size_t nBins = 2;
  size_t indexOppositeCharge = 0, indexMassWindow = 0, indexMaxDeltaZ0 = 0;
  if (m_oppositeCharge) {
    nBins++;
    indexOppositeCharge = nBins;
  }
  if (m_massWindowLow > 0. or m_massWindowHigh > 0) {
    nBins++;
    indexMassWindow = nBins;
  }
  if (m_maxDeltaZ0 > 0) {
    nBins++;
    indexMaxDeltaZ0 = nBins;
  }

  // fill the cutflow histograms
  for (const auto& [sys, counter] : m_counters) {
    std::string histName;
    ANA_CHECK(m_systematicsList.service().makeSystematicsName(histName, "cflow_"+this->name()+"_%SYS%", sys))
    ANA_CHECK (book (TH1F (histName.c_str(), histName.c_str(), nBins, 0, nBins)));
    auto histogram = hist(histName);
    histogram->GetXaxis()->SetBinLabel(1, "N tags");
    for (size_t i = 0; i < counter.nTags; i++) {
      histogram->Fill(0);
    }
    histogram->GetXaxis()->SetBinLabel(2, "N probes");
    for (size_t i = 0; i < counter.nProbes; i++) {
      histogram->Fill(1);
    }
    if (m_oppositeCharge) {
      histogram->GetXaxis()->SetBinLabel(indexOppositeCharge, "Opposite charge");
      for (size_t i = 0; i < counter.nOppositeCharge; i++) {
        histogram->Fill(indexOppositeCharge-1, 1);
      }
    }
    if (m_massWindowLow > 0. or m_massWindowHigh > 0.) {
      std::stringstream binLabel;
      binLabel << std::defaultfloat;
      if (m_massWindowLow > 0. and m_massWindowHigh > 0.) {
        binLabel << m_massWindowLow*1.e-3 << " < M < " << m_massWindowHigh*1.e-3 << " GeV";
      } else if (m_massWindowLow > 0) {
        binLabel << "M > "<< m_massWindowLow*1.e-3 << " GeV";
      } else {
        binLabel << "M < "<< m_massWindowHigh*1.e-3 << " GeV";
      }
      histogram->GetXaxis()->SetBinLabel(indexMassWindow, binLabel.str().c_str());
      for (size_t i = 0; i < counter.nMassWindow; i++) {
        histogram->Fill(indexMassWindow-1);
      }
    }
    if (m_maxDeltaZ0) {
      std::stringstream binLabel;
      binLabel << "#Delta z_0 < " << std::defaultfloat << m_maxDeltaZ0 << " mm";
      histogram->GetXaxis()->SetBinLabel(indexMaxDeltaZ0, binLabel.str().c_str());
      for (size_t i = 0; i < counter.nMaxDeltaZ0; i++) {
        histogram->Fill(indexMaxDeltaZ0-1);
      }
    }
  }

  return StatusCode::SUCCESS;
}

const xAOD::TrackParticle* xTRT::TagAndProbeAlg::getTrack(const xAOD::IParticle* particle) {
  const xAOD::Type::ObjectType type = particle->type();
  const xAOD::TrackParticle* track = nullptr;
  if (type == xAOD::Type::TrackParticle) {
    track = dynamic_cast<const xAOD::TrackParticle*>(particle);
  } else if (type == xAOD::Type::Electron) {
    auto electron = dynamic_cast<const xAOD::Electron*>(particle);
    track = xAOD::EgammaHelpers::getOriginalTrackParticle(electron);
  } else if (type == xAOD::Type::Muon) {
    auto muon = dynamic_cast<const xAOD::Muon*>(particle);
    track = muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
  }
  return track;
}
