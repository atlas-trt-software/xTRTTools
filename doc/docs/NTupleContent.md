# Content of TRT n-tuples

This is a brief description of the branches that are available in
TRT n-tuples (depending on the job options).

The variables are either copied directly from corresponding information
in the input xAOD files or calculated in the [TreeFiller class](../../xTRTTools/Root/TreeFiller.cxx).
That class also defines the list of variables that are stored by default.

## Event level variables
Since each entry corresponds to a single track or particle, the event
level variables are possibly duplicated. This allows to correlate
tracks at the n-tuple level if needed.
- `runNumber`: the run number. In MC this number defines the campaign.
- `eventNumber`: the event number in the sample.
- `averageInteractionsPerCrossing`: the amount of pile-up for this event.
- `eventWeight`: always 1 for data. In MC this value is used to correct the relative contributions of different phase space regions
- `TRTOccupancy*`: straw occupancy in the respective section of the TRT.
- `beamSpot`: the beam spot position (`TVector3`). Used as reference point in the track parametrisation.
- `primaryVertex_sumPt2`: the squared sum of all track transverse momenta associated to the primary vertex.
- `primaryVertex_position`: position of the primary vertex (`TVector3`), i.e. the vertex with the highest sum of associated track transverse momenta.

## Truth particle variables
Truth particles which were associated to the corresponding track, electron
or muon objects using the `TruthParticleLink`. If no matching truth particle
was found all values are set to 0. This can be used to identify fake tracks.

All branches start with the prefix `truth_`.
- `status`: the generator status code.
- `pdgId`: the PDG code that identifies the particle type (see [definition](https://pdg.lbl.gov/2022/reviews/rpp2022-rev-mc-nu-event-gen.pdf)).
- `p4`: the 4-momentum (`TLorentzVector`).
- `prodVertex_position`: origin of the particle (`TVector3`)
- `parentPdgID`: the PDG code of the parent particle (see [definition](https://pdg.lbl.gov/2022/reviews/rpp2022-rev-mc-nu-event-gen.pdf)).

## Track variables
Selected tracks that have passed the pre-selection. There can be multiple
selected track containers that are stored in parallel. In case there is no
matching reconstructed track for a given truth particle all values are set
to 0. This can be used to determine the track reconstruction efficiency.

All branches typically start with the original track container name, e.g.
`InDetTrackParticles_` or `GSFTrackParticles_`. The GSF track objects can
also be stored via the electron object, which results in branches starting
with `electron_gsf_`.
- `nPixelHits`: the number of pixel hits on the track.
- `nPixelHoles`: the number of pixel holes on the track (track passes a module without a hit).
- `nSCTHits`: the number of SCT hits on the track.
- `nSCTHoles`: the number of SCT holes on the track (track passes a module without a hit).
- `nTRTHits`: the number of TRT hits on the track (precision and tube hits, excludes outliers. See [definition](https://twiki.cern.ch/twiki/bin/view/Atlas/TrtSoftwareDefinitions#Hit_types)).
- `nTRTHits_athena`: the number of TRT hits on the track as defined in the reconstruction in Athena.
- `nTRTHitsXe`: the number of TRT hits in Xe straws on the track. This explicitely includes outliers.
- `nTRTHoles`: the number of TRT holes on the track (track passes a straw without a hit, see [definition](https://twiki.cern.ch/twiki/bin/view/Atlas/TrtSoftwareDefinitions#Hit_types)).
- `nTRTOutliers`: the number of TRT hits associated to the track where the track misses the straw (see [definition](https://twiki.cern.ch/twiki/bin/view/Atlas/TrtSoftwareDefinitions#Hit_types)).
- `nTRTOutliers_athena`: the number of TRT hits associated to the track where the track misses the straw as defined in the reconstruction in Athena.
- `nTRTHitsHT`: the number of TRT hits on the track that passed the high threshold middle bit.
- `nTRTHitsHTXe`: the number of TRT hits in Xe straws on the track that passed the high threshold middle bit.
- `nTRTHitsPrecision`: the number of TRT hits with a good match of drift circle with the track (see [definition](https://twiki.cern.ch/twiki/bin/view/Atlas/TrtSoftwareDefinitions#Hit_types)).
- `qOverP`: ratio of charge to particle momentum. The sign can be used to determine the charge.
- `d0`: the distance of the point of closest approach in the transverse plane to the reference point (beamspot).
- `z0`: the longitudinal distance of the point of closest approach in the transverse plane to the reference point (beamspot).
- `d0sig`: d0 divided by its uncertainty.
- `z0sig`: z0 divided by its uncertainty.
- `z0sinTheta`: z0 times the sine of the polar angle of the track.
- `dEdx`: the mean energy loss over all TRT straws along the track. Determined from the time-over-threshold measurements in each hit.
- `dEdx_usedHits`: number of hits used in the dE/dx calculation.
- `dEdx_noHT`: the mean energy loss over all TRT straws along the track. Determined from the time-over-threshold measurements in each hit, excluding high-threshold hits.
- `dEdx_noHT_ usedHits`: number of hits used in the dE/dx calculation, excluding high-threshold hits.
- `eProbabilityHT`: electron classification likelihood determined from the per-straw probability using the HT measurement, active gas, etc. 
- `eProbabilityNN`: electron classification score from a neural net that uses track-level and hit-level input variables 
- `trackOccupancy`: the average weighted TRT module occupancy along the trajectory.
- `chi2`: the chi^2 value of the track fit.
- `Loose`: track passed the [Loose working point](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsRun2R22#Track_Selection) of the [InDetTrackSelectionTool](https://gitlab.cern.ch/atlas/athena/-/tree/master/InnerDetector/InDetRecTools/InDetTrackSelectionTool).
- `LoosePrimary`: track passed the [LoosePrimary working point](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsRun2R22#Track_Selection) of the [InDetTrackSelectionTool](https://gitlab.cern.ch/atlas/athena/-/tree/master/InnerDetector/InDetRecTools/InDetTrackSelectionTool).
- `TightPrimary`: track passed the [TightPrimary working point](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsRun2R22#Track_Selection) of the [InDetTrackSelectionTool](https://gitlab.cern.ch/atlas/athena/-/tree/master/InnerDetector/InDetRecTools/InDetTrackSelectionTool).
- `truthMatchProbability`: the weighted fraction of hits originating of the truth particle with the highest truthMatchProbability.
- `p4`: the 4-momentum (`TLorentzVector`).
- `charge`: the charge of the track.
- `sumL`: the sum of all track segment lengths within TRT straws.

The following variables are only stored in the tag-and-probe n-tuples for all `InDetTrackParticles` objects.
- `matched_{electron/muon}`: the track belongs to a good reconstructed electron or muon
- `tnp_{Z/JPsi}_{electron/muon}`: passed the Z or J/Psi tag-and-probe selection with an electron or muon tag.
- `tnp_{Z/JPsi}_{electron/muon}_p4`: the 4-momentum of the tag-and-probe system (`TLorentzVector`).
- `tnp_{Z/JPsi}_{electron/muon}_deltaZ0`: difference in `z0` between tag and probe tracks.
- `tnp_{Z/JPsi}_{electron/muon}_qxq`: charge product of the tag and the probe.
- `{V0Kshort/V0Lambda/V0Lambdabar}`: track belongs to a reconstructed Kshort, Lambda or Lambdabar vertex
- `{V0Kshort/V0Lambda/V0Lambdabar}_p4`: the 4-vector of all tracks belonging to the Kshort, Lambda or Lambdabar vertex (`TLorentzVector`).
- `{V0Kshort/V0Lambda/V0Lambdabar}_position`: postition of the secondary vertex (`TVector3`).
- `{V0Kshort/V0Lambda/V0Lambdabar}_chi2`: chi^2 of the secondary vertex fit.
- `{V0Kshort/V0Lambda/V0Lambdabar}_dof`: number of degrees of freedon of the secondary vertex fit.

The following variables are only stored in the tag-and-probe n-tuples for all `GSFTrackParticles` objects.
- `conversion`: the track belongs to a reconstructed conversion vertex
- `conversion_p4`: the 4-vector of all tracks belonging to the conversion vertex (`TLorentzVector`).
- `conversion_position`: postition of the conversion vertex (`TVector3`).
- `conversion_chi2`: chi^2 of the conversion vertex fit.
- `conversion_dof`: number of degrees of freedon of the conversion vertex fit.


## Hit variables
TRT hit level variables are stored in the form of `std::vector` objects along
with the corresponding track variables. They are either intrinsic quantities of
the straw and the signal that was read out, like drift time, bit pattern, straw
position, (corresponding to `DriftCircle` objects), or they are determined from
measurement when included in the track fit, like track-to-wire distance,
(corresponding to `MeasurementOnSurface` objects).

The branches start with the original track container name followed by `hit_`,
e.g. `InDetTrackParticles_hit_`.
- `type`: hit type depending on the position wrt. the track (0: precision hit, 1: tube hit, 2: outlier, see [definition](https://twiki.cern.ch/twiki/bin/view/Atlas/TrtSoftwareDefinitions#Hit_types)).
- `type_athena`: hit type depending on the position wrt. the track as defined in the reconstruction in Athena (0: precision or tube hit, 5: outlier, see [definition](https://twiki.cern.ch/twiki/bin/view/Atlas/TrtSoftwareDefinitions#Hit_types)).
- `r`: the radius of the hit position.
- `z`: the z position of the hit.
- `localTheta`: polar angle of the hit in the local coordinate system of the straw.
- `localPhi`: azimuthal angle of the hit in the local coordinate system of the straw.
- `rTrackToWire`: closest distance of the track to the wire at the centre of the straw.
- `unbiasedResidual`: distance between drift circle and track when excluding this hit from the track fit.
- `biasedResidual`: distance between drift circle and track when including this hit in the track fit.
- `unbiasedPull`: unbiased residual divided by the track uncertainty at the hit position.
- `biasedPull`: biased residual divided by the track uncertainty at the hit position.
- `error`: the size of the hit uncertainty, determined by the drift time.
- `isShared`: the hit is shared by multiple reconstructed tracks.
- `gasType`: identifier for the active gas mixture (0: Xe, 1: Ar).
- `bec`: identifier for the TRT sub-detector (-2: endcap C, -1: barrel C, 1: barrel A, 2 endcap A).
- `layer`: the layer (barrel) or wheel (endcap) number in the respective TRT sub-detector.
- `strawLayer`: the straw layer number in the respective layer (barrel) or wheel (endcap).
- `strawNumber`: the straw number in the respective straw layer.
- `board`: the identifier of the readout board.
- `chip`: the identifier of the readout chip.
- `modulePhi`: the azimuthal angle of the TRT module.
- `bitPattern`: the full 24-bit pattern stored as a 32-bit integer.
- `strawPhi`: the azimuthal angle of the straw.
- `t0`: the t0 offset used in the calculation of the drift time
- `leadingEdge`: the time bin number of the identified leading edge.
- `driftTime`: the drift time, determined from the leading edge and t0.
- `driftRadius`: the calibrated drift radius determined from the drift time.
- `tot`: the time over threshold measured in number of time bins.
- `isHighThresholdMiddleBit`: true if the middle high threshold bit was set.
- `isPrecision`: true if the drift circle matches the track trajectory in the straw (see [definition](https://twiki.cern.ch/twiki/bin/view/Atlas/TrtSoftwareDefinitions#Hit_types)).
- `strawPosition`: position of the straw (`TVector3`).
- `l`: length of the track segement in the straw.
- `driftTimeCorrected`: the calibrated drift time including time-over-thrshold and high-threshold corrections.

## Electron variables
Selected electrons that were associated to corresponding truth particles or
tracks. If no corresponding electron is found all values are set to 0.

The branches start with `electron_`.
- `LHLoose`: true if electron passes the LHLoose identification.
- `LHMedium`: true if electron passes the LHMedium identification.
- `LHTight`: true if electron passes the LHTight identification.
- `Loose`: true if electron passes the Loose identification.
- `Medium`: true if electron passes the Medium identification.
- `Tight`: true if electron passes the Tight identification.
- `charge`: the charge of the electron.
- `ptcone20`: amount of track transverse momentum reconstructed in a cone with R=0.2 around the electron. Used to determine isolation.
- `ptcone30`: amount of track transverse momentum reconstructed in a cone with R=0.3 around the electron. Used to determine isolation.
- `ptvarcone20`: amount of track transverse momentum reconstructed in a cone with R=0.2 around the electron that decreases with increasing electron momentum. Used to determine isolation.
- `ptvarcone30`: amount of track transverse momentum reconstructed in a cone with R=0.3 around the electron that decreases with increasing electron momentum. Used to determine isolation.
- `topoetcone20`: amount of topo cluster energy in a cone with R=0.2 around the electron. Used to determine isolation.
- `topoetcone30`: amount of topo cluster energy in a cone with R=0.3 around the electron. Used to determine isolation.
- `topoetcone40`: amount of topo cluster energy in a cone with R=0.4 around the electron. Used to determine isolation.
- `p4`: the 4-momentum (`TLorentzVector`).

## Muon variables
Selected muons that were associated to corresponding truth particles or
tracks. If no corresponding muon is found all values are set to 0.

The branches start with `muon_`.
- `author`: identifies the muon reconstruction algorithm.
- `type`: identifies the muon type.
- `quality`: identifies the muon identification quality.
- `charge`: the charge of the muon.
- `ptcone20`: amount of track transverse momentum reconstructed in a cone with R=0.2 around the muon. Used to determine isolation.
- `ptcone30`: amount of track transverse momentum reconstructed in a cone with R=0.3 around the muon. Used to determine isolation.
- `ptcone40`: amount of track transverse momentum reconstructed in a cone with R=0.4 around the muon. Used to determine isolation.
- `ptvarcone20`: amount of track transverse momentum reconstructed in a cone with R=0.2 around the muon that decreases with increasing muon momentum. Used to determine isolation.
- `ptvarcone30`: amount of track transverse momentum reconstructed in a cone with R=0.3 around the muon that decreases with increasing muon momentum. Used to determine isolation.
- `ptvarcone40`: amount of track transverse momentum reconstructed in a cone with R=0.4 around the muon that decreases with increasing muon momentum. Used to determine isolation.
- `topoetcone20`: amount of topo cluster energy in a cone with R=0.2 around the muon. Used to determine isolation.
- `topoetcone30`: amount of topo cluster energy in a cone with R=0.3 around the muon. Used to determine isolation.
- `topoetcone40`: amount of topo cluster energy in a cone with R=0.4 around the muon. Used to determine isolation.
- `numberOfPrecisisonLayers`: number of precision layers used to reconstruct the muon.
- `p4`: the 4-momentum (`TLorentzVector`).
