#include <xTRTTools/Helpers.h>
#include <xTRTTools/Accessors.h>
#include <xTRTTools/HitSummary.h>
#include <xTRTTools/BranchProxy.h>
#include <xTRTTools/ExampleAlg.h>
#include <xTRTTools/TRTTruthWriterAlg.h>
#include <xTRTTools/AsgLeptonTrackSelectionAlg.h>
#include <xTRTTools/TagAndProbeAlg.h>
#include <xTRTTools/TrackFromVertexSelectionAlg.h>
#include <xTRTTools/TrackToLeptonMatchAlg.h>
#include <xTRTTools/TrackToTrackMatchAlg.h>
#include <xTRTTools/TrackWriterTool.h>
#include <xTRTTools/TruthParticleSelectionTool.h>

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ namespace xTRT;
#pragma link C++ namespace xTRT::Acc;
#pragma link C++ class xTRT::HitSummary+;
#pragma link C++ class xTRT::BranchProxy+;
#pragma link C++ class xTRT::ExampleAlg+;
#pragma link C++ class xTRT::TRTTrackWriterAlg+;
#pragma link C++ class xTRT::AsgLeptonTrackSelectionAlg+;
#pragma link C++ class xTRT::TagAndProbeAlg+;
#pragma link C++ class xTRT::TrackFromVertexSelectionAlg+;
#pragma link C++ class xTRT::TrackToLeptonMatchAlg+;
#pragma link C++ class xTRT::TrackToTrackMatchAlg+;
#pragma link C++ class xTRT::TrackWriterTool+;
#pragma link C++ class xTRT::TruthParticleSelectionTool+;

#endif
