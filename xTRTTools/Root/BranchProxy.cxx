/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/BranchProxy.h>

#include <regex>
#include <type_traits>
#include "TVector3.h"
#include "TLorentzVector.h"

namespace xTRT {
    IBranchProxy* BranchProxyFactory::setupBranch(TTree* tree, const std::string& expression, const std::string& prefix) {
        std::string accessorName, branchName, accessorType, branchType;
        analyseExpression(expression, accessorType, accessorName, branchType, branchName);
        if (not prefix.empty()) {
            branchName = prefix+branchName;
        }
        // cases where accessor and branch share the type
        if (branchType.empty() or branchType == accessorType) {
            if (accessorType == "bool") {
                return new BranchProxy<bool>(tree, accessorName, branchName);
            } else if (accessorType == "uchar") {
                return new BranchProxy<unsigned char>(tree, accessorName, branchName);
            } else if (accessorType == "char") {
                return new BranchProxy<char>(tree, accessorName, branchName);
            } else if (accessorType == "uint") {
                return new BranchProxy<uint>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new BranchProxy<int>(tree, accessorName, branchName);
            } else if (accessorType == "ushort") {
                return new BranchProxy<ushort>(tree, accessorName, branchName);
            } else if (accessorType == "short") {
                return new BranchProxy<short>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new BranchProxy<unsigned long long>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new BranchProxy<long long>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new BranchProxy<float>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new BranchProxy<double>(tree, accessorName, branchName);
            } else if (accessorType == "TVector3") {
                return new BranchProxy<TVector3>(tree, accessorName, branchName);
            } else if (accessorType == "TLorentzVector") {
                return new BranchProxy<TLorentzVector>(tree, accessorName, branchName);
            }
        }
        // create useful combinations of accessor and branch types
        // only allow to save space not convert to types that store more information that is available
        if (branchType == "bool") {
            if (accessorType == "uchar") {
                return new BranchProxy<unsigned char, bool>(tree, accessorName, branchName);
            } else if (accessorType == "char") {
                return new BranchProxy<char, bool>(tree, accessorName, branchName);
            } else if (accessorType == "uint") {
                return new BranchProxy<uint, bool>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new BranchProxy<int, bool>(tree, accessorName, branchName);
            } else if (accessorType == "ushort") {
                return new BranchProxy<ushort, bool>(tree, accessorName, branchName);
            } else if (accessorType == "short") {
                return new BranchProxy<short, bool>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new BranchProxy<unsigned long long, bool>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new BranchProxy<long long, bool>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new BranchProxy<float, bool>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new BranchProxy<double, bool>(tree, accessorName, branchName);
            }
        } else if (branchType == "char") {
            if (accessorType == "uchar") {
                return new BranchProxy<unsigned char, char>(tree, accessorName, branchName);
            } else if (accessorType == "uint") {
                return new BranchProxy<uint, char>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new BranchProxy<int, char>(tree, accessorName, branchName);
            } else if (accessorType == "ushort") {
                return new BranchProxy<ushort, char>(tree, accessorName, branchName);
            } else if (accessorType == "short") {
                return new BranchProxy<short, char>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new BranchProxy<unsigned long long, char>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new BranchProxy<long long, char>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new BranchProxy<float, char>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new BranchProxy<double, char>(tree, accessorName, branchName);
            }
        } else if (branchType == "uint") {
            if (accessorType == "int") {
                return new BranchProxy<int, uint>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new BranchProxy<unsigned long long, uint>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new BranchProxy<long long, uint>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new BranchProxy<float, uint>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new BranchProxy<double, uint>(tree, accessorName, branchName);
            }
        } else if (branchType == "int") {
            if (accessorType == "ulong") {
                return new BranchProxy<unsigned long long, int>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new BranchProxy<long long, int>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new BranchProxy<float, int>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new BranchProxy<double, int>(tree, accessorName, branchName);
            }
        } else if (branchType == "ushort") {
            if (accessorType == "uchar") {
                return new BranchProxy<unsigned char, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "char") {
                return new BranchProxy<char, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "short") {
                return new BranchProxy<short, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "uint") {
                return new BranchProxy<uint, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new BranchProxy<int, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new BranchProxy<unsigned long long, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new BranchProxy<long long, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new BranchProxy<float, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new BranchProxy<double, ushort>(tree, accessorName, branchName);
            }
        } else if (branchType == "short") {
            if (accessorType == "uint") {
                return new BranchProxy<uint, short>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new BranchProxy<int, short>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new BranchProxy<unsigned long long, short>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new BranchProxy<long long, short>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new BranchProxy<float, short>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new BranchProxy<double, short>(tree, accessorName, branchName);
            }
        } else if (branchType == "ulong") {
            if (accessorType == "long") {
                return new BranchProxy<long, unsigned long long>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new BranchProxy<float, unsigned long long>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new BranchProxy<double, unsigned long long>(tree, accessorName, branchName);
            }
        } else if (branchType == "long") {
            if (accessorType == "float") {
                return new BranchProxy<float, long long>(tree, accessorName, branchName);
            }  else if (accessorType == "double") {
                return new BranchProxy<double, long long>(tree, accessorName, branchName);
            }
        } else if (branchType == "float") {
            if (accessorType == "double") {
                return new BranchProxy<double, float>(tree, accessorName, branchName);
            }
        }
        std::cerr << "Invalid accessor type \"" << accessorType << "\" found in expression \"" << expression << "\"" << std::endl;
        return nullptr;
    }

    IBranchProxy* BranchProxyFactory::setupVectorBranch(TTree* tree, const std::string& expression, const::std::string& prefix) {
        std::string accessorName, branchName, accessorType, branchType;
        analyseExpression(expression, accessorType, accessorName, branchType, branchName);
        if (not prefix.empty()) {
            branchName = prefix+branchName;
        }
        // cases where accessor and branch share the type
        if (branchType.empty() or (branchType == accessorType)) {
            if (accessorType == "bool") {
                return new VectorBranchProxy<bool>(tree, accessorName, branchName);
            } else if (accessorType == "uchar") {
                return new VectorBranchProxy<unsigned char>(tree, accessorName, branchName);
            } else if (accessorType == "char") {
                return new VectorBranchProxy<char>(tree, accessorName, branchName);
            } else if (accessorType == "uint") {
                return new VectorBranchProxy<uint>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new VectorBranchProxy<int>(tree, accessorName, branchName);
            } else if (accessorType == "ushort") {
                return new VectorBranchProxy<ushort>(tree, accessorName, branchName);
            } else if (accessorType == "short") {
                return new VectorBranchProxy<short>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new VectorBranchProxy<unsigned long long>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new VectorBranchProxy<long long>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new VectorBranchProxy<float>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new VectorBranchProxy<double>(tree, accessorName, branchName);
            } else if (accessorType == "TVector3") {
                return new VectorBranchProxy<TVector3>(tree, accessorName, branchName);
            } else if (accessorType == "TLorentzVector") {
                return new VectorBranchProxy<TLorentzVector>(tree, accessorName, branchName);
            }
        }
        // create useful combinations of accessor and branch types
        // only allow to save space not convert to types that store more information that is available
        if (branchType == "bool") {
            if (accessorType == "uchar") {
                return new VectorBranchProxy<unsigned char, bool>(tree, accessorName, branchName);
            } else if (accessorType == "char") {
                return new VectorBranchProxy<char, bool>(tree, accessorName, branchName);
            } else if (accessorType == "uint") {
                return new VectorBranchProxy<uint, bool>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new VectorBranchProxy<int, bool>(tree, accessorName, branchName);
            } else if (accessorType == "ushort") {
                return new VectorBranchProxy<ushort, bool>(tree, accessorName, branchName);
            } else if (accessorType == "short") {
                return new VectorBranchProxy<short, bool>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new VectorBranchProxy<unsigned long long, bool>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new VectorBranchProxy<long long, bool>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new VectorBranchProxy<float, bool>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new VectorBranchProxy<double, bool>(tree, accessorName, branchName);
            }
        } else if (branchType == "char") {
            if (accessorType == "uchar") {
                return new VectorBranchProxy<unsigned char, char>(tree, accessorName, branchName);
            }else if (accessorType == "uint") {
                return new VectorBranchProxy<uint, char>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new VectorBranchProxy<int, char>(tree, accessorName, branchName);
            } else if (accessorType == "ushort") {
                return new VectorBranchProxy<ushort, char>(tree, accessorName, branchName);
            } else if (accessorType == "short") {
                return new VectorBranchProxy<short, char>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new VectorBranchProxy<unsigned long long, char>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new VectorBranchProxy<long long, char>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new VectorBranchProxy<float, char>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new VectorBranchProxy<double, char>(tree, accessorName, branchName);
            }
        } else if (branchType == "uint") {
            if (accessorType == "int") {
                return new VectorBranchProxy<int, uint>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new VectorBranchProxy<unsigned long long, uint>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new VectorBranchProxy<long long, uint>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new VectorBranchProxy<float, uint>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new VectorBranchProxy<double, uint>(tree, accessorName, branchName);
            }
        } else if (branchType == "int") {
            if (accessorType == "ulong") {
                return new VectorBranchProxy<unsigned long long, int>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new VectorBranchProxy<long long, int>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new VectorBranchProxy<float, int>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new VectorBranchProxy<double, int>(tree, accessorName, branchName);
            }
        } else if (branchType == "ushort") {
            if (accessorType == "uchar") {
                return new VectorBranchProxy<unsigned char, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "char") {
                return new VectorBranchProxy<char, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "short") {
                return new VectorBranchProxy<short, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "uint") {
                return new VectorBranchProxy<uint, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new VectorBranchProxy<int, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new VectorBranchProxy<unsigned long long, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new VectorBranchProxy<long long, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new VectorBranchProxy<float, ushort>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new VectorBranchProxy<double, ushort>(tree, accessorName, branchName);
            }
        } else if (branchType == "short") {
            if (accessorType == "uint") {
                return new VectorBranchProxy<uint, short>(tree, accessorName, branchName);
            } else if (accessorType == "int") {
                return new VectorBranchProxy<int, short>(tree, accessorName, branchName);
            } else if (accessorType == "ulong") {
                return new VectorBranchProxy<unsigned long long, short>(tree, accessorName, branchName);
            } else if (accessorType == "long") {
                return new VectorBranchProxy<long long, short>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new VectorBranchProxy<float, short>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new VectorBranchProxy<double, short>(tree, accessorName, branchName);
            }
        } else if (branchType == "ulong") {
            if (accessorType == "long") {
                return new VectorBranchProxy<long long, unsigned long long>(tree, accessorName, branchName);
            } else if (accessorType == "float") {
                return new VectorBranchProxy<float, unsigned long long>(tree, accessorName, branchName);
            } else if (accessorType == "double") {
                return new VectorBranchProxy<double, unsigned long long>(tree, accessorName, branchName);
            }
        } else if (branchType == "long") {
            if (accessorType == "float") {
                return new VectorBranchProxy<float, long long>(tree, accessorName, branchName);
            }  else if (accessorType == "double") {
                return new VectorBranchProxy<double, long long>(tree, accessorName, branchName);
            }
        } else if (branchType == "float") {
            if (accessorType == "double") {
                return new VectorBranchProxy<double, float>(tree, accessorName, branchName);
            }
        }
        std::cerr << "Invalid accessor type \"" << accessorType << "\" found in expression \"" << expression << "\"" << std::endl;
        return nullptr;
    }

    void BranchProxyFactory::analyseExpression(const std::string& expr, std::string& accessorType,
            std::string& accessorName, std::string& branchType, std::string& branchName) {
        // Pattern "<type> name"
        static const std::regex re1("\\s*<([\\w]+)>\\s*([\\w]+)");
        // Pattern "<type> name -> name"
        static const std::regex re2("\\s*<([\\w%]+)>\\s*([\\w%]+)\\s*->\\s*([\\w%]+)");
        // Pattern "<type> name -> <type> name"
        static const std::regex re3("\\s*<([\\w%]+)>\\s*([\\w%]+)\\s*->\\s*<([\\w%]+)>\\s*([\\w%]+)");

        // Interpret the expression.
        std::smatch match;
        if (std::regex_match(expr, match, re1)) {
            accessorType = match[1];
            accessorName = match[2];
            branchType = accessorType;
            branchName = accessorName;
            return;
        } else if (std::regex_match(expr, match, re2)) {
            accessorType = match[1];
            accessorName = match[2];
            branchType = accessorType;
            branchName = match[3];
            return;
        } else if (std::regex_match(expr, match, re3)) {
            accessorType = match[1];
            accessorName = match[2];
            branchType = match[3];
            branchName = match[4];
            return;
        }
        std::cerr << "WARNING: Invalid branch expression \"" << expr << "\". Has to be of the form \"<type> accessorName\", \"<type> accessorName -> branchName\" or \"<type> accessorName -> <type> branchName\"" << std::endl;
    }

    template <typename A, typename B>
    BranchProxy<A, B>::BranchProxy(TTree* tree, const std::string& accessorName, const std::string& branchName) 
            : m_accessorName(accessorName), m_value(), m_accessor(accessorName) {
        m_branchName = branchName.empty() ? accessorName : branchName;
        std::string typeString = branchType();
        if (typeString.empty()) {
            tree->Branch(m_branchName.c_str(), &m_value);
        } else {
            tree->Branch(m_branchName.c_str(), &m_value, (m_branchName+"/"+typeString).c_str());
        }
    }

    template <typename A, typename B>
    std::string BranchProxy<A, B>::branchType() {
        if (std::is_same_v<B, bool>) return "O";
        else if (std::is_same_v<B, unsigned char>) return "b";
        else if (std::is_same_v<B, char>) return "B";
        else if (std::is_same_v<B, unsigned short>) return "s";
        else if (std::is_same_v<B, short>) return "S";
        else if (std::is_same_v<B, unsigned int>) return "i";
        else if (std::is_same_v<B, int>) return "I";
        else if (std::is_same_v<B, unsigned long long>) return "l";
        else if (std::is_same_v<B, long long>) return "L";
        else if (std::is_same_v<B, float>) return "F";
        else if (std::is_same_v<B, double>) return "D";
        return "";
    }

    template <typename A, typename B>
    void BranchProxy<A, B>::fill(const SG::AuxElement* element) {
        if (m_accessor.isAvailable(*element)) {
            m_value = static_cast<B>(m_accessor(*element));
        } else if (m_printError) {
            std::cerr << "WARNING: Accessor \"" <<  m_accessorName << "\" not available to fill branch \"" << m_branchName << "\"" << std::endl;
            m_printError = false;
        }
    }

    template <typename A, typename B>
    void BranchProxy<A, B>::reset() {
        m_value = B();
    }

    template <typename A, typename B>
    void BranchProxy<A, B>::setValue(const B& value) {
      m_value = value;
    }

    template <typename A, typename B>
    const B& BranchProxy<A, B>::value() const {
      return m_value;
    }

    template <typename A, typename B>
    VectorBranchProxy<A, B>::VectorBranchProxy(TTree* tree, const std::string& accessorName, const std::string& branchName) 
            : m_accessorName(accessorName), m_accessor(accessorName) {
        m_vector = new std::vector<B>();
        m_branchName = branchName.empty() ? accessorName : branchName;
        tree->Branch(m_branchName.c_str(), &m_vector);
    }

    template <typename A, typename B>
    VectorBranchProxy<A, B>::~VectorBranchProxy() {
        delete m_vector;
    }

    template <typename A, typename B>
    void VectorBranchProxy<A, B>::fill(const SG::AuxElement* element) {
        if (m_accessor.isAvailable(*element)) {
            m_vector->push_back(static_cast<B>(m_accessor(*element)));
        } else if (m_printError) {
            std::cerr << "WARNING: Accessor \"" <<  m_accessorName << "\" not available to fill branch \"" << m_branchName << "\"" << std::endl;
            m_printError = false;
        }
    }

    template <typename A, typename B>
    void VectorBranchProxy<A, B>::reset() {
        m_vector->clear();
    }

    template <typename A, typename B>
    void VectorBranchProxy<A, B>::setValue(const std::vector<B>& value) {
      *m_vector = value;
    }

    template <typename A, typename B>
    const std::vector<B>& VectorBranchProxy<A, B>::value() const {
      return *m_vector;
    }
}
