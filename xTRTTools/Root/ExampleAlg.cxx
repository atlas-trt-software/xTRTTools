#include <xTRTTools/ExampleAlg.h>
#include <xTRTTools/Helpers.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/VertexContainer.h>
#include <xAODTracking/TrackMeasurementValidationContainer.h>
#include <xAODTracking/TrackStateValidationContainer.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>

#include <TFile.h>
#include <TTree.h>

#include <TH1F.h>

xTRT::ExampleAlg::ExampleAlg(const std::string& name,
                             ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator) {

  // the inputTrackContainerName is the name of the container in the
  // TStore that we want to loop over.  For looping over all
  // InDetTrackParticles in a dataset, this should be
  // "InDetTrackParticles". If we want to loop over a container
  // created by the InDetTrackSelectionAlg (which is part of
  // xTRTTools), then we use the name of the output container from
  // that tool.
  declareProperty("inputTrackContainerName", m_inputTrackContainerName = "InDetTrackParticles", "input track container name");

  // This is the name of ntuple saved to one of the output files
  declareProperty("outputTreeName", m_treeName = "TRT_Tree", "output tree name");

  // GRL xml files to use
  // by default these are some good ones to use for all release 21 data (for 2015-2017)
  declareProperty("GRLfiles",
                  m_GRLfiles = {"GoodRunsLists/data16_13TeV/20180129/physics_25ns_21.0.19.xml",
                                "GoodRunsLists/data15_13TeV/20170619/physics_25ns_21.0.19.xml",
                                "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml"});
}

StatusCode xTRT::ExampleAlg::initialize() {
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // setup the GRL tool on the C++ side
  ANA_MSG_INFO("Setting up GRL tool");
  ANA_CHECK(m_GRLTool.setProperty("GoodRunsListVec", m_GRLfiles));
  ANA_CHECK(m_GRLTool.setProperty("PassThrough",     false));
  ANA_CHECK(m_GRLTool.setProperty("OutputLevel",     msg().level()));
  ANA_CHECK(m_GRLTool.initialize());

  // create some histograms, the first argument to the ROOT histogram
  // object is the name you'll use to grab the histogram and fill it
  // later.
  ANA_CHECK(book(TH1F("h_avgMu",";Pileup;Events",100,0.5,100.5)));
  ANA_CHECK(book(TH1F("h_trkEta",";|#eta|;Events",50,0.0,2.5)));
  ANA_CHECK(book(TH1F("h_tot",";ToT;Hits",50,0.5,50.5)));

  ANA_CHECK(book(TTree(m_treeName.c_str(),m_treeName.c_str())));
  m_tree = tree(m_treeName.c_str());
  m_tree->Branch("eventNumber",&m_eventNumber);
  m_tree->Branch("eventWeight",&m_eventWeight);
  m_tree->Branch("track_pt",   &m_track_pt);
  m_tree->Branch("track_nTRT", &m_track_nTRT);
  return StatusCode::SUCCESS;
}

StatusCode xTRT::ExampleAlg::execute() {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // create a pointer to the event info
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

  m_eventNumber = eventInfo->eventNumber();

  // if MC then get the event weight, 1.0 if not
  bool isMC = false;
  m_eventWeight = 1.0;
  if ( eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) {
    m_eventWeight = eventInfo->mcEventWeights().at(0);
    isMC = true;
  }

  // skip Data events not passing GRL
  if ( !isMC && !m_GRLTool->passRunLB(*eventInfo) ) {
    return StatusCode::SUCCESS;
  }

  // grab the average mu from the event info and histogram it
  float avgMu = eventInfo->averageInteractionsPerCrossing();
  hist("h_avgMu")->Fill(avgMu,m_eventWeight);

  // create a pointer to the container of particles saved by the
  // xTRT::InDetTrackSelectionAlg
  const xAOD::IParticleContainer* particles = nullptr;
  ANA_CHECK(evtStore()->retrieve(particles,m_inputTrackContainerName));

  // loop over all of the particles
  for ( auto const& particle : *particles ) {

    // we know that the container is actually filled with one of three
    // possible particle types:
    // - xAOD::TrackParticles
    // - xAOD::Electron
    // - xAOD::Muon
    // so let's cast our generic particle to a const pointer of one of
    // those types depending on the stored type.
    // Since we eventually want to analyze the track, if the particle
    // is an Electron or Muon, we have to grab the respective inner
    // detector track, so we use an xTRTTools function to do that.
    const xAOD::TrackParticle* track {nullptr};
    if ( particle->type() == xAOD::Type::ObjectType::TrackParticle ) {
      track = dynamic_cast<const xAOD::TrackParticle*>(particle);
    }
    if ( particle->type() == xAOD::Type::ObjectType::Electron ) {
      track = xTRT::getTrack(dynamic_cast<const xAOD::Electron*>(particle));
    }
    if ( particle->type() == xAOD::Type::Muon ) {
      track = xTRT::getTrack(dynamic_cast<const xAOD::Muon*>(particle));
    }

    // histogram the eta
    hist("h_trkEta")->Fill(std::abs(track->eta()),m_eventWeight);

    // set the pt and nTRT variables to be saved as a branch
    m_track_pt   = track->pt();
    m_track_nTRT = xTRT::nTRT(track);
    m_tree->Fill();

    const xTRT::MSOS* msos               = nullptr;
    const xTRT::DriftCircle* driftCircle = nullptr;

    // make sure track measurement container is available
    if ( xTRT::Acc::msosLink.isAvailable(*track) ) {
      // if it is, then loop over it
      for ( const auto& trackMeasurementLink : xTRT::Acc::msosLink(*track) ) {
        // now we have a link to a track measurement, check if it is valid
        if ( trackMeasurementLink.isValid() ) {
          // now dereference the link to get the measurement
          msos = *trackMeasurementLink;
          // make sure the measurement is from the TRT (type 3)
          if ( msos->detType() != 3 ) continue;
          // make sure the "measurement validation" (the drift circle) is valid
          if ( !(msos->trackMeasurementValidationLink().isValid()) ) continue;
          // if it is, dereference it to get the drift circle
          driftCircle = *(msos->trackMeasurementValidationLink());

          // get the time over threshold from the driftCircle
          float tot = xTRT::Acc::tot(*driftCircle);
          // histogram it
          hist("h_tot")->Fill(tot,m_eventWeight);

        } // if track measurement is valid
      } // loop over track measurements
    } // if track as msosLink

  }

  m_tree->Fill();
  return StatusCode::SUCCESS;
}

StatusCode xTRT::ExampleAlg::finalize() {
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
