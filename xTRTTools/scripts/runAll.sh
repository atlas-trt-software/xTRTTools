#!/bin/bash

# example script to run over a set of datasets
inputDatasets="/home/cgrefe/atlas/samples/TRT/valid1*DAOD_PHYSVAL*"
outputDirectory="/home/cgrefe/atlas/samples/TRT/nTuples/"
outputFileName="xTRTNTuple.root"
jobOptions="xTRTTools/TRTNTupler_DAOD_PhysVal_jobOptions.py"

mkdir -p ${outputDirectory}

for dataset in `ls -d ${inputDatasets}`; do 
    datasetName=`basename ${dataset}`
    athena ${jobOptions} --filesInput="${dataset}/*"
    if [ -f ${outputFileName} ]; then 
        mv ${outputFileName} "${outputDirectory}/xTRTNTuple.${datasetName}.root"
    else
        echo "ERROR: Unable to find \"${outputFileName}\" file for input \"${dataset}\"."
    fi
done