# `xTRTTools` Documentation

- [Introduction to ATLAS Software](./ATLAS_SW.md) (start here).
- [Setting up xTRTTools](./Setup.md)
- Writing custom algorithms:
    - [Analysing tracks](./Tracks.md)
    - [Analysing hits](./Hits.md)
- Standard n-tuple production:
    - [Producing standard N-tuples](./NTuples.md)
    - [N-tuple content](./NTupleContent.md)
- Details on the tools and algorithms that come with `xTRTTools`
    - [Algorithms](./Algorithms.md)
    - [Tools](./Tools)

- ntuple validation
    - [xTRTToolsValidation](https://gitlab.cern.ch/atlas-trt-software/xtrttoolsvalidation)

Documentation contact: Christian Grefe, cgrefe@cern.ch
