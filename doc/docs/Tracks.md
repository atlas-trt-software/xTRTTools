# Analyzing Tracks

There are two main classes for analyzing tracks:

- `xAOD::TrackParticleContainer`
- `xAOD::TrackParticle`

If a derivation contains the track particle container, then you have
access to track information from the inner detector. If you're working
on a TRT analysis, you are surely analyzing TRT flavor InDet
Derivations, so the `"InDetTrackParticles"` container should definitely be
in it.

!!! note
    You can always check the contents of an xAOD without
    processing it with analysis code; see [this
    package](https://gitlab.cern.ch/atlas-trt-software/TRTDerivationCheck).

To access the container, we grab it (in the execute function) from the
event store like so (I expect you learned about `evtStore()` and
`ANA_CHECK` from the [ASG
tutorial](https://atlassoftwaredocs.web.cern.ch/ABtutorial/).

You'll need to make sure you include the proper header file.

```cpp
#include <xAODTracking/TrackParticleContainer.h>

// ...

StatusCode YourAlgorithm::execute() {
  // ...

  // create a pointer to the container of track particles
  const xAOD::TrackParticleContainer* tracks {nullptr};
  ANA_CHECK(evtStore()->retrieve(tracks, "InDetTrackParticles"));

  // ...
  return StatusCode::SUCCESS;
}
```

Once you have the container you can loop over it -- each element will
be a `const xAOD::TrackParticle*` pointer. To see the API for a track
particle object [check the header
file](https://gitlab.cern.ch/atlas/athena/blob/22.2/Event/xAOD/xAODTracking/xAODTracking/versions/TrackParticle_v1.h)
retrieve a few useful properties of the track. This is where
`xTRTTools` becomes useful, it has some nice helper functions to grab
TRT information. This requires the `HelperFunctions.h` header file.

After the lines for retrieval, we'll do a loop:

```cpp
#include <xAODTracking/TrackParticleContainer.h>
#Include <xTRTTools/HelperFunctions.h>

// ...

StatusCode YourAlgorithm::execute() {
  // ...

  // create a pointer to the container of track particles
  const xAOD::TrackParticleContainer* tracks {nullptr};
  ANA_CHECK(evtStore()->retrieve(tracks, "InDetTrackParticles"));

  for ( const auto& track : *tracks ) {
    // using the xAOD::TrackParticle class member functions
    float track_pt     = track->pt();
    float track_abseta = std::abs(track->eta());

    // This is a function that is parto f xTRT which grabs the track
    // occupancy
    float trackOcc = xTRT::trackOccupancy(track);

    // This is a function that is part of xTRT which sums the total
    // number of TRT precision + tube + outlier hits (all TRT hits).
    int nTRT_hits = xTRT::nTRT(track);

    // this is a function that is part of xTRT which sums the total
    // number of pixel and SCT hits.
    int nSi_hits = xTRT::nSilicon(track);

    // another xTRT function in combination with an enum to test if the
    // track is only in the barrel
    bool trackInBarrel = (xTRT::getStrawRegion(track) == xTRT::StrawRegion::Barrel);

    // another xTRT function to get the track precision hit fraction
    // this function loops over all hits and checks how many are precision.
    // this functions is pretty expensive computationally
    float track_PHF = xTRT::trackPHF(track);
  }

  // ...

  return StatusCode::SUCCESS;
}
```

You can easily histogram or save to ntuple variables any of those
values (again, check the ASG tutorial)
