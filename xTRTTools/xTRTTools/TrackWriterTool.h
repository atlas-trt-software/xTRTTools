/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef XTRTTOOLS_TRACKWRITERTOOL_H
#define XTRTTOOLS_TRACKWRITERTOOL_H

#include <AsgTools/AsgTool.h>
#include <AthContainers/AuxElement.h>
#include <vector>
#include <memory>
#include <TTree.h>

#include <xTRTTools/IAsgParticleWriterTool.h>
#include <xTRTTools/TreeFiller.h>

namespace xTRT {
  /// \brief an \ref AsgTool that writes tracks to flat n-tuples.
  ///

  class TrackWriterTool final
    : public asg::AsgTool, virtual public IAsgParticleWriterTool {
    // Create a proper constructor for Athena
    ASG_TOOL_CLASS( TrackWriterTool, IAsgParticleWriterTool )

    /// \brief standard constructor
    /// \par Guarantee
    ///   strong
    /// \par Failures
    ///   out of memory II
  public:
    TrackWriterTool (const std::string& name);

    //
    // inherited interface
    //
    virtual StatusCode initialize () override;
    virtual bool fill(const xAOD::IParticle* particle, const xAOD::EventInfo* eventInfo=nullptr, const xAOD::Vertex* vertex=nullptr) override;
    virtual StatusCode setupTree(TTree* tree) override;

  private:
    /// tool properties
    std::string m_trackContainerName {"Test"};
    std::string m_prefix;
    bool m_storeHits;
    bool m_isMC;
    std::vector<std::string> m_trackVariables;
    std::vector<std::string> m_measurementVariables;
    std::vector<std::string> m_driftCircleVariables;

    TTree* m_tree;
    std::unique_ptr<TrackFiller> m_trackFiller;
  };
}

#endif
