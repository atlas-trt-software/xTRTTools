#!/usr/bin/env python

## Read the submission directory as a command line argument. You can
## extend the list of arguments with your private ones later on.
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-s', '--submission-dir', dest='submission_dir',
                    action='store', type=str, default='submitDir',
                    help='Submission directory for EventLoop')
parser.add_argument('-i', '--input-files', dest='input_files',
                    action='store', type=str, required=True,
                    help='Text file listing input files')
parser.add_argument('--max-events', dest='max_events', default=-1,
                    action='store', type=int, required=False,
                    help='Maximum number of events to process')
parser.add_argument('--input-container', dest='in_cont', default='InDetTrackParticles',
                    action='store', type=str, required=False)

args = parser.parse_args()

## Set up PyROOT
import ROOT
ROOT.xAOD.Init().ignore()

from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
from AnaAlgorithm.DualUseConfig import addPrivateTool
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence

################################################################################
## Define the example algorithm, call it MyTRTLooperAlg. Notice that
## the inputTrackContainerName is the output of the algorithm which
## created our container of tracks which passed the selection.
exampleAlg = AnaAlgorithmConfig('xTRT::ExampleAlg/MyTRTLooperAlg',
                                inputTrackContainerName = '{}_out'.format(args.in_cont),
                                outputTreeName          = '{}_tree'.format(args.in_cont))

## the xTRT::ExampleAlg has a property which is a vector<string>
## property for handling GRL files. We can define it here with a
## python list. GRLs are required when processing data
exampleAlg.GRLfiles = ['GoodRunsLists/data16_13TeV/20180129/physics_25ns_21.0.19.xml',
                       'GoodRunsLists/data15_13TeV/20170619/physics_25ns_21.0.19.xml',
                       'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml']
################################################################################

################################################################################
## Job definition
job = ROOT.EL.Job()
if args.max_events > 0:
    job.options().setDouble(ROOT.EL.Job.optMaxEvents, args.max_events)
else:
    pass

## We'll create a "sequence of algorithms." These algorithms are
## executed in order of insertion. First we add the algorithm which
## applies a selection on the InDetTrackParticles, then we run our
## Example algorithm which takes the selected tracks and creates an
## ntuple.
algSeq = AnaAlgSequence('xTRTExampleSequence')
algSeq += exampleAlg

## print to shell as an eye test
print(algSeq)

## add algorthms to job
for alg in algSeq:
    job.algsAdd(alg)
################################################################################

################################################################################
# Sample and driver configuration
# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
sh = ROOT.SH.SampleHandler()
sh.setMetaString('nc_tree', 'CollectionTree')
ROOT.SH.readFileList(sh,'ExampleAlgOutput', args.input_files)
sh.printContent()

# Create an EventLoop job.
job.sampleHandler(sh)

# prepare ntuple output
job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit(job, args.submission_dir)
################################################################################
