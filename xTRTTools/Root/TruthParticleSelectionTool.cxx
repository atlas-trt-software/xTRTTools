/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/TruthParticleSelectionTool.h>
#include <xAODTruth/TruthParticle.h>


namespace xTRT {

  TruthParticleSelectionTool::TruthParticleSelectionTool (const std::string& name)
      : AsgTool (name) {
    declareProperty ("statusCodes", m_statusCodes, "list of accepted status codes");
    declareProperty ("pdgIds", m_pdgIds, "list of accepted PDG IDs (absolute values)");
    declareProperty ("parentPdgIds", m_parentPdgIds, "list of accepted parent PDG IDs (absolute values)");
    declareProperty ("printCastWarning", m_printCastWarning = false, "whether to print a warning/error when the cast fails");
  }


  StatusCode TruthParticleSelectionTool::initialize () {
    if (not m_statusCodes.empty()) {
      m_statusCodeCutIndex = m_accept.addCut("status", "status code cut");
    }

    if (not m_pdgIds.empty()) {
      m_pdgIdCutIndex = m_accept.addCut("pdgId", "PDG ID cut");
    }

    if (not m_parentPdgIds.empty()) {
      m_parentPdgIdCutIndex = m_accept.addCut("parentPdgId", "parent PDG ID cut");
    }

    return StatusCode::SUCCESS;
  }


  const asg::AcceptInfo& TruthParticleSelectionTool::getAcceptInfo () const {
    return m_accept;
  }


  asg::AcceptData TruthParticleSelectionTool::accept (const xAOD::IParticle* particle) const {
    asg::AcceptData accept(&m_accept);

    // cast to TruthParticle and check that this valid
    const xAOD::TruthParticle* truthParticle = dynamic_cast<const xAOD::TruthParticle*>(particle);
    if (truthParticle == nullptr) {
      if (m_printCastWarning) ANA_MSG_ERROR ("failed to cast input particle to TruthParticle");
      return accept;
    }

    // cut on status code
    if (m_statusCodeCutIndex >= 0) {
      accept.setCutResult(m_statusCodeCutIndex,
                            std::find(m_statusCodes.begin(), m_statusCodes.end(), truthParticle->status()) != m_statusCodes.end());
    }

    // cut on PDG ID
    if (m_pdgIdCutIndex >= 0) {
      accept.setCutResult(m_pdgIdCutIndex,
                            std::find(m_pdgIds.begin(), m_pdgIds.end(), truthParticle->absPdgId()) != m_pdgIds.end());
    }

    // cut on parent PDG ID. In case of multiple parents accept if at least one PDG ID is accepted.
    if (m_parentPdgIdCutIndex >= 0) {
      bool foundParent = false;
      for (size_t iParent = 0; iParent < truthParticle->nParents(); iParent++) {
        auto parentParticle = truthParticle->parent(iParent);
        if (parentParticle == nullptr) continue;
        foundParent = std::find(m_parentPdgIds.begin(), m_parentPdgIds.end(), parentParticle->absPdgId()) != m_parentPdgIds.end();
        if (foundParent) break;
      }
      accept.setCutResult(m_parentPdgIdCutIndex, foundParent);
    }

    return accept;
  }

}
