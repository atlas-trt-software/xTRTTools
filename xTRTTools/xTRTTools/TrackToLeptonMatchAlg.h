/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef xTRTTools_TrackToLeptonMatchAlg_H
#define xTRTTools_TrackToLeptonMatchAlg_H

#include <PATCore/IAsgSelectionTool.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>
#include <AsgServices/ServiceHandle.h>
#include <SystematicsHandles/ISystematicsSvc.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SelectionHelpers/SysWriteSelectionHandle.h>
#include <SelectionHelpers/SelectionReadAccessorChar.h>
#include <SelectionHelpers/SysReadSelectionHandle.h>

#include <xAODTracking/TrackParticleContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>

// C++ includes
#include <string>
#include <map>

namespace xTRT {
  class TrackToLeptonMatchAlg : public EL::AnaAlgorithm {
  public:
    // this is a standard algorithm constructor
    TrackToLeptonMatchAlg(const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize() override;
    virtual StatusCode execute()    override;
    virtual StatusCode finalize()   override;
  private:
    ServiceHandle<CP::ISystematicsSvc> m_systematics {"SystematicsSvc", ""};
    CP::SysListHandle m_systematicsList {this};
    CP::SysReadHandle<xAOD::IParticleContainer> m_leptonsHandle {this, "leptons", "Muons_%SYS%", "Name of container with leptons. Can be of type xAOD::Electron, xAOD::Muon"};
    CP::SysReadHandle<xAOD::TrackParticleContainer> m_tracksHandle {this, "tracks", "InDetTrackParticles_%SYS%", "Name of container with xAOD::TrackParticle"};    
    CP::SysWriteSelectionHandle m_selectionHandle {this, "selectionDecoration", "lepton_matched,as_char", "the decoration for the selection, needs to be stored as char"};

    struct Counter {
      size_t nTracks = 0;
      size_t nAccepted = 0;
    };

    std::map<CP::SystematicSet, xTRT::TrackToLeptonMatchAlg::Counter> m_counters;
  };
}

#endif
