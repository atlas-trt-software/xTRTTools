/** @file  Helpers.h
 *  @brief TRTFramework helper functions
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#ifndef TRTFramework_Helpers_h
#define TRTFramework_Helpers_h

#include <xTRTTools/Utils.h>
#include <xTRTTools/Accessors.h>
#include <xTRTTools/HitSummary.h>

#include <xAODCore/AuxContainerBase.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/MuonAuxContainer.h>
#include <xAODEgamma/EgammaxAODHelpers.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/ElectronAuxContainer.h>
#include <xAODTruth/TruthParticleAuxContainer.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/xAODTruthHelpers.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <xAODEventInfo/EventInfo.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>


#define _USE_MATH_DEFINES
#include <cmath>
#include <string>

namespace xTRT {

  /** @addtogroup GenHelpers Generic Helper Functions
   *  @{
   */

  /// define \f$\pi\f$ (at compile time)
  constexpr double Pi()     { return M_PI; }
  /// define \f$\pi/2\f$ (at compile time)
  constexpr double PiHalf() { return M_PI_2; }

  /** @} */

  /** @enum IDTSCut
   *  One can look at the definitions here
   *  https://twiki.cern.ch/twiki/bin/view/AtlasProtected/InDetTrackSelectionTool
   */
  enum IDTSCut {
    None          =-1, ///< self explanatory 
    TightPrimary  = 0, ///< self explanatory
    LoosePrimary  = 1, ///< self explanatory
    LooseElectron = 2, ///< self explanatory
    LooseMuon     = 3  ///< self explanatory
  };

  /** @enum StrawRegion
   *  Enum for different straw regions of the TRT
   */
  enum StrawRegion {
    BRL    = 0, ///< Barrel
    BRLECA = 1, ///< Barrel/End Cap type A combination
    ECA    = 2, ///< End Cap Type A
    ECAECB = 3, ///< End Cap Type A/End Cap Type B combination
    ECB    = 4, ///< End Cap Type B
    NOTTRT = 5  ///< outside of the TRT
  };

  /** @enum SideRegion
   *  Enum for different sides of the TRT
   */
  enum SideRegion {
    BARREL = 0, ///< Barrel ("bec" = +/- 1)
    SIDE_A = 1, ///< Side A ("bec" = 2)
    SIDE_C = 2, ///< Side C ("bec" = -2)
    NONTRT = 3  ///< not in TRT
  };

  /** @addtogroup GenHelpers Generic Helper Functions
   *  @brief Some misc. functions to make life easier
   *  @{
   */

  /// get the straw region based on the eta value.
  /**
   * @param eta the pseudorapidity of the track.
   */
  StrawRegion getStrawRegion(const float eta);

  /// get the straw region that  track based through
  /**
   *  @param track the track of interest
   */
  StrawRegion getStrawRegion(const xAOD::TrackParticle* track);

  /// get the side region based on the bec variable.
  /**
   * @param bec the barrel/endcap value ("bec" is a decoration on the
   * drift circle object).
   */
  SideRegion  getSideRegion(const int bec);

  /// get the side region from the drift circle.
  /**
   * @param driftCircle the drift circle object retrieved from the
   * surface measurement (msos).
   */
  SideRegion getSideRegion(const xTRT::DriftCircle* driftCircle);

  /// get the absolute straw layer in the barrel.
  /**
   *  @param sl the straw layer ("strawlayer" decoration on the drift
   *  circle).
   *  @param layer the layer ("layer" decoration on the drift circle).
   */
  int absoluteBarrelSL(const int sl, const int layer);

  /// get the absolute laher in the barrel
  /**
   *  @param driftCircle the drift circle object
   */
  int absoluteBarrelSL(const xTRT::DriftCircle* driftCircle);

  /// get the absolute straw layer in the end cap.
  /**
   *  @param sl the straw layer ("strawlayer" decoration on the drift
   *  circle).
   *  @param wheel the wheel layer ("layer" decoration on the drift
   *  circle).
   */
  int absoluteEndCapSL(const int sl, const int wheel);

  /// get the absolute straw layer in the end cap.
  /**
   *  @param driftCircle the drift circle object
   */
  int absoluteEndCapSL(const xTRT::DriftCircle* driftCircle);

  /** @} */ // end of Helpers


  /** @addtogroup ParticlePropGetters Particle Property Getters
   *  @brief functions to easily get track/lepton properties
   *  @{
   */

  /// return the number of TRT hits on the track (all)
  int nTRT(const xAOD::TrackParticle* track);
  /// return the number of TRT prec+tube (non outlier)
  int nTRT_PrecTube(const xAOD::TrackParticle* track);
  /// return the number of TRT outliers
  int nTRT_Outlier(const xAOD::TrackParticle* track);
  /// return the number of Pixel hits
  int nPixel(const xAOD::TrackParticle* track);
  /// return the number of silicon hits (Pixel + SCT)
  int nSilicon(const xAOD::TrackParticle* track);
  /// return the number of silicon holes (Pixel + SCT)
  int nSiliconHoles(const xAOD::TrackParticle* track);
  /// return the number of silicon shared hits (Pixel + SCT)
  int nSiliconShared(const xAOD::TrackParticle* track);

  /// get the calo iso value from an electron (xAOD::Iso::topoetcone20)
  float caloIso(const xAOD::Electron* electron);
  /// get the calo iso value from a muon
  float caloIso(const xAOD::Muon* muon);
  /// get the track iso value from an electron (xAOD::Iso::ptvarcone20)
  float trackIso(const xAOD::Electron* electron);
  /// get the track iso value from a muon
  float trackIso(const xAOD::Muon* muon);
  /// get the track pTcone20 iso value from an electron (xAOD::Iso::ptcone20)
  float trackIso_pTcone20(const xAOD::Electron* electron);
  /// get if electron is truth matched
  bool  truthMatched(const xAOD::Electron* electron);
  /// get if muon is truth matched
  bool  truthMatched(const xAOD::Muon* muon);
  /// get if particle is from specific particle origin
  bool  isFrom(const xAOD::IParticle* particle, const MCTruthPartClassifier::ParticleOrigin origin);
  /// get if particle is from Z (uses isFrom with ZBoson enum value)
  bool  isFromZ(const xAOD::IParticle* particle);
  /// get if particle is from JPsi (uses isFrom with JPsi enum value)
  bool  isFromJPsi(const xAOD::IParticle* particle);

  /// return the track occupancy if available. return -1 if unavailable
  /**
   *  This is a helper function that wraps the process of retrieving the
   *  auxiliary data "TRTTrackOccupancy" from an xAOD::TrackParticle in
   *  one line.
   *
   *  @param track pointer to an xAOD::TrackParticle object.
   *  @return the "TRTTrackOccupancy" auxiliary data (-1 if unavailabe)
   */
  float trackOccupancy(const xAOD::TrackParticle* track);

  /// return the precision hit fraction by iterating over hits
  /**
   *  Loop over the hits on the track and determine the fraction of
   *  precision hits. This require all of the hit aux data to be
   *  present.  This is an _expensive_ function! If the msosLink aux
   *  data is not available, return -1;
   *
   *  @param track the track of interest
   *  @return the precision hit fraction, -1 if hits not available
   */
  float trackPHF(const xAOD::TrackParticle* track);

  /// return if the track is a failed extension
  /**
   *  Tracks that don't contain any precision hits but do have outlier
   *  hits are considered failed TRT extension tracks.
   *
   *  @param track The pointer to an xAOD::TrackParticle object
   */
  bool failedExtension(const xAOD::TrackParticle* track);

  /// return the (delta z0)*sin(theta) of the track with respect to a given vertex
  float deltaz0sinTheta(const xAOD::TrackParticle* track, const xAOD::Vertex* vtx=0);

  /// return the d0 significance of the track
  double d0signif(const xAOD::TrackParticle* track, const xAOD::EventInfo* evtinfo);

  /// retrieves the TruthParticle associated with the input track particle
  const xAOD::TruthParticle* getTruth(const xAOD::TrackParticle* track);
  /// retrieve the "original" xAOD::TrackParticle pointer from the electron
  const xAOD::TrackParticle* getTrack(const xAOD::Electron* electron);
  /// retrieve the xAOD::TrackParticle pointer from the muon
  const xAOD::TrackParticle* getTrack(const xAOD::Muon* muon);
  /// dummy function for templated selector xTRT::Algorithm::selectedFromIDTSCut
  const xAOD::TrackParticle* getTrack(const xAOD::TrackParticle* track);
  /// retrieve the GSF xAOD::TrackParticle pointer from the electron
  const xAOD::TrackParticle* getGSFTrack(const xAOD::Electron* electron);

  /** @} */

  /** @addtogroup HitHelpers Hit Helpers
   *  @{
   */

  /// get a hit summary object based on the track, surface measurement, and drift circle
  /**
   *  This is a convenience function to interact with the HitSummary
   *  struct; It allows you to avoid handling aux data. See the
   *  xTRT::HitSummary struct documentation for all the properties
   *  that get filled.
   *
   *  @param track the xAOD::TrackParticle that the hit is a part of
   *  @param msos The MSOS of the hit
   *  @param driftCircle the drift circle belonging to the MSOS
   *  @param isMC is MC flag required for calculating the corrected drift time
   */
  xTRT::HitSummary getHitSummary(const xAOD::TrackParticle* track,
                                 const xTRT::MSOS* msos,
                                 const xTRT::DriftCircle* driftCircle,
                                 const bool isMC);

  /// get the corrected drift time of the hist
  /**
   *  @param driftCircle the drift circle object that contains the aux
   *  data "drifttime", "driftTimeToTCorrection" and
   *  "driftTimeHTCorrection"
   *  @param isMC is MC flag required for calculating the corrected drift time
   */
  float hitCorrectedDriftTime(const xTRT::DriftCircle* driftCircle, const bool isMC);

  /// check whether the middle high threshold bit is set
  /**
   *  @param driftCircle the drift circle object
   */
  bool hitIsHighThresholdMiddleBit(const xTRT::DriftCircle* driftCircle);

  /// retrieve the drift circle error on the hit (for percision/tube descrimination)
  /**
   *  @param msos the TRT MSOS object which contains the data "errDC"
   */
  float hitError(const xTRT::MSOS* msos);

  /// calculate the error on the hit (for percision/tube descrimination)
  /**
   *  This method is deprecated, use hitError() or "errDC" if available.
   *  It uses the biased and unbiased residuals and pulls to estimate the
   *  hit error.
   *
   *  @param msos the TRT MSOS object which contains the data
   *  "unbiasedResidualX", "biasedResidualX", "unbiasedPullX",
   *  "biasedPullX"
   */
  float calculateHitError(const xTRT::MSOS* msos);

  /// given a hit, return if it is precision or not
  /**
   *  This is a convenience function that uses xTRT::hitError
   *
   *  @param msos the TRT MSOS object, must have the data:
   *  "errDC"
   */
  bool hitIsPrecision(const xTRT::MSOS* msos);

  /// given a hit, returns the hit type 
  /**
   *  Following the definitions at https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TrtSoftwareDefinitions#Hit_types,
   *  the hit types are defined as
   *  - 0: precision hit
   *  - 1: tube hit
   *  - 2: outlier
   *
   *  @param msos the TRT MSOS object, must have the data:
   *  "rTrkWire", "errDC",
   */
  char hitType(const xTRT::MSOS* msos);

  /// get the hit radius given the MSOS and Drift Circle
  /**
   *  The track to wire distance is given a sign to remove the
   *  "left-right ambiguity" -- so that we know the side of the straw
   *  on which the track crossed. This same sign should be applied to
   *  the drift radius itself to ensure a symmetric residual
   *  distribution.
   *  See: https://twiki.cern.ch/twiki/bin/view/Atlas/TrtAnalysisRun2#Drift_radius_and_residual
   *
   *  @param msos the TRT MSOS object, must have "localX" @param
   *  driftCircle the TRT drift circle object, must have "localX"
   */
  float hitRadius(const xTRT::MSOS* msos, const xTRT::DriftCircle* driftCircle);

  /// get the ZR of a hit
  /**
   *  @param track the track that the hit belongs to
   *  @param driftCircle the drift circle of the event.  The DC must
   *   have the auxdata: bec, globalX, globalY, globalZ
   */
  float hitZR(const xAOD::TrackParticle* track, const xTRT::DriftCircle* driftCircle);

  /// calculate the length of a track in a straw
  /**
   *  Defition in the barrel:
   *
   *  \f[
   *     L_\mathrm{BRL} = \frac{2\sqrt{4-R_\mathrm{DC}^2}}
   *           {\sin\theta_\mathrm{trk}}
   *  \f]
   *
   *  Defition in the end caps:
   *
   *  \f[
   *     L_\mathrm{EC} = \frac{2\sqrt{4-R_\mathrm{DC}^2}}
   *          {\sqrt{1-\sin^2\theta_\mathrm{trk}
   *           \cos^2(\phi_\mathrm{trk}-\phi_\mathrm{straw})}}
   *  \f]
   *
   *  where \f$R_\mathrm{DC}\f$ is the drift circle radius. The units are in mm.
   *
   *  @param msos the measurement on surface object for the hit
   *  @param driftCircle the drift circle for the hit
   *
   *  This function requires aux data rTrkWire (msos), localPhi (msos), bec (driftCircle)
   */
  float hitL(const xTRT::MSOS* msos, const xTRT::DriftCircle* driftCircle);

  /** @} */

  /* Returns a copy of the given string with white spaces new line characters removed
   */
  std::string removeWhiteSpaces(const std::string& s);

}

#include "Helpers.icc"

#endif
