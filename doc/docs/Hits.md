# Analyzing Hits

Every `TrackParticle` that is stored in a derivation augmented with
TRT information will contain the hits belonging to that track.

The two xAOD classes which are associated with track hits:

- `xAOD::TrackStateValidation`
- `xAOD::TrackMeasurementValidation`

To make our lives a bit easier, we have aliases to these class names
in `xTRTTools`:

- `xTRT::MSOS` (measurement on surface) is an alias to
  `xAOD::TrackStateValidation`.
- `xTRT::DriftCircle` is an alias to
  `xAOD::TrackMeasurementValidation`.

To actually access this information, we have to grab the `ElementLink`
to the containers which hold these objects. `xTRTTools` has an
"accessor" for this link. Let's look at a loop over tracks as an
example:

```cpp
// create a pointer to the container of track particles
const xAOD::TrackParticleContainer* tracks {nullptr};
ANA_CHECK(evtStore()->retrieve(tracks, "InDetTrackParticles"));

// create some pointers we'll use later
const xTRT::MSOS* msos = nullptr;
const xTRT::DriftCircle* driftCircle = nullptr;

// loop over tracks
for ( const auto& track : *tracks ) {
  // check the msosLink accessor to see if its available
  if ( xTRT::CA::msosLink.isAvailable(*track) ) {
    // if it is, grab it and loop over it (it contains links to measurements)
    for ( const auto& trackMeasurementLink : xTRT::CA::msosLink(*track) ) {
      // make sure the link is valid
      if ( trackMeasurementLink.isValid() ) {
        // dereference the link to get the measurement:
        msos = *trackMeasurementLink;
        if ( msos->detType() != 3 ) continue; // makes sure we get TRT hits only.
        // make sure the drift circle (the track measurement validation) link is valid
        if ( !(msos->trackMeasurementValidationLink().isValid()) ) continue;
        // get the drift circle by dereferencing the link
        driftCircle = *(msos->trackMeasurementValidationLink());

        // now you can do anything with the drift circle and the MSOS,
        // like get the length of the track in this straw
        // xTRTTools provides a function for this:
        float hitLength = xTRT::hitL(track, msos, driftCircle);
        // now calculate the ToT/L [ns/mm], using the ToT accessor
        float totOverL = xTRT::Acc::tot(driftCircle) / hitLength;

      } // end if track measurement is valid
    } // end loop over track measurements
  } // end if track measurement link is valid
} // end lop over tracks
```
