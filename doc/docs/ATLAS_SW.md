# ATLAS Software

If you already have some familiarity with ATLAS Software, you can
probably skip this prologue style chapter (but it's not that long of a
read) and move on to [setup](02_Setup.md).

## The ATLAS Computing Environment

The best point to get started with the ATLAS software are the
[official tutorials](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorial).
Some more details on the different aspects of ATLAS data analysis
are documented by the [Analysis Model Group (AMG)](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisModelgroup).

Below you can find a very brief summary of the basics in case you
just need to refresh your memory.

### The Software

For our purposes ATLAS software (SW) can be broken in two: Athena and
Analysis Releases (it gets more complicated but that's beyond the
scope of this small tutorial).

- **Athena**: the main software - can do everything (simulation,
  reconstruction, analysis, etc.)
- **Analysis Releases**: a subset of the main ATLAS code base (that exists
  in Athena) but can be used outside of Athena ("dual use
  code")... and also some analysis specific code on top of it.
  There are two flavours of analysis releases AnalysisBase, which relies on a the so-called EventLoop,
  and AthAnalysis, which runs directly in Athena.

All of the code is stored in a git repository hosted at the Athena
GitLab page [here](https://gitlab.cern.ch/atlas/athena).

People who run the core software group and the analysis software group
handle building what parts of the code are needed for Athena releases
and what parts are needed for analysis releases. The builds are called
releases and they are installed to the ATLAS CVMFS areas so that all
machines connected to the ATLAS CVMFS repositories have access to the
releases. These days, all code is built with cmake. We'll talk more
about that later.

The current main release of ATLAS Software is Athena release
22.0.X.

The Analysis Releases are on the numbering scheme 22.2.X. For the main
part of this workbook, we'll be setting up and using an Analysis
Release.


### The Data Format

The data format (what is actually stored on disk in ROOT files) is
very closely related to the analysis software. The xAOD model is a set
of classes which make it nice and intuitive to analyze physics objects.
At its core it represents physics objects as C++ classes (e.g. Event,
TrackParticle, Electron, Jet, etc.). It also provides the means to analyze
those objects, and "decorate" them with properties. The information that make
up the xAOD objects are stored in branches of a ROOT tree (which can be
accessed using standard ROOT code). The full functionality is only
achieved if the information is accessed through the xAOD C++ interfaces.

The actual files stored on disks all over the world at grid sites and
in your work areas if you've downloaded some from rucio are called
DAOD's (Derived Analysis Object Data, some people also call the files
themselves xAOD's). A primary AOD is the starting point: Some Athena
code produces the AOD after reconstruction, and then the *Derivation
Framework* is used to produce the Derived AOD's. These are tailored
for specific purposes. Starting from the AOD which contains
everything, a derivation is made by removing whole events, removing
whole objects from events, removing some information attached to
objects in some events, adding some information to some objects (we do
this in the TRT a lot), or calibrating existing objects. The
nomenclature:

- **Skimming**: removing whole events
- **Thinning**: removing whole objects from within an event, but keeping
  the rest of the event
- **Slimming**: removing information from within objects, but keeping the
  rest of the object
- **Augmentation**: adding data not found in the input data

In the TRT SW group, we use the Inner Detector (InDet) specific
xAOD derivation, the so called TRTxAOD format. This is technically
not a derivation of the basic xAOD format. It includes additional
information (like TRT hits) but misses several of the typical analysis
objects that are not needed for TRT studies. You can find out more about
the TRTxAOD format and available samples [here](https://twiki.cern.ch/twiki/bin/view/Atlas/TrtxAOD).
