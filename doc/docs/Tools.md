# Analysis Tools
These are the `ASGTools` provided by the `xTRTTools` package.

## `TruthParticleSelectionTool`
The `TruthParticleSelectionTool` is an `ASGSelectionTool` that allows to
select `TruthParticle` objects depending on their generator status code,
their PDG ID or their parent particle PDG ID.

See [TruthParticleSelectionTool.h](../../xTRTTools/xTRTTools/TruthParticleSelectionTool.h) and [TruthParticleSelectionTool.cxx](../../xTRTTools/Root/TruthParticleSelectionTool.cxx).

The following steering paramters are available:
- `statusCodes`: list of accepted status codes.
- `pdgIds`: list of accepted PDG ID values (absolute values).
- `parentPdgIds`: list of accepted parent PDG ID values (absolute values).
- `printCastWarning`: decide if a warning should be printed whenever the cast of the object to a `TruthParticle` fails.markdown link to files
