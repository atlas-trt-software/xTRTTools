from AthenaCommon.AppMgr import theApp
from xTRTTools.TRTTruthParticleSequence import makeTRTTruthParticleSelectionSequence
from xTRTTools.TRTMuonSequence import makeTRTMuonSelectionSequence
from xTRTTools.TRTElectronSequence import makeTRTElectronSelectionSequence
from xTRTTools.TRTTrackSequence import makeTRTTrackSelectionSequence

from AnaAlgorithm.DualUseConfig import createAlgorithm, createService, createPublicTool
from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence

##
# These job options create TRT n-tuples with track information. Each entry corresponds to a track that passed at least one of several selections.
# 1) For tag-and-probe, electrons and muons passing some pre-selection are cosidered as tags.
#    Then, the invariant mass and the z0 difference between the tag and the probe tracks are checked.
#.   Z boson and J/psi hypothesis are checked separately.
#    Opposite charge is not required, but the information is stored.
# 2) Any track linked to a secondary vertex is stored. The hypothesis which are considered are K0S, Lambda,
#    or photon conversions (via GSF tracks).
# 3) Any track matched to a good electron or muon candidate is stored.
# 4) Any track above 5 GeV is stored.
# If they exist, truth particles (only for MC), electron and muon objects that correspond to the selected tracks will be stored as well.
# For MC, true electrons and muons are stored even if no corresponding track exists to allow tracking efficiency studies.
# No trigger requirement is applied.
##

############################################################################################
# Define the containers and the pre-selection for the objects to be stored in the n-tuples #
############################################################################################

# output file information
outputFileName = 'xTRTNTupleTagAndProbe.root'
outputTreeName = 'TRT'

# the event info container to be used (only needed to extract trigger information)
eventInfoName = 'EventInfo'

# define the truth particle container and the selection based on pT, eta and PDG ID
truthParticleContainer = 'TruthParticles'
truthMinPt = 2.e3               # in MeV
truthMaxEta = 2.1               # TRT acceptance
truthStatusCodes = [1]          # only store stable truth particles
truthPdgIds = [11, 13]  # only store charged particle with these PDG IDs: electrons, muons, pions, kaons, protons
truthParentPdgIds = []  # any truth parent

# define the track preselection
# see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsRun2R22#Track_Selection
trackContainer = 'InDetTrackParticles'
trackCutLevelDecorations = ['Loose', 'LoosePrimary', 'TightPrimary']
trackCutLevel = 'Loose'
trackMinPt = 1.0e3   # in MeV
trackMaxEta = 2.1    # TRT acceptance
trackMinTRTHits = -1 # any value < 0 means that no TRT hits are required

highPtTrackCut = 5.0e3 # all tracks above this threshold will be kept

secondaryVertexContainers = ['V0KshortVertices', 'V0LambdaVertices', 'V0LambdabarVertices']  # list of secondary vertex containers that are used to select tracks

preselectedTrackContainer = trackContainer+'_preselected'
selectedTrackContainerHighPt = trackContainer+'_selected_highPt'
selectedTrackContainer = trackContainer+'_selected'

gsfTrackContainer = 'GSFTrackParticles'
selectedGsfTrackContainer = gsfTrackContainer+'_selected'

# Define electron selections
electronContainer = 'Electrons'      # set to '' to ignore electrons
# 1) Electron selection that is used for tag objects
electronZTagIdentification = 'LHTight'   # Loose, Medium, Tight, LHLoose, LHMedium, LHTight
electronZTagIsolation = 'NonIso'         # electron isolation variables missing in TRTxAOD
electronZTagMinPt = 10e3                 # in MeV
electronZTagMassWindow = (65e3,100e3)    # in MeV
electronZTagMaxDeltaZ0 = 5.              # in mm
electronZTagContainer = 'Electrons_ZTag'
# 2) Electron selection that is used for tag objects
electronJPsiTagIdentification = 'LHTight' # Loose, Medium, Tight, LHLoose, LHMedium, LHTight
electronJPsiTagIsolation = 'NonIso'       # electron isolation variables missing in TRTxAOD
electronJPsiTagMinPt = 4.5e3              # in MeV
electronJPsiTagMassWindow = (2.7e3,3.3e3) # in MeV
electronJPsiTagMaxDeltaZ0 = 5.            # in mm
electronJPsiTagContainer = 'Electrons_JPsiTag'
# 3) Electron selection used to select matched InDet tracks, no bias from TRT particle ID
electronMatchIdentification = 'Loose'
electronMatchIsolation = 'NonIso'
electronMatchMinPt = 4.5e3
electronMatchContainer = 'Electrons_match'

# Define muon selections
muonContainer = 'Muons'          # set to '' to ignore muons
# 1) Muon selection that is used for tag objects
muonZTagIdentification = 'Loose'
muonZTagIsolation = 'NonIso'         # muon isolation variables missing in TRTxAOD
muonZTagMinPt = 10e3                 # in MeV
muonZTagMassWindow = (80e3,100e3)    # in MeV
muonZTagMaxDeltaZ0 = 2.              # in mm
muonZTagContainer = 'Muons_ZTag'
# 2) Muon selection that is used for tag objects
muonJPsiTagIdentification = 'Loose'
muonJPsiTagIsolation = 'NonIso'       # muon isolation variables missing in TRTxAOD
muonJPsiTagMinPt = 3e3                # in MeV
muonJPsiTagMassWindow = (2.9e3,3.3e3) # in MeV
muonJPsiTagMaxDeltaZ0 = 2.            # in mm
muonJPsiTagContainer = 'Muons_JPsiTag'
# 2) Muon selection used to select matched InDet tracks, no bias from TRT particle ID
muonMatchIdentification = 'Loose'
muonMatchIsolation = 'NonIso'
muonMatchMinPt = 3e3
muonMatchContainer = 'Muons_match'

# definition of the trigger requirements to be applied for data (OR between all listed trigger items)
triggerListElectron = ['HLT_e24_lhmedium_L1EM20VH', 'HLT_e60_lhmedium', 'HLT_e120_lhloose', 'HLT_e24_lhtight_nod0_ivarloose',
                       'HLT_e26_lhtight_nod0_ivarloose', 'HLT_e26_lhtight_nod0', 'HLT_e60_lhmedium_nod0', 'HLT_e140_lhloose_nod0', 'HLT_e300_etcut']
triggerListMuon = ['HLT_mu20_iloose_L1MU15', 'HLT_mu40', 'HLT_mu60_0eta105_msonly', 'HLT_mu24_iloose',
                   'HLT_mu24_ivarloose', 'HLT_mu40', 'HLT_mu50', 'HLT_mu24_ivarmedium', 'HLT_mu26_ivarmedium']

########################################################
# Changes below here should typically not be necessary #
########################################################

# define the input file, override from the command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = []

# Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
#jps.AthenaCommonFlags.AccessMode = "ClassAccess"
# POOL access should be more robust across releases
jps.AthenaCommonFlags.AccessMode = "POOLAccess"

# define the output file name
jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:"+outputFileName]
svcMgr.THistSvc.MaxFileSize = -1 #speeds up jobs that output lots of histograms

# read the metadata from the first input file
dataType = 'data'
if len(jps.AthenaCommonFlags.FilesInput()) > 0:
    from PyUtils import AthFile
    af = AthFile.fopen(jps.AthenaCommonFlags.FilesInput()[0]) # opens the first file from the FilesInput list

    # extract the datatype from the metadata of the first file to process
    if 'IS_SIMULATION' in af.fileinfos['evt_type']:
        if 'ATLFASTII' in af.fileinfos['metadata']['/Simulation/Parameters']['SimulationFlavour'].upper():
            dataType = 'afii'
        else:
            dataType = 'mc'

# Set up the systematics loader/handler service:
sysService = createService( 'CP::SystematicsSvc', 'SystematicsSvc', sequence = athAlgSeq )
sysService.sigmaRecommended = 1



####################################
# Sequence of algorithms to be run #
####################################

# input files needed for pile-up reweighting - these files need to be updated depending on dataset to be processed
prwfiles = []
lumicalcfiles = []
if dataType in ['mc', 'afii']:
    lumicalcfiles = [
        "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
        "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
        "GoodRunsLists/data17_13TeV/20190708/ilumicalc_histograms_None_325713-340453_OflLumi-13TeV-010.root",
        "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",
    ]
    prwfiles = ["dev/PileupReweighting/mc16_13TeV/pileup_mc16a_dsid410501_FS.root"]

## disable pile-up reweighting for TRT studies
# pileupSequence = makePileupAnalysisSequence(dataType, userPileupConfigs=prwfiles, userLumicalcFiles=lumicalcfiles)
# pileupSequence.configure(inputName = eventInfoName, outputName = eventInfoName+'_%SYS%')
# athAlgSeq += pileupSequence

# do not apply the trigger selection, we rely on tag-and-probe
if dataType == 'data':
    alg = createAlgorithm('TriggerSelectionAlg', 'TriggerSelectionAlg')
    alg.EventInfoName = eventInfoName
    alg.TriggerList = triggerListElectron + triggerListMuon
#    athAlgSeq += alg

# preselection for truth particles to store, stores a cutflow histogram
selectedTruthParticleContainer = ''
if dataType in [ 'mc', 'afii' ]:
    selectedTruthParticleContainer = truthParticleContainer+'_selected'
    seq = makeTRTTruthParticleSelectionSequence(dataType, maxEta=truthMaxEta, minPt=truthMinPt,
                                                statusCodes=truthStatusCodes, pdgIds=truthPdgIds, parentPdgIds=truthParentPdgIds,
                                                enableCutflow=True, enableKinematicHistograms=False)
    seq.configure(inputName = truthParticleContainer, outputName = selectedTruthParticleContainer)
    athAlgSeq += seq

if electronContainer:
    # preselection for tag electrons, stores a cutflow histogram
    seq = makeTRTElectronSelectionSequence(dataType, electronZTagMinPt, electronZTagIdentification, electronZTagIsolation,
                                           postfix = 'ZTag', enableCutflow=True, enableKinematicHistograms=False)
    seq.configure(inputName = electronContainer, outputName = electronZTagContainer)
    athAlgSeq += seq
    
    # preselection for match electrons, stores a cutflow histogram
    seq = makeTRTElectronSelectionSequence(dataType, electronMatchMinPt, electronMatchIdentification, electronMatchIsolation,
                                           postfix = 'Match', enableCutflow=True, enableKinematicHistograms=False)
    seq.configure(inputName = electronContainer, outputName = electronMatchContainer)
    athAlgSeq += seq
    # from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import makeElectronAnalysisSequence
    # electronWP = electronZTagIdentification + '.' + electronZTagIsolation
    # electronSequence = makeElectronAnalysisSequence(dataType, electronWP, postfix = 'ZTag', recomputeLikelihood=True, shallowViewOutput = True)
    # electronSequence.configure(inputName = electronContainer, outputName = electronZTagContainer )
    # athAlgSeq += electronSequence

    # electronWP = electronMatchIdentification + '.' + electronMatchIsolation
    # electronSequence = makeElectronAnalysisSequence(dataType, electronWP, postfix = 'Match', recomputeLikelihood=True, shallowViewOutput = True)
    # electronSequence.configure(inputName = electronContainer, outputName = electronMatchContainer )
    # athAlgSeq += electronSequence


if muonContainer:
    # preselection for tag muons, stores a cutflow histogram
    seqZTag = makeTRTMuonSelectionSequence(dataType, muonZTagMinPt, muonZTagIdentification, muonZTagIsolation,
                                       postfix = 'ZTag', enableCutflow=True, enableKinematicHistograms=False)
    seqZTag.configure(inputName = muonContainer, outputName = muonZTagContainer)
    athAlgSeq += seqZTag

    # preselection for match muons, stores a cutflow histogram
    seq = makeTRTMuonSelectionSequence(dataType, muonMatchMinPt, muonMatchIdentification, muonMatchIsolation,
                                      postfix = 'Match', enableCutflow=True, enableKinematicHistograms=False)
    seq.configure(inputName = muonContainer, outputName = muonMatchContainer)
    athAlgSeq += seq
    # from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
    # muonWP = muonZTagIdentification + '.' + muonZTagIsolation
    # muonSequence = makeMuonAnalysisSequence(dataType, muonWP, postfix = 'ZTag', shallowViewOutput = True, isRun3Geo=True)
    # muonSequence.configure( inputName = muonContainer, outputName = muonZTagContainer )
    # athAlgSeq += muonSequence

    # muonWP = muonMatchIdentification + '.' + muonMatchIsolation
    # muonSequence = makeMuonAnalysisSequence(dataType, muonWP, postfix = 'Match', shallowViewOutput = True, isRun3Geo=True)
    # muonSequence.configure( inputName = muonContainer, outputName = muonMatchContainer )
    # athAlgSeq += muonSequence

# preselection for tracks
seq = makeTRTTrackSelectionSequence(dataType, postfix=trackContainer,
                                    maxEta=trackMaxEta, minPt=trackMinPt, cutLevel=trackCutLevel,
                                    minNTRTHits=trackMinTRTHits, cutLevelDecorations=trackCutLevelDecorations,
                                    enableCutflow=True, enableKinematicHistograms=False,)
seq.configure(inputName = trackContainer, outputName = preselectedTrackContainer)
athAlgSeq += seq

# High pT selection for tracks
seq = makeTRTTrackSelectionSequence(dataType, postfix=trackContainer+'_highPt',
                                    maxEta=trackMaxEta, minPt=highPtTrackCut, cutLevel=trackCutLevel,
                                    minNTRTHits=trackMinTRTHits, cutLevelDecorations=[],
                                    enableCutflow=True, enableKinematicHistograms=False,)
seq.configure(inputName = trackContainer, outputName = selectedTrackContainerHighPt)
athAlgSeq += seq

# Prepare the track writer tools
inDetTrackWriterTool = createPublicTool( 'xTRT::TrackWriterTool', 'InDetTrackWriterTool' )
inDetTrackWriterTool.container = selectedTrackContainer
inDetTrackWriterTool.prefix = trackContainer+'_'
inDetTrackWriterTool.isMC = dataType in [ 'mc', 'afii' ]
# add the track selection cut information to the list of output variables
for decoration in trackCutLevelDecorations:
    inDetTrackWriterTool.trackVariables.append("<char> {} -> <bool> {}".format(decoration, decoration))
inDetTrackWriterTool.trackVariables += [
    "<float> Reco_ToT_dEdx_noHT_divByL -> dEdx_noHT",
    "<float> Reco_ToT_usedHits_noHT_divByL -> dEdx_noHT_usedHits",
]

gsfTrackWriterTool = createPublicTool( 'xTRT::TrackWriterTool', 'GSFTrackWriterTool' )
gsfTrackWriterTool.container = selectedGsfTrackContainer
gsfTrackWriterTool.prefix = gsfTrackContainer+'_'
gsfTrackWriterTool.isMC = dataType in [ 'mc', 'afii' ]
gsfTrackWriterTool.measurementVariables = list(filter(lambda entry: 'isShared' not in entry, gsfTrackWriterTool.measurementVariables))
gsfTrackWriterTool.trackVariables += [
    "<float> GSF_ToT_dEdx_noHT_divByL -> dEdx_noHT",
    "<float> GSF_ToT_usedHits_noHT_divByL -> dEdx_noHT_usedHits",
]

# tag and probe selection using electron as tags
label = 'tnp_Z_electron'
eleTnpZAlg = createAlgorithm('xTRT::TagAndProbeAlg', 'TnPZElectronAlg')
eleTnpZAlg.tags = electronZTagContainer
eleTnpZAlg.probes = preselectedTrackContainer
eleTnpZAlg.massWindowLow = electronZTagMassWindow[0]
eleTnpZAlg.massWindowHigh = electronZTagMassWindow[1]
eleTnpZAlg.maxDeltaZ0 = electronZTagMaxDeltaZ0
eleTnpZAlg.oppositeCharge = False
eleTnpZAlg.p4Decoration = label+'_p4'
eleTnpZAlg.deltaZ0Decoration = label+'_deltaZ0'
eleTnpZAlg.qxqDecoration = label+'_qxq'
eleTnpZAlg.selectionDecoration = label+',as_char'
athAlgSeq += eleTnpZAlg
# add the corresponding variables
inDetTrackWriterTool.trackVariables += [
    '<char> {0} -> <bool> {0}'.format(label),
    '<TLorentzVector> {0}_p4'.format(label),
    '<float> {0}_deltaZ0'.format(label),
    '<char> {0}_qxq'.format(label),
]
trackSelection = [eleTnpZAlg.selectionDecoration]

# tag and probe selection using electron as tags
label = 'tnp_JPsi_electron'
eleTnpJPsiAlg = createAlgorithm('xTRT::TagAndProbeAlg', 'TnPJPsiElectronAlg')
eleTnpJPsiAlg.tags = electronZTagContainer
eleTnpJPsiAlg.probes = preselectedTrackContainer
eleTnpJPsiAlg.massWindowLow = electronJPsiTagMassWindow[0]
eleTnpJPsiAlg.massWindowHigh = electronJPsiTagMassWindow[1]
eleTnpJPsiAlg.maxDeltaZ0 = electronJPsiTagMaxDeltaZ0
eleTnpJPsiAlg.oppositeCharge = False
eleTnpJPsiAlg.p4Decoration = label+'_p4'
eleTnpJPsiAlg.deltaZ0Decoration = label+'_deltaZ0'
eleTnpJPsiAlg.qxqDecoration = label+'_qxq'
eleTnpJPsiAlg.selectionDecoration = label+',as_char'
athAlgSeq += eleTnpJPsiAlg
# add the corresponding variables
inDetTrackWriterTool.trackVariables += [
    '<char> {0} -> <bool> {0}'.format(label),
    '<TLorentzVector> {0}_p4'.format(label),
    '<float> {0}_deltaZ0'.format(label),
    '<char> {0}_qxq'.format(label),
]
trackSelection[0] += '||{}'.format(eleTnpJPsiAlg.selectionDecoration)

# tag and probe selection using muons as tags
label = 'tnp_Z_muon'
muonTnpZAlg = createAlgorithm('xTRT::TagAndProbeAlg', 'TnPZMuonAlg')
muonTnpZAlg.tags = muonZTagContainer
muonTnpZAlg.probes = preselectedTrackContainer
muonTnpZAlg.massWindowLow = muonZTagMassWindow[0]
muonTnpZAlg.massWindowHigh = muonZTagMassWindow[1]
muonTnpZAlg.maxDeltaZ0 = muonZTagMaxDeltaZ0
muonTnpZAlg.oppositeCharge = False
muonTnpZAlg.p4Decoration = label+'_p4'
muonTnpZAlg.deltaZ0Decoration = label+'_deltaZ0'
muonTnpZAlg.qxqDecoration = label+'_qxq'
muonTnpZAlg.selectionDecoration = label+',as_char'
athAlgSeq += muonTnpZAlg
# add the corresponding variables
inDetTrackWriterTool.trackVariables += [
    '<char> {0} -> <bool> {0}'.format(label),
    '<TLorentzVector> {0}_p4'.format(label),
    '<float> {0}_deltaZ0'.format(label),
    '<char> {0}_qxq'.format(label),
]
trackSelection[0] += '||{}'.format(muonTnpZAlg.selectionDecoration)

# tag and probe selection using muons as tags
label = 'tnp_JPsi_muon'
muonTnpJPsiAlg = createAlgorithm('xTRT::TagAndProbeAlg', 'TnPJPsiMuonAlg')
muonTnpJPsiAlg.tags = muonZTagContainer
muonTnpJPsiAlg.probes = preselectedTrackContainer
muonTnpJPsiAlg.massWindowLow = muonJPsiTagMassWindow[0]
muonTnpJPsiAlg.massWindowHigh = muonJPsiTagMassWindow[1]
muonTnpJPsiAlg.maxDeltaZ0 = muonJPsiTagMaxDeltaZ0
muonTnpJPsiAlg.oppositeCharge = False
muonTnpJPsiAlg.p4Decoration = label+'_p4'
muonTnpJPsiAlg.deltaZ0Decoration = label+'_deltaZ0'
muonTnpJPsiAlg.qxqDecoration = label+'_qxq'
muonTnpJPsiAlg.selectionDecoration = label+',as_char'
athAlgSeq += muonTnpJPsiAlg
# add the corresponding variables
inDetTrackWriterTool.trackVariables += [
    '<char> {0} -> <bool> {0}'.format(label),
    '<TLorentzVector> {0}_p4'.format(label),
    '<float> {0}_deltaZ0'.format(label),
    '<char> {0}_qxq'.format(label),
]
trackSelection[0] += '||{}'.format(muonTnpJPsiAlg.selectionDecoration)

# select tracks matching to electrons
label = 'matched_electron'
electronMatchAlg = createAlgorithm('xTRT::TrackToLeptonMatchAlg', 'ElectronMatchAlg')
electronMatchAlg.leptons = electronMatchContainer
electronMatchAlg.tracks = preselectedTrackContainer
electronMatchAlg.selectionDecoration = label+",as_char"
athAlgSeq += electronMatchAlg
inDetTrackWriterTool.trackVariables.append('<char> {0} -> <bool> {0}'.format(label))
trackSelection[0] += '||{}'.format(electronMatchAlg.selectionDecoration)

# select tracks matching to muons
label = 'matched_muon'
muonMatchAlg = createAlgorithm('xTRT::TrackToLeptonMatchAlg', 'MuonMatchAlg')
muonMatchAlg.leptons = muonMatchContainer
muonMatchAlg.tracks = preselectedTrackContainer
muonMatchAlg.selectionDecoration = label+",as_char"
athAlgSeq += muonMatchAlg
inDetTrackWriterTool.trackVariables.append('<char> {0} -> <bool> {0}'.format(label))
trackSelection[0] += '||{}'.format(muonMatchAlg.selectionDecoration)

label = 'conversion'
gsfConversionVertexSelectionAlg = createAlgorithm('xTRT::TrackFromVertexSelectionAlg', 'GSFTrackConversionVertexSelectioinAlg')
gsfConversionVertexSelectionAlg.tracks = gsfTrackContainer
gsfConversionVertexSelectionAlg.vertices = 'GSFConversionVertices'
gsfConversionVertexSelectionAlg.vertexSelectionDecoration = label+',as_char'
gsfConversionVertexSelectionAlg.vertexChi2Decoration = label+'_chi2'
gsfConversionVertexSelectionAlg.vertexDoFDecoration = label+'_dof'
gsfConversionVertexSelectionAlg.vertexP4Decoration = label+'_p4'
gsfConversionVertexSelectionAlg.vertexPositionDecoration = label+'_position'
athAlgSeq += gsfConversionVertexSelectionAlg
gsfTrackWriterTool.trackVariables += [
    '<char> {0} -> <bool> {0}'.format(label),
    '<float> {}_chi2'.format(label),
    '<uint> {}_dof'.format(label),
    '<TLorentzVector> {}_p4'.format(label),
    '<TVector3> {}_position'.format(label),
]
gsfTrackSelection = [gsfConversionVertexSelectionAlg.vertexSelectionDecoration]

# select InDetTracks based on links to secondary vertices and decorate information
for secondaryVertexContainer in secondaryVertexContainers:
    label = secondaryVertexContainer.replace('Vertices', '')
    alg = createAlgorithm('xTRT::TrackFromVertexSelectionAlg', 'Track{}SelectionAlg'.format(label))
    alg.tracks = preselectedTrackContainer
    alg.vertices = secondaryVertexContainer
    alg.vertexSelectionDecoration = label+',as_char'
    alg.vertexChi2Decoration = label+'_chi2'
    alg.vertexDoFDecoration = label+'_dof'
    alg.vertexP4Decoration = label+'_p4'
    alg.vertexPositionDecoration = label+'_position'
    athAlgSeq += alg
    inDetTrackWriterTool.trackVariables += [
        '<char> {0} -> <bool> {0}'.format(label),
        '<float> {}_chi2'.format(label),
        '<uint> {}_dof'.format(label),
        '<TLorentzVector> {}_p4'.format(label),
        '<TVector3> {}_position'.format(label),
    ]
    trackSelection[0] += '||{}'.format(alg.vertexSelectionDecoration)

# find matching GSF tracks that have been accepted explicitely and add them to the accepted tracks
label = 'matched_gsf'
gsfMatchAlg = createAlgorithm('xTRT::TrackToTrackMatchAlg', 'GSFMatchAlg')
gsfMatchAlg.tracksToMatch = gsfTrackContainer
gsfMatchAlg.preselection = gsfTrackSelection[0]
gsfMatchAlg.tracks = preselectedTrackContainer
gsfMatchAlg.selectionDecoration = label+",as_char"
athAlgSeq += gsfMatchAlg
trackSelection[0] += '||{}'.format(gsfMatchAlg.selectionDecoration)

# label high pT tracks that have been accepted explicitely and add them to the accepted tracks
label = 'matched_highPt'
highPtMatchAlg = createAlgorithm('xTRT::TrackToTrackMatchAlg', 'HighPtMatchAlg')
highPtMatchAlg.tracksToMatch = selectedTrackContainerHighPt
highPtMatchAlg.tracks = preselectedTrackContainer
highPtMatchAlg.selectionDecoration = label+",as_char"
athAlgSeq += highPtMatchAlg
trackSelection[0] += '||{}'.format(highPtMatchAlg.selectionDecoration)

# make a view container that only contains the selected tracks
trackSelectionAlg = createAlgorithm('CP::AsgViewFromSelectionAlg', 'SelectedTrackViewAlg')
trackSelectionAlg.input = preselectedTrackContainer
trackSelectionAlg.output = selectedTrackContainer
trackSelectionAlg.deepCopy = False
# select only tracks that pass electron or muon TnP selection
trackSelectionAlg.selection = trackSelection
athAlgSeq += trackSelectionAlg

# select all GSF tracks that can be matched to accepted tracks
label = 'matched_gsf'
gsfMatchAlg = createAlgorithm('xTRT::TrackToTrackMatchAlg', 'TrackToGSFMatchAlg')
gsfMatchAlg.tracksToMatch = selectedTrackContainer
gsfMatchAlg.tracks = gsfTrackContainer
gsfMatchAlg.trackAccessor = 'originalTrackParticle'
gsfMatchAlg.trackToMatchAccessor = ''
gsfMatchAlg.selectionDecoration = label+",as_char"
athAlgSeq += gsfMatchAlg
gsfTrackSelection[0] += '||{}'.format(gsfMatchAlg.selectionDecoration)

# make a view container that only contains the selected GSF tracks
gsfTrackSelectionAlg = createAlgorithm('CP::AsgViewFromSelectionAlg', 'SelectedGSFTrackViewAlg')
gsfTrackSelectionAlg.input = gsfTrackContainer
gsfTrackSelectionAlg.output = selectedGsfTrackContainer
gsfTrackSelectionAlg.deepCopy = False
# select only GSF tracks that are matched to a relevant InDetTrack
gsfTrackSelectionAlg.selection = [gsfMatchAlg.selectionDecoration]
athAlgSeq += gsfTrackSelectionAlg

# set up the algorithm to fill the ROOT tree
trackWriter = createAlgorithm( 'xTRT::TRTTrackWriterAlg', 'TrackWriter' )
trackWriter.isMC = dataType in [ 'mc', 'afii' ]
trackWriter.treeName = outputTreeName
trackWriter.truthParticles = selectedTruthParticleContainer
trackWriter.electrons = electronContainer
trackWriter.muons = muonContainer
trackWriter.trackWriterTools = [inDetTrackWriterTool, gsfTrackWriterTool]
# store only those electron and muon objects that have a corresponding selected track object
trackWriter.storeOnlyObjectsMatchedToTracks = True
athAlgSeq += trackWriter

#############################
# End of algorithm sequence #
#############################



MessageSvc.defaultLimit=100000
# limit the number of events (for testing purposes)
#from AthenaCommon.AppMgr import ServiceMgr
#ServiceMgr.EventSelector.SkipEvents = 10
theApp.EvtMax = -1

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")