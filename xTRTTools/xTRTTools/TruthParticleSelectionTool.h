/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef XTRTTOOLS_TRUTHPARTICLESELECTIONTOOL_H
#define XTRTTOOLS_TRUTHPARTICLESELECTIONTOOL_H

#include <AsgTools/AsgTool.h>
#include <AthContainers/AuxElement.h>
#include <PATCore/IAsgSelectionTool.h>
#include <vector>

namespace xTRT {
  /// \brief an \ref IAsgSelectionTool that selects truth particles.
  ///
  /// This tool allows to select truth particles depending on their
  /// status, their absolute PDG ID and their parent PDG ID.

  class TruthParticleSelectionTool final
    : public asg::AsgTool, virtual public IAsgSelectionTool
  {
    // Create a proper constructor for Athena
    ASG_TOOL_CLASS( TruthParticleSelectionTool, IAsgSelectionTool )

    /// \brief standard constructor
    /// \par Guarantee
    ///   strong
    /// \par Failures
    ///   out of memory II
  public:
    TruthParticleSelectionTool (const std::string& name);

    //
    // inherited interface
    //
    virtual StatusCode initialize () override;
    virtual const asg::AcceptInfo& getAcceptInfo( ) const override;
    virtual asg::AcceptData accept( const xAOD::IParticle* /*part*/ ) const override;
    //virtual const Root::TAccept& getTAccept( ) const override;
    //virtual const Root::TAccept& accept( const xAOD::IParticle* /*part*/ ) const override;


  private:
    /// tool properties
    std::vector<int> m_statusCodes;
    std::vector<int> m_pdgIds;
    std::vector<int> m_parentPdgIds;
    bool m_printCastWarning;

    /// Index for the status code selection
    int m_statusCodeCutIndex{ -1 };
    /// Index for the PDG ID selection
    int m_pdgIdCutIndex{ -1 };
    /// Index for the parent PDG ID selection
    int m_parentPdgIdCutIndex{ -1 };

    /// \brief the \ref TAccept we are using
  private:
    // r22 uses asg::AcceptInfo
    asg::AcceptInfo m_accept;
    // r21 uses Root::TAccept
    //mutable Root::TAccept m_accept;
  };
}

#endif
