/** @file  Utils.h
 *  @brief xTRTTools utilities
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#ifndef xTRTTools_Utils_h
#define xTRTTools_Utils_h

#include <cstdlib>
#include <iostream>

#define GeV   1000.0
#define toGeV 0.0010

#include <xAODTracking/TrackMeasurementValidationContainer.h>
#include <xAODTracking/TrackStateValidationContainer.h>

namespace xTRT {
  using MSOS        = xAOD::TrackStateValidation;
  using DriftCircle = xAOD::TrackMeasurementValidation;

  auto stringSplit(const std::string &s, char delim, std::vector<std::string> &elems);
  auto stringSplit(const std::string &s, char delim);
}

inline auto xTRT::stringSplit(const std::string &s, char delim, std::vector<std::string> &elems) {
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.emplace_back(item);
  }
  return elems;
}

inline auto xTRT::stringSplit(const std::string &s, char delim) {
  std::vector<std::string> elems;
  xTRT::stringSplit(s, delim, elems);
  return elems;
}

/*! \def XTRT_WARNING
  Print warning message for non Algorithm class warnings
*/
#define XTRT_WARNING(MSG)						\
  { std::cout << "XTRT_WARNING: " << MSG << '\n' << std::flush; }

/*!
  \def XTRT_FATAL
  Exit and print message including function
*/
#define XTRT_FATAL(MSG)                                                 \
  { std::cerr << "XTRT_FATAL: "						\
	      << __PRETTY_FUNCTION__ << MSG << '\n' << std::flush;	\
    std::exit(EXIT_FAILURE); }

#endif
