#ifndef xTRTTools_ExampleAlg_H
#define xTRTTools_ExampleAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>

namespace xTRT {
  class ExampleAlg : public EL::AnaAlgorithm {
  public:
    // this is a standard algorithm constructor
    ExampleAlg(const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize() override;
    virtual StatusCode execute()    override;
    virtual StatusCode finalize()   override;

  private:

    // GRL tool handle and a vector for GRL xml files
    asg::AnaToolHandle<IGoodRunsListSelectionTool>
    m_GRLTool{"GoodRunsListSelectionTool/GRLTool",this};
    std::vector<std::string> m_GRLfiles;

    // Configuration, and any other types of variables go here.
    std::string m_inputTrackContainerName;
    std::string m_treeName;

    TTree* m_tree {nullptr};

    int   m_eventNumber;
    float m_eventWeight;
    float m_track_pt;
    int   m_track_nTRT;

  };
}

#endif
