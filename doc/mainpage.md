# xTRTTools Doxygen

This is the Doxygen based API documentation for `xTRTTools`.

If you haven't taken a look at the tutorial, start here:
http://trtswdocs.web.cern.ch/xtrttools
