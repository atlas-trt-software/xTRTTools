#ifndef XTRTTOOLSVECTORDICT_H
#define XTRTTOOLSVECTORDICT_H

#include <vector>
#include <TVector3.h>
#include <TLorentzVector.h>

namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XTRTTOOLSVECTOR {
    std::vector<TVector3> std_vTVector3;
    std::vector<TLorentzVector> std_vTLorentzVector;
  };
}

#endif