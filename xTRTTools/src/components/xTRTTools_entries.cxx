#include <xTRTTools/ExampleAlg.h>
#include <xTRTTools/TRTTrackWriterAlg.h>
#include <xTRTTools/AsgLeptonTrackSelectionAlg.h>
#include <xTRTTools/TagAndProbeAlg.h>
#include <xTRTTools/TrackFromVertexSelectionAlg.h>
#include <xTRTTools/TrackToLeptonMatchAlg.h>
#include <xTRTTools/TrackToTrackMatchAlg.h>
#include <xTRTTools/TrackWriterTool.h>
#include <xTRTTools/TruthParticleSelectionTool.h>

// DECLARE_ALGORITHM_FACTORY (...)
DECLARE_COMPONENT(xTRT::ExampleAlg)
DECLARE_COMPONENT(xTRT::AsgLeptonTrackSelectionAlg)
DECLARE_COMPONENT(xTRT::TRTTrackWriterAlg)
DECLARE_COMPONENT(xTRT::TagAndProbeAlg)
DECLARE_COMPONENT(xTRT::TrackFromVertexSelectionAlg)
DECLARE_COMPONENT(xTRT::TrackToLeptonMatchAlg)
DECLARE_COMPONENT(xTRT::TrackToTrackMatchAlg)
DECLARE_COMPONENT(xTRT::TrackWriterTool)
DECLARE_COMPONENT(xTRT::TruthParticleSelectionTool)