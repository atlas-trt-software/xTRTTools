/** @file  Accessors.h
 *  @brief xTRT supplied Accessors header
 *
 *  This file houses a bunch of predefined accessor variables for
 *  you. They are meant to be used with the xTRT::Algorithm::get
 *  function.
 *
 *  @namespace xTRT::Acc
 *  @brief namespace for const xAOD AuxElement accessors
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#ifndef TRTFramework_Accessors_h
#define TRTFramework_Accessors_h
#pragma once

// ATLAS
#include <AthContainers/AuxElement.h>
#include <xAODTracking/TrackStateValidationContainer.h>
#include <xAODTruth/TruthParticleContainer.h>

namespace xTRT {
  namespace Acc {

    /** @addtogroup acc_Event Aux Accessors for Events
     *  @brief SG::AuxElement::ConstAccessor variables for event properties
     *  @{
     */

    using ull = unsigned long long;

    const SG::AuxElement::ConstAccessor<float> TRTOccGlobal  {"TRTOccGlobal"};
    const SG::AuxElement::ConstAccessor<float> TRTOccBarrelA {"TRTOccBarrelA"};
    const SG::AuxElement::ConstAccessor<float> TRTOccBarrelC {"TRTOccBarrelC"};
    const SG::AuxElement::ConstAccessor<float> TRTOccEndcapAA{"TRTOccEndcapAA"};
    const SG::AuxElement::ConstAccessor<float> TRTOccEndcapBA{"TRTOccEndcapBA"};
    const SG::AuxElement::ConstAccessor<float> TRTOccEndcapAC{"TRTOccEndcapAC"};
    const SG::AuxElement::ConstAccessor<float> TRTOccEndcapBC{"TRTOccEndcapBC"};
    const SG::AuxElement::ConstAccessor<float> TrtPhaseTime  {"TrtPhaseTime"};

    /** @}*/

    /** @addtogroup acc_Track Aux Accessors for Tracks
     *  @brief SG::AuxElement::ConstAccessor variables for track properties
     *  @{
     */
    const SG::AuxElement::ConstAccessor<float> eProbabilityToT          {"eProbabilityToT"};
    const SG::AuxElement::ConstAccessor<float> eProbabilityHT           {"eProbabilityHT"};
    const SG::AuxElement::ConstAccessor<float> eProbabilityNN           {"eProbabilityNN"};
    const SG::AuxElement::ConstAccessor<float> TRTTrackOccupancy        {"TRTTrackOccupancy"};
    const SG::AuxElement::ConstAccessor<float> TRTdEdx                  {"TRTdEdx"};
    const SG::AuxElement::ConstAccessor<float> ToT_dEdx_noHT_divByL     {"ToT_dEdx_noHT_divByL"};
    const SG::AuxElement::ConstAccessor<float> ToT_usedHits_noHT_divByL {"ToT_usedHits_noHT_divByL"};
    const SG::AuxElement::ConstAccessor<float> truthMatchProbability    {"truthMatchProbability"};


    const SG::AuxElement::ConstAccessor<unsigned char> numberOfTRTHits     {"numberOfTRTHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfTRTOutliers {"numberOfTRTOutliers"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfInnermostPixelLayerHits
    {"numberOfInnermostPixelLayerHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfNextToInnermostPixelLayerHits
    {"numberOfNextToInnermostPixelLayerHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfPixelHits    {"numberOfPixelHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfSCTHits      {"numberOfSCTHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfPixelHoles   {"numberOfPixelHoles"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfSCTHoles     {"numberOfSCTHoles"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfTRTHoles     {"numberOfTRTHoles"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfTRTXenonHits {"numberOfTRTXenonHits"};

    const SG::AuxElement::ConstAccessor<float> numberDoF  {"numberDoF"};
    const SG::AuxElement::ConstAccessor<float> chiSquared {"chiSquared"};

    const SG::AuxElement::ConstAccessor<
      ElementLink<xAOD::TruthParticleContainer>
      > truthParticleLink{"truthParticleLink"};

    const SG::AuxElement::ConstAccessor<std::vector<ElementLink<xAOD::TrackStateValidationContainer>>> msosLink{"msosLink"};
    const SG::AuxElement::ConstAccessor<std::vector<ElementLink<xAOD::TrackStateValidationContainer>>> Reco_msosLink{"Reco_msosLink"};
    const SG::AuxElement::ConstAccessor<std::vector<ElementLink<xAOD::TrackStateValidationContainer>>> GSF_msosLink{"GSF_msosLink"};



    /** @} */

    /** @addtogroup acc_DCandMSOS Aux Accessors for MSOSs and Drift Circles
     *  @brief SG::AuxElement::ConstAccessor variables for shared between MSOSs and Drift Circles
     *  @{
     */

    // for MSOS and drift Circle
    const SG::AuxElement::ConstAccessor<float> localX {"localX"};
    const SG::AuxElement::ConstAccessor<float> localY {"localY"};

    /** @} */

    /** @addtogroup acc_DriftCircle Aux Accessors for Drift Circles
     *  @brief SG::AuxElement::ConstAccessor variables for drift circle properties
     *  @{
     */

#if __clang__
    const SG::AuxElement::ConstAccessor<ull> identifier {"identifier"};
    const SG::AuxElement::ConstAccessor<std::vector<ull>> rdoIdentifierList{"rdoIdentifierList"};
#else
    const SG::AuxElement::ConstAccessor<unsigned long> identifier {"identifier"};
    const SG::AuxElement::ConstAccessor<std::vector<unsigned long>> rdoIdentifierList{"rdoIdentifierList"};
#endif

    const SG::AuxElement::ConstAccessor<float> localXError{"localXError"};

    const SG::AuxElement::ConstAccessor<float> globalX {"globalX"};
    const SG::AuxElement::ConstAccessor<float> globalY {"globalY"};
    const SG::AuxElement::ConstAccessor<float> globalZ {"globalZ"};

    const SG::AuxElement::ConstAccessor<unsigned int>  bitPattern    {"bitPattern"};
    const SG::AuxElement::ConstAccessor<char>          gasType       {"gasType"};
    const SG::AuxElement::ConstAccessor<int>           bec           {"bec"};
    const SG::AuxElement::ConstAccessor<int>           phi_module    {"phi_module"};
    const SG::AuxElement::ConstAccessor<int>           layer         {"layer"};
    const SG::AuxElement::ConstAccessor<int>           strawlayer    {"strawlayer"};
    const SG::AuxElement::ConstAccessor<int>           strawnumber   {"strawnumber"};
    const SG::AuxElement::ConstAccessor<int>           TRTboard      {"TRTboard"};
    const SG::AuxElement::ConstAccessor<int>           TRTchip       {"TRTchip"};
    const SG::AuxElement::ConstAccessor<float>         drifttime     {"drifttime"};
    const SG::AuxElement::ConstAccessor<int>           status        {"status"};
    const SG::AuxElement::ConstAccessor<float>         tot           {"tot"};
    const SG::AuxElement::ConstAccessor<char>          isHT          {"isHT"};
    const SG::AuxElement::ConstAccessor<char>          highThreshold {"highThreshold"};
    const SG::AuxElement::ConstAccessor<float>         T0            {"T0"};
    const SG::AuxElement::ConstAccessor<float>         leadingEdge   {"leadingEdge"};
    const SG::AuxElement::ConstAccessor<float>         strawphi      {"strawphi"};

    const SG::AuxElement::ConstAccessor<float> driftTimeToTCorrection{"driftTimeToTCorrection"};
    const SG::AuxElement::ConstAccessor<float> driftTimeHTCorrection {"driftTimeHTCorrection"};

    /** @} */

    /** @addtogroup acc_MSOS Aux Accessors for MSOSs
     *  @brief SG::AuxElement::ConstAccessor variables for msos properties
     *  @{
     */

    const SG::AuxElement::ConstAccessor<int>           type         {"type"};

#if __clang__
    const SG::AuxElement::ConstAccessor<ull>           detElementId {"detElementId"};
#else
    const SG::AuxElement::ConstAccessor<unsigned long> detElementId {"detElementId"};
#endif

    const SG::AuxElement::ConstAccessor<float>         localTheta   {"localTheta"};
    const SG::AuxElement::ConstAccessor<float>         localPhi     {"localPhi"};
    const SG::AuxElement::ConstAccessor<float>         HitZ         {"HitZ"};
    const SG::AuxElement::ConstAccessor<float>         HitR         {"HitR"};
    const SG::AuxElement::ConstAccessor<float>         rTrkWire     {"rTrkWire"};
    const SG::AuxElement::ConstAccessor<float>         errDC        {"errDC"};

    const SG::AuxElement::ConstAccessor<float> unbiasedResidualX{"unbiasedResidualX"};
    const SG::AuxElement::ConstAccessor<float> biasedResidualX  {"biasedResidualX"};
    const SG::AuxElement::ConstAccessor<float> unbiasedPullX    {"unbiasedPullX"};
    const SG::AuxElement::ConstAccessor<float> biasedPullX      {"biasedPullX"};

    /** @} */
  }

  /** @addtogroup AuxHelp Aux Helpers
   *  @brief helpers for dealing with auxiliary data
   *  @{
   */

  /// grab aux data by using ConstAccessor and some object
  /**
   *  Using a ConstAccessor, look to see if the object has the
   *  auxdata and then return it. This is a wrapper around the
   *  ConstAccessor class to also print a warning if the auxdata
   *  isn't there.
   *
   *  Should be used with: @ref acc_Event, @ref acc_Track, @ref
   *  acc_DCandMSOS, @ref acc_MSOS, @ref acc_DriftCircle
   *
   *  Example usage (with the "bec" drift circle auxdata and
   *  "eProbability" track auxdata) given the existing xAOD objects:
   *
   *  @code{.cpp}
   *  auto track       = ...; // an xAOD::TrackParticle object
   *  auto driftCircle = ...; // an xTRT::DriftCircle object
   *  int brlec    = get(xTRT::Acc::bec,driftCircle);
   *  float trkOcc = get(xTRT::Acc::eProbabilityHT,track,"eProbabilityHT");
   *  @endcode
   *
   *  @param acc the const accessor object
   *  @param xobj the xAOD object to grab the auxdata from
   *  @param adn the auxdata variable name (only used for error message,
   *  default is blank)
   */
  template <class T>
  static const T get(const SG::AuxElement::ConstAccessor<T>& acc,
                     const SG::AuxElement* xobj, const std::string& adn = "");

  /// grab aux data from an xAOD object based on name
  /**
   *  This function will create a ConstAccessor to retrieve some aux
   *  data from the object. This is differen from the get function
   *  in that you must supply the return type, where with get the
   *  return type is deduced by the compiler from the already
   *  declared ConstAccessor.
   *
   *  @param xobj the xAOD object to retrieve the auxdata from
   *  @param adn the aux data variable name
   */
  template <typename T1, typename T2 = SG::AuxElement>
  static const T1 retrieve(const T2* xobj, const std::string& adn);
}

#include "Accessors.icc"

#endif