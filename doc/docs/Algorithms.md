# `xTRTTools` Algorithms

## `TagAndProbeAlg`

The `TagAndProbeAlg` allows to select objects from a set of probes.
The selection is based on the invariant mass formed with a tag object,
the difference in z0 at the interaction point and the charge product.
All possible combinations with a set of tag objects are checked.

The algorithm supports any combination of TrackParticle, Electron or Muon
containers either as tag or probe objects.

The decision whether probe object passes the selection is stored as
a `char` decoration. If selected, the invariant mass, the z0 difference
and charge product is stored as additional decoration on the probe object.

See [TagAndProbeAlg.h](../../xTRTTools/xTRTTools/TagAndProbeAlg.h) and [TagAndProbeAlg.cxx](../../xTRTTools/Root/TagAndProbeAlg.cxx).

The following steering paramters are available:
- `tags`: Name of container with tag objects. Can be of type electron, muon or trackParticle.
- `probes`: Name of container with probe objects. Can be of type electron, muon or trackParticle.
- `selectionDecoration`: Name of the decoration for the tag-and-probe decision that is added to the probe objects. Needs to be stored as `char`, e.g, `"tnp_selected,as_char"`.
- `massDecoration`: Name of the decoration to store the invariant mass of selected objects that is added to the selected objects.
- `deltaZ0Decoration`: Name of the decoration to store the z0 difference of selected objects that is added to the selected objects.
- `qxqDecoration`: Name of the decoration to store the charge product of selected objects that is added to the selected objects.
- `massWindowLow`: Lower boundary for invariant mass window. Set to 0 to accept all.
- `massWindowHigh`: Upper boundary for invariant mass window. Set to 0 to accept all.
- `maxDeltaZ0`: Maximum difference in z0 at the interaction point between the tracks of the tag and probe object. Set to 0 to accept all.
- `oppositeCharge`: Decide whether only probe objects with opposite charge than the tag object are accepted.


## `TRTTrackWriterAlg`

The `TRTTrackWriterAlg` provides a flexible way to store TrackParticle
information, as well as corresponding TruthParticle, Electron and
Muon information in a ROOT `TTree`.

Each track corresponds to an entry in the resulting ROOT tree.
TruthParticles, Electrons and Muons are matched and stored
in parallel branches.

Multiple track containers can be processed and will be written
as parallel branches. For example the default InDet tracks can be
stored alongside the corresponding GSF tracks.

The list of variables to be stored for the various objects can be
modified from the job options. In particular, any quantity that
is readily available as xAOD aux or auxDyn variable can be added
without modification of the c++ code. The default list of variables
and variables which need to be calculated from xAOD information
are implemented in [TRTTrackWriterAlg.cxx](../../xTRTTools/Root/TreeFiller.cxx).

See [TRTTrackWriterAlg.h](../../xTRTTools/xTRTTools/TRTTrackWriterAlg.h) and [TRTTrackWriterAlg.cxx](../../xTRTTools/Root/TRTTrackWriterAlg.cxx).

The following steering paramters are available:
- `truthContainerName`: Name of the truth particle container to process. Set to empty to store no truth particles.
- `truthPrefix`: Prefix used for the truth particle branches. Default is `truth_`.
- `primaryVertexContainerName`: Name of the vertex container to process. Default is set to `PrimaryVertices`.
- `primaryVertexPrefix`: Prefix used for the primary vertex branches. Default is `primaryVertex_`.
- `trackContainerNames`: List of track container names to process.
- `trackPrefixes`: List of prefixes used for the track branches. Must be empty or match the length of `trackContainerNames`.
- `muonContainerName`: Name of the muon container to process. Default is set to `Muons`.
- `muonPrefix`: Prefix used for the muon branches. Default is `muon_`.
- `electronContainerName`: Name of the electron container to process. Default is set to `Electrons`.
- `electronPrefix`: Prefix used for the electron branches. Default is `electron_`.
- `treeName`: Name of the output ROOT `TTree`.
- `storeHits`: Decide whether to store TRT hit information for tracks.
- `storeElectronGsfTracks`: Store the GSF tracks for electrons with the `electronPrefix`.
- `storeOnlyObjectsMatchedToTracks`: Decide whether to store only those electron and muon objects that correspond to a track object.
- `isMC`: Decide whether the sample is simulated or real data. Needed for correct treatment of truth particles.
- `eventInfoVariables`: List of event info variables to fill.
- `trackVariables`: List of track variables to fill.
- `measurementVariables`: List of track hit variables to fill from `MeasurementOnSurface` objects.
- `driftCircleVariables`: List of track hit variables to fill from `DriftCircle` objects.
- `electronVariables`: List of electron variables to fill.
- `electronGsfTrackVariables`: List of track variables to fill for GSF tracks from electrons. Independent of `trackVariables`.
- `muonVariables`: List of muon variables to fill.