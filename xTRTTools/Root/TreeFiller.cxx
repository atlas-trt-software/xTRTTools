/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/TreeFiller.h>
#include <xTRTTools/Helpers.h>

#include <numeric>

namespace xTRT {

  // initialisation of default variable lists
  const std::vector<std::string> EventInfoFiller::defaultVariables = {
    "<uint> runNumber",
    "<ulong> eventNumber",
    "<float> averageInteractionsPerCrossing",
    "<float> TRTOccGlobal -> TRTOccupancyGlobal",
    "<float> TRTOccBarrelA -> TRTOccupancyBarrelA",
    "<float> TRTOccBarrelC -> TRTOccupancyBarrelC",
    "<float> TRTOccEndcapAA -> TRTOccupancyEndcapAA",
    "<float> TRTOccEndcapAC -> TRTOccupancyEndcapAC",
    "<float> TRTOccEndcapBA -> TRTOccupancyEndcapBA",
    "<float> TRTOccEndcapBC -> TRTOccupancyEndcapBC"
  };
  
  const std::vector<std::string> VertexFiller::defaultVariables = {
    "<float> sumPt2"
  };

  const std::vector<std::string> ParticleFiller::defaultVariables = {

  };

  const std::vector<std::string> TruthParticleFiller::defaultVariables = {
    "<int> status",
    "<int> pdgId"
  };

  const std::vector<std::string> TrackFiller::defaultVariables = {
    "<uchar> numberOfPixelHits -> nPixelHits",
    "<uchar> numberOfPixelHoles -> nPixelHoles",
    "<uchar> numberOfSCTHits -> nSCTHits",
    "<uchar> numberOfSCTHoles -> nSCTHoles",
    "<uchar> numberOfTRTHits -> nTRTHits_athena",
    "<uchar> numberOfTRTXenonHits -> nTRTHitsXe",
    "<uchar> numberOfTRTHoles -> nTRTHoles",
    "<uchar> numberOfTRTOutliers -> nTRTOutliers_athena",
    "<uchar> numberOfTRTHighThresholdHitsTotal -> nTRTHitsHT",
    "<uchar> numberOfTRTHighThresholdHits -> nTRTHitsHTXe",
    "<float> qOverP",
    "<float> d0",
    "<float> z0",
    "<float> TRTdEdx -> dEdx",
    "<uchar> TRTdEdxUsedHits -> dEdx_usedHits",
    "<float> eProbabilityHT",
    "<float> eProbabilityNN",
    "<float> TRTTrackOccupancy -> trackOccupancy",
    "<float> chiSquared -> chi2",
    "<float> vz",
    "<uchar> particleHypothesis",
  };

  const std::vector<std::string> TrackFiller::defaultMeasurementVariables = {
    "<int> type -> <char> type_athena",
    "<float> HitR -> r",
    "<float> HitZ -> z",
    "<float> localTheta",
    "<float> localPhi",
    "<float> rTrkWire -> rTrackToWire",
    "<float> driftTime",
    "<float> unbiasedResidualX -> unbiasedResidual",
    "<float> biasedResidualX -> biasedResidual",
    "<float> unbiasedPullX -> unbiasedPull",
    "<float> biasedPullX -> biasedPull",
    "<float> errDC -> error",
    "<char> isShared -> <bool> isShared"
  };

  const std::vector<std::string> TrackFiller::defaultDriftCircleVariables = {
    "<char> gasType",
    "<int> bec -> <short> bec",
    "<int> layer -> <char> layer",
    "<int> strawlayer -> <char> strawLayer",
    "<int> strawnumber -> <char> strawNumber",
    "<int> TRTboard -> <char> board",
    "<int> TRTchip -> <ushort> chip",
    "<int> phi_module -> <char> modulePhi",
    "<uint> bitPattern",
    "<float> strawphi -> strawPhi",
    "<float> T0 -> t0",
    "<float> leadingEdge",
    "<float> localX -> driftRadius",
    "<float> tot"
  };

  const std::vector<std::string> ElectronFiller::defaultVariables = {
    "<char> LHLoose -> <bool> LHLoose",
    "<char> LHMedium -> <bool> LHMedium",
    "<char> LHTight -> <bool> LHTight",
    "<char> Loose -> <bool> Loose",
    "<char> Medium -> <bool> Medium",
    "<char> Tight -> <bool> Tight",
    "<float> charge",
    "<float> ptcone20",
    "<float> ptcone30",
    "<float> ptvarcone20",
    "<float> ptvarcone30",
    "<float> topoetcone20",
    "<float> topoetcone30",
    "<float> topoetcone40"
  };

  const std::vector<std::string> MuonFiller::defaultVariables = {
    "<ushort> author",
    "<ushort> muonType -> type",
    "<uchar> quality -> quality",
    "<float> charge",
    "<float> ptcone20",
    "<float> ptcone30",
    "<float> ptcone40",
    "<float> ptvarcone20",
    "<float> ptvarcone30",
    "<float> ptvarcone40",
    "<float> topoetcone20",
    "<float> topoetcone30",
    "<float> topoetcone40",
    "<uchar> numberOfPrecisionLayers"
  };

  TreeFiller::TreeFiller(const std::string& prefix, TTree* tree,
      const std::vector<std::string>& variables, bool useVectorBranches, bool isMC)
        : m_isMC(isMC) {
    for (const auto& expression : variables) {
      if (useVectorBranches) {
        m_variables.push_back(std::unique_ptr<IBranchProxy>(BranchProxyFactory::setupVectorBranch(tree, expression, prefix)));
      } else {
        m_variables.push_back(std::unique_ptr<IBranchProxy>(BranchProxyFactory::setupBranch(tree, expression, prefix)));
      }
    }
  }

  bool TreeFiller::fill(const SG::AuxElement* element) {
    for (auto& variable : m_variables) {
      variable->fill(element);
    }
    return true;
  }

  void TreeFiller::reset() {
    for (auto& variable : m_variables) {
      variable->reset();
    }      
  }

  EventInfoFiller::EventInfoFiller(const std::string& prefix, TTree* tree, const std::vector<std::string>& variables, bool isMC)
    : TreeFiller(prefix, tree, variables, false, isMC)  {
    // set up branches
    m_beamSpot = new TVector3();
    tree->Branch((prefix + "eventWeight").c_str(), &m_eventWeight, (prefix + "eventWeight/F").c_str());
    tree->Branch((prefix + "beamSpot").c_str(), &m_beamSpot);
  }

  EventInfoFiller::~EventInfoFiller() {
    delete m_beamSpot;
  }

  bool EventInfoFiller::fill(const xAOD::EventInfo* eventInfo) {
    bool result = TreeFiller::fill(eventInfo);
    m_eventWeight = 1.0;
    if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
      m_eventWeight = eventInfo->mcEventWeights().at(0);
    }
    m_beamSpot->SetXYZ(eventInfo->beamPosX(), eventInfo->beamPosY(), eventInfo->beamPosZ());
    return result;
  }

  void EventInfoFiller::reset() {
    TreeFiller::reset();
    m_eventWeight = 0.;
    m_beamSpot->SetXYZ(0., 0., 0.);
  }


  VertexFiller::VertexFiller(const std::string& prefix, TTree* tree, const std::vector<std::string>& variables, bool isMC)
    : TreeFiller(prefix, tree, variables, false, isMC) {
    // set up branches
    m_position = new TVector3();
    tree->Branch((prefix + "position").c_str(), &m_position);
  }

  VertexFiller::~VertexFiller() {
    delete m_position;
  }

  bool VertexFiller::fill(const xAOD::Vertex* vertex) {
    bool result = TreeFiller::fill(vertex);
    m_position->SetXYZ(vertex->x(), vertex->y(), vertex->z());
    return result;
  }

  void VertexFiller::reset() {
    TreeFiller::reset();
    m_position->SetXYZ(0., 0., 0.);
  }


  ParticleFiller::ParticleFiller(const std::string& prefix, TTree* tree, const std::vector<std::string>& variables, bool isMC)
    : TreeFiller(prefix, tree, variables, false, isMC) {
    m_p4 = new TLorentzVector();
    tree->Branch((prefix + "p4").c_str(), &m_p4);
  }

  ParticleFiller::~ParticleFiller() {
    delete m_p4;
  }

  bool ParticleFiller::fill(const xAOD::IParticle* particle) {
    TreeFiller::fill(particle);
    *m_p4 = particle->p4();
    return true;
  }

  void ParticleFiller::reset() {
    TreeFiller::reset();
    m_p4->SetPtEtaPhiM(0., 0., 0., 0.);
  }
    // List of default variables to fill
    static const std::vector<std::string> defaultVariables;


  TruthParticleFiller::TruthParticleFiller(const std::string& prefix, TTree* tree, const std::vector<std::string>& variables, bool isMC) 
    : ParticleFiller(prefix, tree, variables, isMC) {
    // set up special branches
    m_prodVertex = new TVector3();
    tree->Branch((prefix + "prodVertex").c_str(), &m_prodVertex);
    tree->Branch((prefix + "parentPdgId").c_str(), &m_parentPdgId, (prefix + "parentPdgId/I").c_str());
  }

  TruthParticleFiller::~TruthParticleFiller() {
    delete m_prodVertex;
  }

  bool TruthParticleFiller::fill(const xAOD::TruthParticle* particle) {
    bool result = ParticleFiller::fill(particle);
    if (particle->hasProdVtx()) {
      auto vtx = particle->prodVtx();
      m_prodVertex->SetXYZ(vtx->x(), vtx->y(), vtx->z());
    }
    if (particle->nParents() > 0) {
      auto parentParticle = particle->parent(0);
      if (parentParticle != nullptr) {
        try {
          m_parentPdgId = parentParticle->pdgId();
        } catch (std::exception& e) {
          std::cerr << e.what() << std::endl;
        }
      }
    }
    m_charge = particle->charge();
    return result;
  }

  void TruthParticleFiller::reset() {
    ParticleFiller::reset();
    m_prodVertex->SetXYZ(0., 0., 0.);
    m_parentPdgId = 0;
    m_charge = 0;
  }


  TrackFiller::TrackFiller(const std::string& prefix, TTree* tree,
        const std::vector<std::string>& trackVariables,const std::vector<std::string>& measurementVariables,
        const std::vector<std::string>& driftCircleVariables, bool storeHits, bool isMC) 
    : ParticleFiller(prefix, tree, trackVariables, isMC), m_storeHits(storeHits) {

    // variables that need special calculations
    tree->Branch((prefix + "charge").c_str(), &m_charge);
    tree->Branch((prefix + "d0sig").c_str(), &m_d0sig);
    tree->Branch((prefix + "z0sig").c_str(), &m_z0sig);
    tree->Branch((prefix + "z0sinTheta").c_str(), &m_z0sinTheta);
    if (m_storeHits) {
      tree->Branch((prefix + "nTRTHits").c_str(), &m_nTRTHits);
      tree->Branch((prefix + "nTRTOutliers").c_str(), &m_nTRTOutliers);
      tree->Branch((prefix + "nTRTHitsPrecision").c_str(), &m_nHitsPrecision);
      tree->Branch((prefix + "nTRTHitsTube").c_str(), &m_nHitsTube);
      tree->Branch((prefix + "sumL").c_str(), &m_sumL);
      m_measurementFiller = std::make_unique<TreeFiller>(prefix+"hit_", tree, measurementVariables, true, isMC);
      m_driftCircleFiller = std::make_unique<TreeFiller>(prefix+"hit_", tree, driftCircleVariables, true, isMC);

      m_hit_isHighThresholdMiddleBit = new std::vector<bool>();
      m_hit_type = new std::vector<char>();
      m_hit_position = new std::vector<TVector3>();
      m_hit_l = new std::vector<float>();
      m_hit_driftTimeCorrected = new std::vector<float>();
      
      tree->Branch((prefix + "hit_type").c_str(), &m_hit_type);
      tree->Branch((prefix + "hit_isHighThresholdMiddleBit").c_str(), &m_hit_isHighThresholdMiddleBit);
      tree->Branch((prefix + "hit_strawPosition").c_str(), &m_hit_position);
      tree->Branch((prefix + "hit_l").c_str(), &m_hit_l);
      tree->Branch((prefix + "hit_driftTimeCorrected").c_str(), &m_hit_driftTimeCorrected);
    }
  }

  TrackFiller::~TrackFiller() {
    if (m_storeHits) {
      delete m_hit_type;
      delete m_hit_isHighThresholdMiddleBit;
      delete m_hit_position;
      delete m_hit_l;
      delete m_hit_driftTimeCorrected;
    }
  }

  bool TrackFiller::fill(const xAOD::TrackParticle* track) {
    bool result = ParticleFiller::fill(track);

    // update the mass based on the particle hypothesis
    int particleHypothesis = track->particleHypothesis();
    if (particleHypothesis == 1) {
      // electron
      m_p4->SetVectM(m_p4->Vect(), 0.51099891);
    } else if (particleHypothesis == 2) {
      // muon
      m_p4->SetVectM(m_p4->Vect(), 105.658367);
    } else if (particleHypothesis == 4) {
      // kaon
      m_p4->SetVectM(m_p4->Vect(), 493.677);
    } // else leave at default, i.e. pion

    m_charge = track->charge();
    // catch runtime error in case the covariance matrix is missing
    try {
      m_d0sig = xAOD::TrackingHelpers::d0significance(track, m_eventInfo->beamPosSigmaX(), m_eventInfo->beamPosSigmaY(), m_eventInfo->beamPosSigmaXY());
    } catch (const std::runtime_error& e) {
      m_d0sig = 0.;
    }
    // need to catch runtime error which is thrown for too large d0
    try {
      m_z0sig = xAOD::TrackingHelpers::z0significance(track, m_primaryVertex);
    } catch (const std::runtime_error& e) {
      m_z0sig = 0.;
    }
    m_z0sinTheta = xTRT::deltaz0sinTheta(track, m_primaryVertex);

    if (m_storeHits) {
      const xTRT::MSOS* measurement = nullptr;
      const xTRT::DriftCircle* driftCircle = nullptr;
      // loop over all possible accessors for different track containers
      for (auto accessor : {xTRT::Acc::msosLink, xTRT::Acc::Reco_msosLink, xTRT::Acc::GSF_msosLink}) {
        // check that we have a link to the vector of measurement objects
        if (accessor.isAvailable(*track)) {
          for (const auto& measurementLink : accessor(*track) ) {
            // extract measurement on surface
            if (not measurementLink.isValid()) continue;
            measurement = *measurementLink;
            if (measurement->detType() != 3) continue; // TRT hits only.
            
            // fill measurement observables
            result &= m_measurementFiller->fill(measurement);
            m_hit_type->push_back(xTRT::hitType(measurement));

            // get the drift circle object
            if (not (measurement->trackMeasurementValidationLink().isValid())) continue;
            driftCircle = *(measurement->trackMeasurementValidationLink());

            // fill drift circle observables
            result &= m_driftCircleFiller->fill(driftCircle);
            m_hit_l->push_back(xTRT::hitL(measurement, driftCircle));
            m_hit_position->push_back(TVector3(get(xTRT::Acc::globalX, driftCircle), get(xTRT::Acc::globalY, driftCircle), get(xTRT::Acc::globalZ, driftCircle)));
            m_hit_isHighThresholdMiddleBit->push_back(xTRT::hitIsHighThresholdMiddleBit(driftCircle));
            m_hit_driftTimeCorrected->push_back(xTRT::hitCorrectedDriftTime(driftCircle, m_isMC));
          }
          m_nHitsPrecision = std::count(m_hit_type->begin(), m_hit_type->end(), 0);
          m_nHitsTube = std::count(m_hit_type->begin(), m_hit_type->end(), 1);
          m_nTRTOutliers = std::count(m_hit_type->begin(), m_hit_type->end(), 2);
          m_nTRTHits = m_nHitsPrecision+m_nHitsTube;
          m_sumL = std::accumulate(m_hit_l->begin(), m_hit_l->end(), 0.);
          // skip the alternative accessors
          break;
        }
      }
    }
    return result;
  } 

  void TrackFiller::reset() {
    ParticleFiller::reset();

    m_charge = 0.;
    m_d0sig = 0.;
    m_z0sig = 0.;
    m_z0sinTheta = 0.;

    if (m_storeHits) {
      m_nTRTHits = 0;
      m_nTRTOutliers = 0;
      m_nHitsPrecision = 0;
      m_nHitsTube = 0;
      m_sumL = 0.;

      m_measurementFiller->reset();
      m_driftCircleFiller->reset();
      
      m_hit_isHighThresholdMiddleBit->clear();
      m_hit_type->clear();
      m_hit_position->clear();
      m_hit_l->clear();
      m_hit_driftTimeCorrected->clear();
    }
  }

  ElectronFiller::ElectronFiller(const std::string& prefix, TTree* tree, const std::vector<std::string>& variables, bool isMC) 
    : ParticleFiller(prefix, tree, variables, isMC) {
  }

  bool ElectronFiller::fill(const xAOD::Electron* electron) {
    bool result = ParticleFiller::fill(electron);
    return result;
  }

  void ElectronFiller::reset() {
    ParticleFiller::reset();
  }


  MuonFiller::MuonFiller(const std::string& prefix, TTree* tree, const std::vector<std::string>& variables, bool isMC)
    : ParticleFiller(prefix, tree, variables, isMC) {}

  bool MuonFiller::fill(const xAOD::Muon* muon) {
    return ParticleFiller::fill(muon);
  }

  void MuonFiller::reset() {
    ParticleFiller::reset();
  }

}