/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/TrackToLeptonMatchAlg.h>

#include <xAODBase/IParticleHelpers.h>
#include "xAODEgamma/ElectronxAODHelpers.h"
#include <AthContainers/ConstDataVector.h>
#include <AsgMessaging/MessageCheck.h>

#include <iomanip>
#include <sstream>

xTRT::TrackToLeptonMatchAlg::TrackToLeptonMatchAlg(const std::string& name, ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator) {

}

StatusCode xTRT::TrackToLeptonMatchAlg::initialize() {

  // initialise all handles
  ANA_CHECK (m_leptonsHandle.initialize(m_systematicsList));
  ANA_CHECK (m_tracksHandle.initialize(m_systematicsList));
  ANA_CHECK (m_selectionHandle.initialize(m_systematicsList, m_tracksHandle));
  ANA_CHECK (m_systematicsList.initialize());

  return StatusCode::SUCCESS;
}

StatusCode xTRT::TrackToLeptonMatchAlg::execute() {
  for (const auto& sys : m_systematicsList.systematicsVector()) {
    // keep track of the number of selected objects
    auto& counter = m_counters[sys];

    const xAOD::IParticleContainer* leptons = nullptr;
    ANA_CHECK (m_leptonsHandle.retrieve(leptons, sys));

    const xAOD::TrackParticleContainer* trackParticles = nullptr;
    ANA_CHECK (m_tracksHandle.retrieve(trackParticles, sys));

    const xAOD::TrackParticle* leptonTrack = nullptr;
    const xAOD::TrackParticle* gsfTrack = nullptr;
    for (const auto& trackParticle : *trackParticles) {
      counter.nTracks++;
      bool foundLepton = false;
      for (const auto& lepton : *leptons) {
        leptonTrack = nullptr;
        gsfTrack = nullptr;
        const xAOD::Type::ObjectType type = lepton->type();
        if (type == xAOD::Type::Electron) {
          auto electron = dynamic_cast<const xAOD::Electron*>(lepton);
          leptonTrack = xAOD::EgammaHelpers::getOriginalTrackParticle(electron);
          gsfTrack = electron->trackParticle();
        } else if (type == xAOD::Type::Muon) {
          auto muon = dynamic_cast<const xAOD::Muon*>(lepton);
          leptonTrack = muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
        } else {
          ANA_MSG_WARNING("Lepton candidate is neither xAOD::Electron nor xAOD::Muon.");
        }
        if (leptonTrack == nullptr) continue;
        if (trackParticle == leptonTrack || (gsfTrack != nullptr and trackParticle == gsfTrack)) {
          foundLepton = true;
          counter.nAccepted++;
          break;
        }
      }
      m_selectionHandle.setBool(*trackParticle, foundLepton, sys);
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode xTRT::TrackToLeptonMatchAlg::finalize() {
  // determine number of bins for cutflow histograms
  size_t nBins = 2;

  // fill the cutflow histograms
  for (const auto& [sys, counter] : m_counters) {
    std::string histName;
    ANA_CHECK(m_systematicsList.service().makeSystematicsName(histName, "cflow_"+this->name()+"_%SYS%", sys))
    ANA_CHECK (book (TH1F (histName.c_str(), histName.c_str(), nBins, 0, nBins)));
    auto histogram = hist(histName);
    histogram->GetXaxis()->SetBinLabel(1, "N_{tracks}");
    for (size_t i = 0; i < counter.nTracks; i++) {
      histogram->Fill(0);
    }
    histogram->GetXaxis()->SetBinLabel(2, "N_{accepted}");
    for (size_t i = 0; i < counter.nAccepted; i++) {
      histogram->Fill(1);
    }
  }

  return StatusCode::SUCCESS;
}