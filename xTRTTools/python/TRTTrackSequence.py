
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from AsgAnalysisAlgorithms.AnalysisObjectSharedSequence import makeSharedObjectSequence
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence

# Creates a track selection sequence that cuts on pT, eta and working point.
# There is a possibility to enable cut-flow histograms and kinematic plots.
# Creates a shallow copy container with the selection already applied.
def makeTRTTrackSelectionSequence(dataType, postfix='', maxEta=2.5, minPt=2000.,
    cutLevel='NoCut', minNTRTHits=-1, cutLevelDecorations=['Loose', 'LoosePrimary', 'TightPrimary'],
    enableCutflow=True, enableKinematicHistograms=False):
    postfix = '_'+postfix if postfix else ''

    seq = AnaAlgSequence("TRTTrackSelectionSequence"+postfix)
    seq.addMetaConfigDefault ("selectionDecorNames", [])
    seq.addMetaConfigDefault ("selectionDecorNamesOutput", [])
    seq.addMetaConfigDefault ("selectionDecorCount", [])

    # the track selection tool doing all the work
    alg = createAlgorithm('CP::AsgSelectionAlg', 'TrackSelectionAlg'+postfix)
    alg.selectionDecoration = 'trackSelection{},as_bits'.format(postfix)
    addPrivateTool(alg, 'selectionTool', 'InDet::InDetTrackSelectionTool')
    alg.selectionTool.maxAbsEta = maxEta
    alg.selectionTool.minPt = minPt
    alg.selectionTool.minNTrtHitsPlusOutliers = minNTRTHits
    alg.selectionTool.CutLevel = cutLevel
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                      'selectionDecorNamesOutput' : [alg.selectionDecoration],
                      'selectionDecorCount' : [10]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])})

    # more track selection tools to decorate WP names, not used for selection
    for cutLevel in cutLevelDecorations:
        alg = createAlgorithm('CP::AsgSelectionAlg', 'TrackSelectionDecorationAlg_'+cutLevel+postfix)
        addPrivateTool(alg, 'selectionTool', 'InDet::InDetTrackSelectionTool')
        alg.selectionDecoration = cutLevel+',as_char'
        alg.selectionTool.CutLevel = cutLevel
        seq.append( alg, inputPropName = 'particles',
            stageName = 'selection',
            dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])})

    makeSharedObjectSequence (seq, deepCopyOutput = False, shallowViewOutput = True, postfix = '_Tracks'+postfix,
            enableCutflow = enableCutflow, enableKinematicHistograms = enableKinematicHistograms)
    
    return seq