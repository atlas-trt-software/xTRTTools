/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/TrackFromVertexSelectionAlg.h>

#include <AsgMessaging/MessageCheck.h>

xTRT::TrackFromVertexSelectionAlg::TrackFromVertexSelectionAlg(const std::string& name, ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator) {
}

StatusCode xTRT::TrackFromVertexSelectionAlg::initialize() {

  // initialise all handles
  ANA_CHECK (m_verticesHandle.initialize(m_systematicsList));
  ANA_CHECK (m_tracksHandle.initialize(m_systematicsList));
  ANA_CHECK (m_selectionHandle.initialize(m_systematicsList, m_tracksHandle));
  ANA_CHECK (m_vertexChi2Decor.initialize(m_systematicsList, m_tracksHandle));
  ANA_CHECK (m_vertexDoFDecor.initialize(m_systematicsList, m_tracksHandle));
  ANA_CHECK (m_vertexP4Decor.initialize(m_systematicsList, m_tracksHandle));
  ANA_CHECK (m_vertexPositionDecor.initialize(m_systematicsList, m_tracksHandle));
  ANA_CHECK (m_systematicsList.initialize());

  return StatusCode::SUCCESS;
}

StatusCode xTRT::TrackFromVertexSelectionAlg::execute() {

  for (const auto& sys : m_systematicsList.systematicsVector()) {
    // keep track of the number of selected objects
    auto& counter = m_counters[sys];

    const xAOD::VertexContainer* vertices = nullptr;
    ANA_CHECK (m_verticesHandle.retrieve(vertices, sys));

    const xAOD::TrackParticleContainer* tracks = nullptr;
    ANA_CHECK (m_tracksHandle.retrieve(tracks, sys));

    std::map<const xAOD::TrackParticle*, const xAOD::Vertex*> trackToVertexMap;
    std::map<const xAOD::Vertex*, TLorentzVector> vertexP4Map;

    // loop over all vertices and store linked tracks
    for (const auto& vertex : *vertices) {
      xAOD::IParticle::FourMom_t p4;
      xAOD::IParticle::FourMom_t trackP4;
      // only consider vertices with at least two tracks
      if (vertex->nTrackParticles() < 2) continue;
      for (size_t nTrack = 0; nTrack < vertex->nTrackParticles(); nTrack++) {
        const xAOD::TrackParticle* vtxTrack = vertex->trackParticle(nTrack);
        if (vtxTrack != nullptr) {
          trackP4 = vtxTrack->p4();
          int particleHypothesis = vtxTrack->particleHypothesis();
          if (particleHypothesis == 1) {
            // electron
            trackP4.SetVectM(trackP4.Vect(), 0.51099891);
          } else if (particleHypothesis == 2) {
            // muon
            trackP4.SetVectM(trackP4.Vect(), 105.658367);
          } else if (particleHypothesis == 4) {
            // kaon
            trackP4.SetVectM(trackP4.Vect(), 493.677);
          } // else leave at default, i.e. pion
          p4 += trackP4;
          trackToVertexMap[vtxTrack] = vertex;
        }
      }
      vertexP4Map[vertex] = p4;
      counter.nVertices++;
    }

    // loop over all tracks and store decorations
    for (const auto& track : *tracks) {
      // default values
      TVector3 position(0.,0.,0.);
      TLorentzVector p4(0.,0.,0.,0.);
      float chi2 = 0.;
      unsigned dof = 0;
      bool foundVertex = false;
      counter.nTracks++;
      // check whether track was linked to a vertex
      try {
        const xAOD::Vertex* vertex = trackToVertexMap.at(track);
        p4 = vertexP4Map.at(vertex);
        position.SetXYZ(vertex->x(), vertex->y(), vertex->z());
        chi2 = vertex->chiSquared();
        dof = vertex->numberDoF();
        foundVertex = true;
        counter.nTracksSelected++;
      } catch (const std::out_of_range& e) {
        // nothing to do
      }
      m_vertexChi2Decor.set(*track, chi2, sys);
      m_vertexDoFDecor.set(*track, dof, sys);
      m_vertexP4Decor.set(*track, p4, sys);
      m_vertexPositionDecor.set(*track, position, sys);
      m_selectionHandle.setBool(*track, foundVertex, sys);
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode xTRT::TrackFromVertexSelectionAlg::finalize() {
  // number of bins for cutflow histograms
  size_t nBins = 3;

  // fill the cutflow histograms
  for (const auto& [sys, counter] : m_counters) {
    std::string histName;
    ANA_CHECK(m_systematicsList.service().makeSystematicsName(histName, "cflow_"+this->name()+"_%SYS%", sys))
    ANA_CHECK (book (TH1F (histName.c_str(), histName.c_str(), nBins, 0, nBins)));
    auto histogram = hist(histName);
    histogram->GetXaxis()->SetBinLabel(1, "N Vertices");
    for (size_t i = 0; i < counter.nVertices; i++) {
      histogram->Fill(0);
    }
    histogram->GetXaxis()->SetBinLabel(2, "N Tracks");
    for (size_t i = 0; i < counter.nTracks; i++) {
      histogram->Fill(1);
    }
    histogram->GetXaxis()->SetBinLabel(3, "N Selected Tracks");
    for (size_t i = 0; i < counter.nTracksSelected; i++) {
      histogram->Fill(2, 1);
    }
  }
  return StatusCode::SUCCESS;
}