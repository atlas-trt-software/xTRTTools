from AthenaCommon.AppMgr import theApp
from xTRTTools.TRTTruthParticleSequence import makeTRTTruthParticleSelectionSequence
from xTRTTools.TRTMuonSequence import makeTRTMuonSelectionSequence
from xTRTTools.TRTElectronSequence import makeTRTElectronSelectionSequence
from xTRTTools.TRTTrackSequence import makeTRTTrackSelectionSequence

from AnaAlgorithm.DualUseConfig import createAlgorithm, createService
from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence

##
# These job options create TRT n-tuples with track information. Each entry corresponds to a track that passed some pre-selection.
# If they exist, corresponding truth particles (only for MC), electron and muon objects will be stored as well.
# For MC, truth particles that pass some preselection are stored even if no corresponding track exists to allow tracking efficiency studies.
# In case of data, any single electron or single muon trigger is required to process the event.
##

############################################################################################
# Define the containers and the pre-selection for the objects to be stored in the n-tuples #
############################################################################################

# output file information
outputFileName = 'xTRTNTuple.root'
outputTreeName = 'TRT'

# the event info container to be used (only needed to extract trigger information)
eventInfoName = 'EventInfo'

# define the truth particle container and the selection based on pT and eta
truthParticleContainer = 'TruthParticles'
truthMinPt = 1.e3                # in MeV
truthMaxEta = 2.5                # tracker acceptance
truthStatusCodes = [1]           # only store stable truth particles
truthPdgIds = [11,13,15,211,321] # only store particle with these PDG IDs: electrons, muons, pions, kaons
truthParentPdgIds = []           # store only truth particles with these parent PDG IDs (disabled)

# define the list of track containers to store and their respective pre-selection
# see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsRun2R22#Track_Selection
trackContainers = ['InDetTrackParticles', 'GSFTrackParticles']
trackCutLevelDecorations = ['Loose', 'LoosePrimary', 'TightPrimary']
trackCutLevel = 'Loose'
trackMinPt = 2.e3    # in MeV
trackMaxEta = 2.5    # tracker acceptance
trackMinTRTHits = -1 # any value < 0 means that no TRT hits are required

# define the electron container to be stored and its pre-selection
electronContainer = 'Electrons'     # set to '' to ignore electrons
electronIdentification = 'LHLoose'  # Loose, Medium, Tight, LHLoose, LHMedium, LHTight
electronIsolation = 'NonIso'        # electron isolation variables missing in TRTxAOD
electronMinPt = 4.5e3               # in MeV

# define the muon container to be stored and its pre-selection
muonContainer = 'Muons'   # set to '' to ignore muons
muonIdentification = ''   # for some reason most muons are tagged as calo-muons and will fail the selection algorithm - expected are combined muons
muonIsolation = 'NonIso'  # muon isolation variables missing in TRTxAOD
muonMinPt = 3e3           # in MeV

# definition of the trigger requirements to be applied for data (OR between all listed trigger items)
triggerListElectron = ['HLT_e24_lhmedium_L1EM20VH', 'HLT_e60_lhmedium', 'HLT_e120_lhloose', 'HLT_e24_lhtight_nod0_ivarloose',
                       'HLT_e26_lhtight_nod0_ivarloose', 'HLT_e26_lhtight_nod0', 'HLT_e60_lhmedium_nod0', 'HLT_e140_lhloose_nod0', 'HLT_e300_etcut']
triggerListMuon = ['HLT_mu20_iloose_L1MU15', 'HLT_mu40', 'HLT_mu60_0eta105_msonly', 'HLT_mu24_iloose',
                   'HLT_mu24_ivarloose', 'HLT_mu40', 'HLT_mu50', 'HLT_mu24_ivarmedium', 'HLT_mu26_ivarmedium']



########################################################
# Changes below here should typically not be necessary #
########################################################

# define the input file, override from the command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = []

# Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
#jps.AthenaCommonFlags.AccessMode = "ClassAccess"
# POOL access should be more robust across releases
jps.AthenaCommonFlags.AccessMode = "POOLAccess"

# define the output file name
jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:"+outputFileName]
svcMgr.THistSvc.MaxFileSize = -1 #speeds up jobs that output lots of histograms

# read the metadata from the first input file
dataType = 'data'
if len(jps.AthenaCommonFlags.FilesInput()) > 0:
    from PyUtils import AthFile
    af = AthFile.fopen(jps.AthenaCommonFlags.FilesInput()[0]) # opens the first file from the FilesInput list

    # extract the datatype from the metadata of the first file to process
    if 'IS_SIMULATION' in af.fileinfos['evt_type']:
        if 'ATLFASTII' in af.fileinfos['metadata']['/Simulation/Parameters']['SimulationFlavour'].upper():
            dataType = 'afii'
        else:
            dataType = 'mc'

# Set up the systematics loader/handler service:
sysService = createService( 'CP::SystematicsSvc', 'SystematicsSvc', sequence = athAlgSeq )
sysService.sigmaRecommended = 1



####################################
# Sequence of algorithms to be run #
####################################

# input files needed for pile-up reweighting - these files need to be updated depending on dataset to be processed
prwfiles = []
lumicalcfiles = []
if dataType in ['mc', 'afii']:
    lumicalcfiles = [
        "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
        "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
        "GoodRunsLists/data17_13TeV/20190708/ilumicalc_histograms_None_325713-340453_OflLumi-13TeV-010.root",
        "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",
    ]
    prwfiles = ["dev/PileupReweighting/mc16_13TeV/pileup_mc16a_dsid410501_FS.root"]

# disable pile-up reweighting for TRT studies
# pileupSequence = makePileupAnalysisSequence(dataType, userPileupConfigs=prwfiles, userLumicalcFiles=lumicalcfiles)
# pileupSequence.configure(inputName = eventInfoName, outputName = eventInfoName+'_%SYS%')
# athAlgSeq += pileupSequence

# apply the trigger selection
if dataType == 'data':
    alg = createAlgorithm('TriggerSelectionAlg', 'TriggerSelectionAlg')
    alg.EventInfoName = eventInfoName
    alg.TriggerList = triggerListElectron + triggerListMuon
    athAlgSeq += alg

# preselection for truth particles to store, stores a cutflow histogram
selectedTruthParticleContainer = ''
if dataType in [ 'mc', 'afii' ]:
    selectedTruthParticleContainer = truthParticleContainer+'_selected'
    seq = makeTRTTruthParticleSelectionSequence(dataType, maxEta=truthMaxEta, minPt=truthMinPt,
                                                statusCodes=truthStatusCodes, pdgIds=truthPdgIds, parentPdgIds=truthParentPdgIds,
                                                enableCutflow=True, enableKinematicHistograms=False)
    seq.configure(inputName = truthParticleContainer, outputName = selectedTruthParticleContainer)
    athAlgSeq += seq

# preselection for electrons to store, stores a cutflow histogram
selectedElectrons = ''
if electronContainer:
    selectedElectrons = electronContainer+'_selected'
    seq = makeTRTElectronSelectionSequence(dataType, electronMinPt, electronIdentification, electronIsolation,
                                           postfix = '', enableCutflow=True, enableKinematicHistograms=False)
    seq.configure(inputName = electronContainer, outputName = selectedElectrons)
    athAlgSeq += seq

# preselection for muons to store, stores a cutflow histogram
selectedMuons = ''
if muonContainer:
    selectedMuons = muonContainer + '_selected'
    seq = makeTRTMuonSelectionSequence(dataType, muonMinPt, muonIdentification, electronIsolation,
                                       postfix = '', enableCutflow=True, enableKinematicHistograms=False)
    seq.configure(inputName = muonContainer, outputName = selectedMuons)
    athAlgSeq += seq

# preselection for tracks to store - allow for multiple track containers
selectedTrackContainers = []
for trackContainer in trackContainers:
    seq = makeTRTTrackSelectionSequence(dataType, postfix=trackContainer,
                                        maxEta=trackMaxEta, minPt=trackMinPt, cutLevel=trackCutLevel,
                                        minNTRTHits=trackMinTRTHits, cutLevelDecorations=trackCutLevelDecorations,
                                        enableCutflow=True, enableKinematicHistograms=False,)
    selectedTrackContainers.append(trackContainer+'_selected')
    seq.configure(inputName = trackContainer, outputName = trackContainer+'_selected')
    athAlgSeq += seq

# set up the algorithm to fill the ROOT tree
trackWriter = createAlgorithm( 'xTRT::TRTTrackWriterAlg', 'TrackWriter' )
trackWriter.isMC = dataType in [ 'mc', 'afii' ]
trackWriter.treeName = outputTreeName
trackWriter.truthContainerName = selectedTruthParticleContainer
trackWriter.electronContainerName = selectedElectrons
trackWriter.muonContainerName = selectedMuons
trackWriter.trackContainerNames = selectedTrackContainers
trackWriter.trackPrefixes = [container+'_' for container in trackContainers]
# no need to store GSF tracks through the electron objects, since the GSF tracks are stored directly
trackWriter.storeElectronGsfTracks = False
# store only those electron and muon objects that have a corresponding selected track object
trackWriter.storeOnlyObjectsMatchedToTracks = True
# add the track selection cut information to the list of output variables
for decoration in trackCutLevelDecorations:
    trackWriter.trackVariables.append("<char> {} -> <bool> {}".format(decoration, decoration))
athAlgSeq += trackWriter

#############################
# End of algorithm sequence #
#############################



MessageSvc.defaultLimit=100000
# limit the number of events (for testing purposes)
#from AthenaCommon.AppMgr import ServiceMgr
#ServiceMgr.EventSelector.SkipEvents = 10
theApp.EvtMax = -1

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")