
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from AsgAnalysisAlgorithms.AnalysisObjectSharedSequence import makeSharedObjectSequence
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
import ROOT

# Creates an electron selection sequence that cuts on pT, eta, impact parameter, electron ID and isolation.
# The cuts on impact parameter and isolation are not working properly in TRTxAODs.
# There is a possibility to enable cut-flow histograms and kinematic plots.
# Creates a shallow copy container with the selection already applied.
def makeTRTElectronSelectionSequence(dataType, minPt=4.5e3, identificationWP='', isolationWP='NonIso', postfix='', enableCutflow=True, enableKinematicHistograms=False):
    postfix = '_'+postfix if postfix else ''

    seq = AnaAlgSequence( "TRTElectronSelectionSequence"+postfix )
    seq.addMetaConfigDefault ("selectionDecorNames", [])
    seq.addMetaConfigDefault ("selectionDecorNamesOutput", [])
    seq.addMetaConfigDefault ("selectionDecorCount", [])

    # kinematic cuts
    alg = createAlgorithm( 'CP::AsgSelectionAlg', 'ElectronPtEtaCutAlg'+postfix )
    alg.selectionDecoration = 'selectPtEta{},as_bits'.format(postfix)
    addPrivateTool( alg, 'selectionTool', 'CP::AsgPtEtaSelectionTool' )
    alg.selectionTool.maxEta = 2.1
    #alg.selectionTool.etaGapLow = 1.37
    #alg.selectionTool.etaGapHigh = 1.52
    alg.selectionTool.minPt = minPt
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                        'selectionDecorNamesOutput' : [alg.selectionDecoration],
                        'selectionDecorCount' : [5]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])} )

    # impact parameter cuts (also requires existance of track link)
    alg = createAlgorithm( 'CP::AsgLeptonTrackSelectionAlg', 'ElectronTrackSelectionAlg'+postfix )
    alg.selectionDecoration = 'trackSelection{},as_bits'.format(postfix)
    ## for some reason the IP cuts will kill most electrons in TRTxAODS - wrong primary vertex?
    alg.maxD0Significance = 5
    alg.maxDeltaZ0SinTheta = 0.5
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                        'selectionDecorNamesOutput' : [alg.selectionDecoration],
                        'selectionDecorCount' : [3]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])} )

    # filter bad quality electrons
    alg = createAlgorithm( 'CP::AsgSelectionAlg', 'ElectronObjectQualityAlg'+postfix )
    alg.selectionDecoration = 'goodOQ{},as_bits'.format(postfix)
    addPrivateTool( alg, 'selectionTool', 'CP::EgammaIsGoodOQSelectionTool' )
    alg.selectionTool.Mask = ROOT.xAOD.EgammaParameters.BADCLUSELECTRON
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                        'selectionDecorNamesOutput' : [alg.selectionDecoration],
                        'selectionDecorCount' : [1]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])} )

    # electron identification using the existing WP decorations
    alg = createAlgorithm( 'CP::AsgSelectionAlg', 'ElectronLikelihoodAlg'+postfix )
    alg.selectionDecoration = 'selectLikelihood{},as_bits'.format(postfix)
    addPrivateTool( alg, 'selectionTool', 'CP::AsgFlagSelectionTool' )
    alg.selectionTool.selectionFlags = [identificationWP]
    seq.append( alg, inputPropName = 'particles',
        stageName = 'selection',
        metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                        'selectionDecorNamesOutput' : [alg.selectionDecoration],
                        'selectionDecorCount' : [1]},
        dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])} )

    # electron isolation - the input variables for recommended isolation WPs are missing in TRTxAODs
    if isolationWP != 'NonIso' :
        alg = createAlgorithm( 'CP::EgammaIsolationSelectionAlg', 'ElectronIsolationSelectionAlg'+postfix )
        alg.selectionDecoration = 'isolated{},as_bits'.format(postfix)
        addPrivateTool( alg, 'selectionTool', 'CP::IsolationSelectionTool' )
        alg.selectionTool.ElectronWP = isolationWP
        seq.append( alg, inputPropName = 'egammas',
            stageName = 'selection',
            metaConfig = {'selectionDecorNames' : [alg.selectionDecoration],
                            'selectionDecorNamesOutput' : [alg.selectionDecoration],
                            'selectionDecorCount' : [1]},
            dynConfig = {'preselection' : lambda meta : "&&".join (meta["selectionDecorNamesOutput"])} )

    makeSharedObjectSequence (seq, deepCopyOutput = False, shallowViewOutput = True, postfix = '_TRTElectron'+postfix,
        enableCutflow = enableCutflow, enableKinematicHistograms = enableKinematicHistograms )
    
    return seq