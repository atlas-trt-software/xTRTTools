/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef xTRTTools_TRTTrackWriterAlg_H
#define xTRTTools_TRTTrackWriterAlg_H

#include <xTRTTools/TreeFiller.h>
#include <xTRTTools/TrackWriterTool.h>

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/VertexContainer.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>

// C++ includes
#include <map>
#include <memory>
#include <vector>

namespace xTRT {
  /**
  * @brief Algorithm to fill TRT track- and hit-level quantities into a ROOT tree.
  * 
  * This algorithm allows to create custom TRT n-tuples including track- and hit-level
  * quantities, as well as truth particle, electron and muon related quantities.
  * 
  * The user can provide a list of TrackParticle containers, and optionally also a
  * TruthParticle, an Electron and a Muon container. The resulting ROOT tree will create
  * branches for all provided objects. Each entry of the tree correspons to a particle
  * or a track, i.e. event level quantities like pile-up are duplicated for each track entry.
  * The event context can still be retrieved from the n-tuple using the event number and run
  * number information. Hit-level quantities are stored in vectors that are associated to the
  * corresponding track quantities.
  * 
  * The entries for the different objects are added using the following matching logic:
  * TrackParticles are matched to their corresponding TruthParticle (see https://indico.cern.ch/event/795039/contributions/3391771/attachments/1857138/3050771/TruthTrackFTAGWS.pdf).
  * Electrons and Muons are first matched to their corresponding TrackParticle and if that does
  * not exist, they are matched to their corresponding truth particle. If no match can be found
  * the corresponding entries will be filled with dummy values (typically zeros).
  * 
  * To summarise: All objects in the provided input containers will be added to the tree,
  * i.e. the object selection should happen before passing them to this algorithm.
  * Each object will add a new entry to the tree unless it is matched to one or multiple
  * other objects, in which case they are all added as a single entry.
  * 
  * The list of variables stored for each object can be configured in the job options, as long
  * as the variables are available as member-, auxiliary or decorated variables on the given
  * objects. Other variables need to be explicitely implemented in the TreeFiller class.
  * 
  */
  class TRTTrackWriterAlg : public EL::AnaAlgorithm {
  public:
    // this is a standard algorithm constructor
    TRTTrackWriterAlg(const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize() override;
    virtual StatusCode execute()    override;
    virtual StatusCode finalize()   override;
  private:
    virtual StatusCode setupTree();

    // Configuration parameters
    std::string m_treeName;
    CP::SysListHandle m_systematicsList {this};
    CP::SysReadHandle<xAOD::EventInfo> m_eventInfoHandle {this, "eventInfo", "EventInfo", "Event info"};
    CP::SysReadHandle<xAOD::TruthParticleContainer> m_truthParticleHandle {this, "truthParticles", "TruthParticles", "Truth Particles"};
    CP::SysReadHandle<xAOD::VertexContainer> m_primaryVerticesHandle {this, "primaryVertices", "PrimaryVertices", "Primary vertex container."};
    CP::SysReadHandle<xAOD::MuonContainer> m_muonHandle {this, "muons", "Muons", "Muon container."};
    CP::SysReadHandle<xAOD::ElectronContainer> m_electronHandle {this, "electrons", "Electrons", "Electron container."};
    ToolHandleArray<TrackWriterTool> m_trackWriterTools;
    std::map<std::string, CP::SysReadHandle<xAOD::TrackParticleContainer>*> m_containerNameToTrackContainer;

    bool m_isMC;
    bool m_storeOnlyObjectsMatchedToTracks;
    
    std::string m_primaryVertexPrefix;
    std::string m_truthPrefix;
    std::string m_muonPrefix;
    std::string m_electronPrefix;

    std::vector<std::string> m_eventInfoVariables;
    std::vector<std::string> m_electronVariables;
    std::vector<std::string> m_muonVariables;
    
    std::unique_ptr<xTRT::EventInfoFiller> m_eventInfoFiller;
    std::unique_ptr<xTRT::VertexFiller> m_primaryVertexFiller;
    std::unique_ptr<xTRT::TruthParticleFiller> m_truthFiller;
    std::unique_ptr<xTRT::ElectronFiller> m_electronFiller;
    std::unique_ptr<xTRT::MuonFiller> m_muonFiller;
    std::map<std::string, TrackWriterTool* > m_containerNameToTrackWriterTool;
    std::vector<std::string> m_trackContainerNames;

    TTree* m_tree {nullptr};

    struct Counter {
      size_t nEvents = 0;
      size_t nTruthParticles = 0;
      size_t nTracks = 0;
      size_t nElectrons = 0;
      size_t nMuons = 0;
    };

    std::map<CP::SystematicSet, xTRT::TRTTrackWriterAlg::Counter> m_counters;
  };
}

#endif
