/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef xTRTTools_TagAndProbeAlg_H
#define xTRTTools_TagAndProbeAlg_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgServices/ServiceHandle.h>
#include <SystematicsHandles/ISystematicsSvc.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SelectionHelpers/SysWriteSelectionHandle.h>
#include <SystematicsHandles/SysWriteDecorHandle.h>

#include <xAODTracking/TrackParticleContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>

// C++ includes
#include <string>
#include <map>

namespace xTRT {
  class TagAndProbeAlg : public EL::AnaAlgorithm {
  public:
    // this is a standard algorithm constructor
    TagAndProbeAlg(const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize() override;
    virtual StatusCode execute()    override;
    virtual StatusCode finalize()   override;
  private:
    ServiceHandle<CP::ISystematicsSvc> m_systematics {"SystematicsSvc", ""};
    CP::SysListHandle m_systematicsList {this};
    CP::SysReadHandle<xAOD::IParticleContainer> m_tagHandle {this, "tags", "Muons_%SYS%", "Name of container with tag objects. Can be of type electron, muon or trackParticle"};
    CP::SysReadHandle<xAOD::IParticleContainer> m_probeHandle {this, "probes", "InDetTrackParticles_%SYS%", "Name of container with probe objects. Can be of type electron, muon or trackParticle"};
    
    CP::SysWriteSelectionHandle m_selectionHandle {this, "selectionDecoration", "tnp_selected,as_char", "the decoration for the tnp selection, needs to be stored as char."};
    CP::SysWriteDecorHandle<TLorentzVector> m_p4Decor {this, "p4Decoration", "tnp_p4", "Name of the four vector of the tnp selected object."};
    CP::SysWriteDecorHandle<float> m_deltaZ0Decor {this, "deltaZ0Decoration", "tnp_deltaZ0", "Name of the distance in z between the two points of closest approach."};
    CP::SysWriteDecorHandle<char> m_qxqDecor {this, "qxqDecoration", "tnp_qxq", "Name of the charge product of the tag and probe objects."};

    float m_massWindowLow;
    float m_massWindowHigh;
    float m_targetMass;
    float m_maxDeltaZ0;
    bool m_oppositeCharge;

    struct Counter {
      size_t nTags = 0;
      size_t nProbes = 0;
      size_t nOppositeCharge = 0;
      size_t nMassWindow = 0;
      size_t nMaxDeltaZ0 = 0;
    };

    std::map<CP::SystematicSet, xTRT::TagAndProbeAlg::Counter> m_counters;

    // helper method to retrieve a track from TrackParticle, Electron or Muon objects
    static const xAOD::TrackParticle* getTrack(const xAOD::IParticle* particle);
  };
}

#endif
