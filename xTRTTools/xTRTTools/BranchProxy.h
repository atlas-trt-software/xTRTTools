/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef xTRTTools_BranchProxy_H
#define xTRTTools_BranchProxy_H

#include <AthContainers/AuxElement.h>

#include <TTree.h>

#include <vector>
#include <string>

namespace xTRT {

  // abstract base class for BranchProxy
  class IBranchProxy {
    public:
      virtual void fill(const SG::AuxElement* element) = 0;
      virtual void reset() = 0;
  };

  class BranchProxyFactory {
    public:
      // factory method to create concrete BranchProxy objects depending on the expression.
      static IBranchProxy* setupBranch(TTree* tree, const std::string& expression, const std::string& prefix="");

      // factory method to create concrete VectorBranchProxy objects depending on the expression.
      static IBranchProxy* setupVectorBranch(TTree* tree, const std::string& expression, const std::string& prefix="");
    
    private:
      static void analyseExpression(const std::string& expr, std::string& accessorName,
          std::string& branchName, std::string& accessorType, std::string& branchType);
  };
  
  // class to link an accessor to some xAOD variable with a branch of a TTree
  template <typename A, typename B = A>
  class BranchProxy : public IBranchProxy {
  public:
    BranchProxy(TTree* tree, const std::string& accessorName, const std::string& branchName="");
    virtual void fill(const SG::AuxElement* element) override;
    virtual void reset() override;
    virtual void setValue(const B& value);
    virtual const B& value() const;

  private:
    std::string m_branchName;
    std::string m_accessorName;
    B m_value;
    const SG::AuxElement::ConstAccessor<A> m_accessor;
    bool m_printError = true;
    // helper method to retrieve the shorthand needed to define the type of the Branch
    static std::string branchType();
  };


  // class to link an accessor to some xAOD variable with a branch of a TTree
  template <typename A, typename B = A>
  class VectorBranchProxy : public IBranchProxy {
  public:
    VectorBranchProxy(TTree* tree, const std::string& accessorName, const std::string& branchName="");
    ~VectorBranchProxy();
    virtual void fill(const SG::AuxElement* element) override;
    virtual void reset() override;
    virtual void setValue(const std::vector<B>& value);
    virtual const std::vector<B>& value() const;

  private:
    std::string m_branchName;
    std::string m_accessorName;
    std::vector<B>* m_vector;
    const SG::AuxElement::ConstAccessor<A> m_accessor;
    bool m_printError = true;
  };
}

#endif