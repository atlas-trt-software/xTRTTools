/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/TrackWriterTool.h>

namespace xTRT {

  TrackWriterTool::TrackWriterTool (const std::string& name)
      : AsgTool (name) {
    declareProperty ("container", m_trackContainerName = "InDetTrackParticles", "name of track container to store");
    declareProperty ("prefix", m_prefix, "prefix used for all branches");
    declareProperty ("storeHist", m_storeHits = true , "decide whether hit-level variables are stored");
    declareProperty ("isMC", m_isMC = true, "Decide whether the sample is simulated or real data");
    declareProperty ("trackVariables", m_trackVariables = TrackFiller::defaultVariables, "list of track-level variables to store");
    declareProperty ("measurementVariables", m_measurementVariables = TrackFiller::defaultMeasurementVariables, "list of hit-level variables to store (from mesurement on track obejcts)");
    declareProperty ("driftCircleVariables", m_driftCircleVariables = TrackFiller::defaultDriftCircleVariables, "list of hit-level variables to store (from DriftCircle objects)");
  }

  StatusCode TrackWriterTool::initialize () {
    if (m_isMC) {
      m_trackVariables.push_back("<float> truthMatchProbability");
    }
    return StatusCode::SUCCESS;
  }

  bool TrackWriterTool::fill(const xAOD::IParticle* particle, const xAOD::EventInfo* eventInfo, const xAOD::Vertex* vertex) {
    m_trackFiller->reset();
    const xAOD::TrackParticle* track = dynamic_cast<const xAOD::TrackParticle*>(particle);
    if (track != nullptr) {
      ATH_MSG_DEBUG("Filling particle with pT = " << track->p4().Pt() << " MeV.");
      m_trackFiller->setEventInfo(eventInfo);
      m_trackFiller->setPrimaryVertex(vertex);
      m_trackFiller->fill(track);
      return true;
    }
    return false;
  }

  StatusCode TrackWriterTool::setupTree(TTree* tree) {
    ATH_MSG_DEBUG("Setting up ROOT tree");
    m_tree = tree;
    if (m_tree == nullptr) {
      ATH_MSG_ERROR("No ROOT tree has been set");
      return StatusCode::FAILURE;
    }
    m_trackFiller = std::make_unique<xTRT::TrackFiller>(m_prefix, m_tree, m_trackVariables, m_measurementVariables, m_driftCircleVariables, m_storeHits, m_isMC);
    return StatusCode::SUCCESS;
  }

}
