/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#ifndef xTRTTools_TreeFiller_H
#define xTRTTools_TreeFiller_H

#include <xTRTTools/BranchProxy.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/TrackParticle.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODEgamma/Electron.h>
#include <xAODMuon/Muon.h>
#include <AthContainers/AuxElement.h>

#include <TTree.h>

#include <vector>
#include <string>
#include <memory>

namespace xTRT {

  class TreeFiller {
  public:
    // default constructor
    TreeFiller(const std::string& prefix, TTree* tree,
        const std::vector<std::string>& variables=std::vector<std::string>(),
        bool useVectorBranches=false, bool isMC=false);
    // Fill all variables from the given object
    virtual bool fill(const SG::AuxElement* element);
    // Set all variables to their default value
    virtual void reset();
  protected:
    bool m_isMC;
  private:
    std::vector<std::unique_ptr<IBranchProxy>> m_variables;
  };

  class EventInfoFiller : public TreeFiller {
  public:
    // default constructor
    EventInfoFiller(const std::string& prefix, TTree* tree,
        const std::vector<std::string>& variables=defaultVariables, bool isMC=false);
    ~EventInfoFiller();
    // Fill all variables from the given object
    virtual bool fill(const xAOD::EventInfo* eventInfo);
    // Set all variables to their default value
    virtual void reset();
    // List of default variables to fill
    static const std::vector<std::string> defaultVariables;
  private:
    // hide the inherited fill function
    using TreeFiller::fill;
    float m_eventWeight;
    TVector3* m_beamSpot;
  };
  
  class VertexFiller : public TreeFiller {
  public:
    // default constructor
    VertexFiller(const std::string& prefix, TTree* tree,
        const std::vector<std::string>& variables=defaultVariables, bool isMC=false);
    ~VertexFiller();
    // Fill all variables from the given object
    virtual bool fill(const xAOD::Vertex* vertex);
    // Set all variables to their default value
    virtual void reset();
    // List of default variables to fill
    static const std::vector<std::string> defaultVariables;
  private:
    // hide the inherited fill function
    using TreeFiller::fill;
    TVector3* m_position;
  };

  class ParticleFiller : public TreeFiller {
    public:
    // default constructor
    ParticleFiller(const std::string& prefix, TTree* tree,
        const std::vector<std::string>& variables=defaultVariables, bool isMC=false);
    ~ParticleFiller();
    // Fill all variables from the given object
    virtual bool fill(const xAOD::IParticle* particle);
    // Set all variables to their default value
    virtual void reset();
    // List of default variables to fill
    static const std::vector<std::string> defaultVariables;
  protected:
    // hide the inherited fill function
    using TreeFiller::fill;
    TLorentzVector* m_p4;
  };

  class TruthParticleFiller : public ParticleFiller {
  public:
    TruthParticleFiller(const std::string& prefix, TTree* tree,
        const std::vector<std::string>& variables=defaultVariables, bool isMC=true);
    ~TruthParticleFiller();
    // Fill all variables from the given object
    virtual bool fill(const xAOD::TruthParticle* particle);
    // Set all variables to their default value
    virtual void reset();
    // List of default variables to fill
    static const std::vector<std::string> defaultVariables;
  private:
    // hide the inherited fill function
    using ParticleFiller::fill;
    TVector3* m_prodVertex;
    int m_parentPdgId;
    float m_charge;
  };

  class TrackFiller : public ParticleFiller {
  public:
    TrackFiller(const std::string& prefix, TTree* tree,
        const std::vector<std::string>& trackVariables=defaultVariables,
        const std::vector<std::string>& measurementVariables=defaultMeasurementVariables,
        const std::vector<std::string>& driftCircleVariables=defaultDriftCircleVariables,
        bool storeHits = true, bool isMC=false);
    ~TrackFiller();

    // Fill all variables from the given object
    virtual bool fill(const xAOD::TrackParticle* track);
    // Set all variables to their default value
    virtual void reset();
    // Set the event info
    void setEventInfo(const xAOD::EventInfo* eventInfo) {
      m_eventInfo = eventInfo;
    }
    // Set the primary vertex
    void setPrimaryVertex(const xAOD::Vertex* primaryVertex) {
      m_primaryVertex = primaryVertex;
    }
    // List of default variables to fill
    static const std::vector<std::string> defaultVariables;
    static const std::vector<std::string> defaultMeasurementVariables;
    static const std::vector<std::string> defaultDriftCircleVariables;
  private:
    // hide the inherited fill function
    using ParticleFiller::fill;
    bool m_storeHits;
    const xAOD::EventInfo* m_eventInfo {nullptr};
    const xAOD::Vertex* m_primaryVertex {nullptr};

    std::unique_ptr<TreeFiller> m_measurementFiller;
    std::unique_ptr<TreeFiller> m_driftCircleFiller;

    float m_charge;
    float m_d0sig;
    float m_z0sig;
    float m_z0sinTheta;
    float m_sumL;
    unsigned char m_nTRTHits;
    unsigned char m_nTRTOutliers;
    unsigned char m_nHitsPrecision;
    unsigned char m_nHitsTube;
    std::vector<bool>* m_hit_isHighThresholdMiddleBit;
    std::vector<char>* m_hit_type;
    std::vector<TVector3>* m_hit_position;
    std::vector<float>* m_hit_l;
    std::vector<float>* m_hit_driftTimeCorrected;
  };

  class ElectronFiller : public ParticleFiller {
  public:
    ElectronFiller(const std::string& prefix, TTree* tree, const std::vector<std::string>& variables=defaultVariables, bool isMC=false);

    // Copy content of an electron into local variables
    virtual bool fill(const xAOD::Electron* electron);
    // Reset all values to default and clear all vectors
    virtual void reset();
    // List of default variables to fill
    static const std::vector<std::string> defaultVariables;

  private:
    // hide the inherited fill function
    using ParticleFiller::fill;
  };

  class MuonFiller : public ParticleFiller {
  public:
    MuonFiller(const std::string& prefix, TTree* tree, const std::vector<std::string>& variables=defaultVariables,
      bool isMC=false);
    // Fill all variables from the given object
    virtual bool fill(const xAOD::Muon* muon);
    // Set all variables to their default value
    virtual void reset();
    // List of default variables to fill
    static const std::vector<std::string> defaultVariables;
  private:  
    // hide the inherited fill function
    using ParticleFiller::fill;
  };

}

#endif