/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Christian Grefe

#include <xTRTTools/TrackToTrackMatchAlg.h>

#include <xAODBase/IParticleHelpers.h>
#include <AsgMessaging/MessageCheck.h>

xTRT::TrackToTrackMatchAlg::TrackToTrackMatchAlg(const std::string& name, ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator) {

  declareProperty ("trackAccessor", m_trackAccessorDecoration, "name of the element link on the tracks");
  declareProperty ("trackToMatchAccessor", m_trackToMatchAccessorDecoration = "originalTrackParticle", "name of the element link on the tracks to match");
}

StatusCode xTRT::TrackToTrackMatchAlg::initialize() {

  // initialise all handles
  ANA_CHECK (m_tracksToMatchHandle.initialize(m_systematicsList));
  ANA_CHECK (m_tracksHandle.initialize(m_systematicsList));
  ANA_CHECK (m_selectionHandle.initialize(m_systematicsList, m_tracksHandle));
  ANA_CHECK (m_preselectionHandle.initialize(m_systematicsList, m_tracksToMatchHandle, SG::AllowEmpty));
  ANA_CHECK (m_systematicsList.initialize());

  if (!m_trackAccessorDecoration.empty()) {
    m_trackAccessor = std::make_unique<const SG::AuxElement::Accessor<ElementLink<xAOD::TrackParticleContainer>>>(m_trackAccessorDecoration);
  }

  if (!m_trackToMatchAccessorDecoration.empty()) {
    m_trackToMatchAccessor = std::make_unique<const SG::AuxElement::Accessor<ElementLink<xAOD::TrackParticleContainer>>>(m_trackToMatchAccessorDecoration);
  }

  return StatusCode::SUCCESS;
}

StatusCode xTRT::TrackToTrackMatchAlg::execute() {
  for (const auto& sys : m_systematicsList.systematicsVector()) {
    auto& counter = m_counters[sys];

    const xAOD::TrackParticleContainer* tracks = nullptr;
    ANA_CHECK (m_tracksHandle.retrieve(tracks, sys));

    const xAOD::TrackParticleContainer* tracksToMatch = nullptr;
    ANA_CHECK (m_tracksToMatchHandle.retrieve(tracksToMatch, sys));

    std::map<const xAOD::TrackParticle*, const xAOD::TrackParticle*> tracksToMatchedTracks;

    for (const auto& trackToMatch : *tracksToMatch) {
      counter.nTracksToMatch++;
      if (not m_preselectionHandle.getBool(*trackToMatch, sys)) continue;
      if (m_trackToMatchAccessor && m_trackToMatchAccessor->isAvailable(*trackToMatch)) {
        if (m_trackToMatchAccessor->operator()(*trackToMatch).isValid()) {
          tracksToMatchedTracks[*(m_trackToMatchAccessor->operator()(*trackToMatch))] = trackToMatch;
          counter.nTracksToMatchSelected++;
        }
      } else {
        auto originalTrack = dynamic_cast<const xAOD::TrackParticle*>(xAOD::getOriginalObject(*trackToMatch));
        if (originalTrack) {
          tracksToMatchedTracks[originalTrack] = trackToMatch;
        } else {
          tracksToMatchedTracks[trackToMatch] = trackToMatch;
        }
        counter.nTracksToMatchSelected++;
      }
    }

    for (const auto& track : *tracks) {
      counter.nTracks++;
      bool foundMatch = false;
      if (m_trackAccessor && m_trackAccessor->isAvailable(*track)) {
        if (m_trackAccessor->operator()(*track).isValid()) {
          if (tracksToMatchedTracks.find(*(m_trackAccessor->operator()(*track))) != tracksToMatchedTracks.end()) {
            foundMatch = true;
          }
        }
      } else {
        auto originalTrack = dynamic_cast<const xAOD::TrackParticle*>(xAOD::getOriginalObject(*track));
        if (originalTrack != nullptr and tracksToMatchedTracks.find(originalTrack) != tracksToMatchedTracks.end()) {
          foundMatch = true;          
        } else if (tracksToMatchedTracks.find(track) != tracksToMatchedTracks.end()) {
          foundMatch = true;
        }
      }
      if (foundMatch) counter.nTracksAccepted++;
      m_selectionHandle.setBool(*track, foundMatch, sys);
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode xTRT::TrackToTrackMatchAlg::finalize() {

  size_t nBins = 4;

  // fill the cutflow histograms
  for (const auto& [sys, counter] : m_counters) {
    std::string histName;
    ANA_CHECK(m_systematicsList.service().makeSystematicsName(histName, "cflow_"+this->name()+"_%SYS%", sys))
    ANA_CHECK (book (TH1F (histName.c_str(), histName.c_str(), nBins, 0, nBins)));
    auto histogram = hist(histName);
    histogram->GetXaxis()->SetBinLabel(1, "TracksToMatch");
    for (size_t i = 0; i < counter.nTracksToMatch; i++) {
      histogram->Fill(0);
    }
    histogram->GetXaxis()->SetBinLabel(2, "TracksToMatchSelected");
    for (size_t i = 0; i < counter.nTracksToMatchSelected; i++) {
      histogram->Fill(1);
    }
    histogram->GetXaxis()->SetBinLabel(3, "Tracks");
    for (size_t i = 0; i < counter.nTracks; i++) {
      histogram->Fill(2);
    }
    histogram->GetXaxis()->SetBinLabel(4, "TracksAccepted");
    for (size_t i = 0; i < counter.nTracksAccepted; i++) {
      histogram->Fill(3);
    }
  }

  return StatusCode::SUCCESS;
}